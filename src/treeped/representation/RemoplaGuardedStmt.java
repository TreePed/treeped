/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaGuardedStmt.java
// Author: Matthew Hague
//
// Guarded statements are if and do.
// Eg.
//
// if                  do
// :: test -> stmt;    :: test -> stmt;
// :: test -> stmt;    :: test -> stmt;
// :: else -> stmt;    :: else -> stmt;
// if                  do

package treeped.representation;

import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

import java.lang.StringBuffer;

public abstract class RemoplaGuardedStmt extends RemoplaStmt {

    public class GuardStmt {
        RemoplaExpr condition;
        List<RemoplaStmt> stmts;

        /** sensitiveToIntermediateLabels, when set to false, means that we can optimise
         * translation to pushdown rules.  Instead of:
         *   ifLabel -(guard)-> firstClauseLabel -> nextLabel
         * we specify that the (first) label on the clause is not interesting to the
         * model checking, then we can combine the guard and the first clause:
         *   ifLabel -> (guard + firstClause) -> nextLabel
         */
        public GuardStmt(RemoplaExpr condition, List<RemoplaStmt> stmts) {
            this.condition = condition;
            this.stmts = stmts;
        }

        public StringBuffer toString(StringBuffer out) {
            condition.toString(out).append(" -> ");
            for (RemoplaStmt stmt : stmts) {
                stmt.toString(out).append(" ");
            }
            return out;
        }

        public RemoplaExpr getCondition() {
            return condition;
        }

        public void setCondition(RemoplaExpr condition) {
            assert condition != null;
            this.condition = condition;
        }

        public List<RemoplaStmt> getStmts() {
            return stmts;
        }
    }

    private Collection<GuardStmt> clauses = new HashSet<GuardStmt>();
    private List<RemoplaStmt> elseClause = null;

    /** Construct a Remopla if statement -- add clauses if necessary, including
      * an else clause using addClause and addElse
      */
    public RemoplaGuardedStmt() {
    }


    /** Add a new clause to the if statement -- note that Remopla does not pay
      * attention to clause ordering: if two guards are satisfied, execution is
      * non-deterministic
      *  @param guard non-null guard
      *  @param stmt non-null statement
      *  @param sensitiveToIntermediateLabels false if we don't care whether the
      *  label on the clause is reported as reachable (can be optimised away)
      *  default true.
      *  @return the clause
      */
    public GuardStmt addClause(RemoplaExpr guard, RemoplaStmt stmt) {
        assert guard != null && stmt != null;
        List<RemoplaStmt> stmts = new ArrayList<RemoplaStmt>(1);
        stmts.add(stmt);
        GuardStmt clause = new GuardStmt(guard, stmts);
        clauses.add(clause);
        return clause;
    }


    /** Add a new clause to the if statement -- note that Remopla does not pay
      * attention to clause ordering: if two guards are satisfied, execution is
      * non-deterministic
      *  @param guard non-null guard
      *  @param stmt non-null statement
      *  @param sensitiveToIntermediateLabels false if we don't care whether the
      *  label on the clause is reported as reachable (can be optimised away)
      *  default true.
      *  @return the clause
      */
    public GuardStmt addClause(RemoplaExpr guard, List<RemoplaStmt> stmts) {
        assert guard != null && stmts != null && !stmts.isEmpty();
        for (RemoplaStmt stmt : stmts) {
            assert stmt != null;
        }
        GuardStmt clause = new GuardStmt(guard, stmts);
        clauses.add(clause);
        return clause;
    }

    /** @return the collection of statements with clauses (does not contain the
     * else clause)
     */
    public Collection<GuardStmt> getClauses() {
        return clauses;
    }

    /** Add an else clause, overwriting any existing else
      *  @param clause non-null else clause
      *  @param sensitiveToIntermediateLabels see addClause
      */
    public void addElse(RemoplaStmt stmt) {
        assert stmt != null;
        elseClause = new ArrayList<RemoplaStmt>(1);
        elseClause.add(stmt);
    }

    public void addElse(List<RemoplaStmt> stmts) {
        assert stmts != null;
        for (RemoplaStmt s : stmts) {
            assert s != null;
        }
        elseClause = stmts;
    }

    public boolean hasElse() {
        return elseClause != null;
    }

    public List<RemoplaStmt> getElse() {
        return elseClause;
    }


    /** Writes the body of a statement to a string buffer.
      * That is, the statement without labels or other paraphernalia.
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer toStringBody(StringBuffer out) {
        writeStart(out).append("\n");

        for(GuardStmt gs : clauses) {
            out.append(":: ");
            gs.toString(out).append("\n");
        }

        if(elseClause != null) {
            out.append(":: else -> ");
            for (RemoplaStmt stmt : elseClause) {
                stmt.toString(out).append(" ");
            }
            out.append("\n");
        }

        return writeEnd(out).append(";");
    }


    public void flatten(List<RemoplaStmt> stmts) {
        super.flatten(stmts);
        for (GuardStmt gs : clauses) {
            for (RemoplaStmt stmt : gs.getStmts()) {
                stmt.flatten(stmts);
            }
        }

        if (elseClause != null) {
            for (RemoplaStmt stmt : elseClause) {
                stmt.flatten(stmts);
            }
        }
    }

    /** Writes the beginning (if or do) **/
    public abstract StringBuffer writeStart(StringBuffer out);
    /** Writes the end (fi or od) **/
    public abstract StringBuffer writeEnd(StringBuffer out);
}
