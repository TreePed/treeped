/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// Remopla.java
// Author: Matthew Hague
//
// The Remopla class hierarchy is based on the PDS hierarchy in jMoped by
// Dejvuth Suwimonteerabuth

// The Remopla class is the umbrella class for the Remopla representation
// Maintains a list of methods and variables &c.
// Provides to<WHAT> output method

package treeped.representation;

import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.lang.StringBuffer;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;

import treeped.main.CmdOptions;
import java.util.Set;
import treeped.representation.RemoplaStmt;
import java.util.List;
import java.util.ArrayList;

public class Remopla {
    private static final String INIT = "init";
    private static final String DEF_DEFAULT_INT_BITS = "define DEFAULT_INT_BITS";

    static Logger logger = Logger.getLogger(Remopla.class);

    // map names to objects
    Map<String, RemoplaDeclaredType> declaredTypes = new HashMap<String, RemoplaDeclaredType>();
    Map<String, RemoplaMethod> methods = new HashMap<String, RemoplaMethod>();
    Map<String, RemoplaVar> globals = new HashMap<String, RemoplaVar>();
    Map<String, RemoplaIntConstVar> constants = new HashMap<String, RemoplaIntConstVar>();
    Collection<RemoplaStmt> floating = new HashSet<RemoplaStmt>();

    RemoplaMethod initialMethod;

    public Remopla() {
    }

    public void addMethod(RemoplaMethod method) {
        methods.put(method.getCallString(), method);
    }

    public Collection<RemoplaMethod> getMethods() {
        return methods.values();
    }

    /** Designates the initial method.
     *   @param initialMethod must be non-null, take no arguments and already added
     *   to the remopla instance as a method
     */
    public void setInitialMethod(RemoplaMethod initialMethod) {
        assert initialMethod != null;
        assert initialMethod.getNumParameters() == 0;
        assert getMethods().contains(initialMethod);
        this.initialMethod = initialMethod;
    }


    /** Returns the initial method, null if one hasn't been set **/
    public RemoplaMethod getInitialMethod() {
        return initialMethod;
    }

    /** Writes the Remopla program to a string buffer
      *  @param out the buffer to write to
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        assert out != null;
        writeHeader(out);
        for(RemoplaMethod method : getMethods()) {
            method.toString(out);
            out.append("\n\n");
        }
        writeFooter(out);
        return out;
    }


    public String toString() {
        return toString(new StringBuffer()).toString();
    }

    /** returns the RemoplaVar object associated with a global variable name
      *  @param name variable name
      *  @return RemoplaVar object, or null if not found
      */
    public RemoplaVar getGlobalVar(String name) {
        return globals.get(name);
    }

    /** adds a new local variable, throws an exception if it already exists
      *  @param var the var, non-null
      */
    public void addGlobalVar(RemoplaVar var) throws Exception {
        assert var != null;
        if (globals.containsKey(var.getName())) {
            throw new Exception("Variable " + var.getName() + " already exists.");
        }

        globals.put(var.getName(), var);
    }

    public Collection<RemoplaVar> getGlobals() {
        return globals.values();
    }

    /** @return a map from variable names to RemoplaVar **/
    public Map<String, RemoplaVar> getGlobalsMap() {
        return globals;
    }

    /** adds a new constant definition, throws an exception if it already exists
      *  @param var the var, non-null
      */
    public void addConstant(RemoplaIntConstVar var) throws Exception {
        assert var != null;
        if (constants.containsKey(var.getName())) {
            throw new Exception("Variable " + var.getName() + " already exists.");
        }

        constants.put(var.getName(), var);
    }

    public Collection<RemoplaIntConstVar> getConstants() {
        return constants.values();
    }

    public RemoplaIntConstVar getConstant(String name) {
        return constants.get(name);
    }

    /** @return a map from variable names to RemoplaIntConstVar **/
    public Map<String, RemoplaIntConstVar> getConstantsMap() {
        return constants;
    }
    /** Returns the declared type object corresponding to the given string
      *  @param typeName a String giving the name of the type
      *  @return the associated RemoplaDeclaredType, null if not found
      */
    public RemoplaDeclaredType getDeclaredType(String typeName) {
        return declaredTypes.get(typeName);
    }


    /** Adds a new declared type, throws an exception if it already exists
     *   @param type the RemoplaDeclaredType object, non-null
     */
    public void addDeclaredType(RemoplaDeclaredType type) throws Exception {
        assert type != null;
        if (globals.containsKey(type.getName())) {
            throw new Exception("Declared type " + type.getName() + " already exists.");
        }

        declaredTypes.put(type.getName(), type);
    }


    /** Define a declared type, possibly overwriting an existing type.
     *   @param type the RemoplaDeclaredType object, non-null
     */
    public void redefineDeclaredType(RemoplaDeclaredType type) {
        assert type != null;
        declaredTypes.put(type.getName(), type);
    }



    /** Add a floating statement (these are generally of the form label: goto
     * label and constitute dead end error states)
     *  @param stmt non-null statement
     */
    public void addFloatingStmt(RemoplaStmt stmt) {
        assert stmt != null;
        floating.add(stmt);
    }

    public Collection<RemoplaStmt> getFloatingStmts() {
        return floating;
    }


    public RemoplaMethod getMethod(String callString) {
        return methods.get(callString);
    }

    /** @return a map from call strings to methods **/
    public Map<String, RemoplaMethod> getMethodMap() {
        return methods;
    }


    /** @return all the labels defined in the program **/
    public Set<String> getLabels() {
        Set<String> labels = new HashSet();
        List<RemoplaStmt> stmts = new ArrayList();
        for (RemoplaMethod method : methods.values()) {
            stmts.clear();
            method.getFlattenedStmts(stmts);
            for (RemoplaStmt s : stmts) {
                if (s.getLabel() != null)
                    labels.add(s.getLabel());
            }
        }
        return labels;
    }


    /////////////////////////////////////////////////////////////////////////////////////
    // Private methods
    //



    private StringBuffer writeHeader(StringBuffer out) {
        out.append(DEF_DEFAULT_INT_BITS).append(" ").append(CmdOptions.getIntBits()).append("\n\n");

        for (RemoplaIntConstVar constVar : constants.values()) {
            if (constVar.getUsed()) {
                constVar.getDeclaration(out).append("\n");
            }
        }

        out.append("\n");

        for (RemoplaDeclaredType type : declaredTypes.values()) {
            type.toString(out).append("\n");
        }

        out.append("\n");

        for (RemoplaVar global : globals.values()) {
            if (global.getUsed()) {
                global.getDeclaration(out).append(";\n");
            }
        }

        out.append("\n");

        for (RemoplaMethod method : getMethods()) {
            method.getDeclaration(out).append(";\n");
        }


        out.append("\n");

        if (initialMethod != null) {
            out.append(INIT).append(" ").append(initialMethod.getCallString()).append(";");
        }

        return out.append("\n\n");
    }

    private StringBuffer writeFooter(StringBuffer out) {
        out.append("\n");

        for (RemoplaStmt stmt : floating) {
            stmt.toString(out).append("\n");
        }
        return out;
    }

}

