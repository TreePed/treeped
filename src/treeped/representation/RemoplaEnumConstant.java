/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaEnumConstant.java
// Author: Matthew Hague
//
// A class for representing constant enum expressions

package treeped.representation;

import java.lang.StringBuffer;

import org.apache.log4j.Logger;

public class RemoplaEnumConstant extends RemoplaExpr {

    static Logger logger = Logger.getLogger(RemoplaEnumConstant.class);

    String val;

    public RemoplaEnumConstant(String val) {
        assert val != null;
        this.val = val;
    }

    public String getVal() {
        return val;
    }


    /** Writes the constant to a string buffer
      *  @param out the string buffer
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        return out.append(val);
    }


    // Need to override these two methods (well, hashcode at least) to ensure we
    // don't add duplicate constants to sets)
    public boolean equals(Object ec) {
        if (ec instanceof RemoplaEnumConstant)
            return val.equals(((RemoplaEnumConstant)ec).getVal());
        else
            return false;
    }

    public int hashCode() {
        return val.hashCode();
    }

    public void accept(RemoplaExprVisitor visitor) {
        visitor.visit(this);
    }
}
