/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaIntConstVar.java
// Author: Matthew Hague
//
// Class for constant variables.  Extends RemoplaExpr since they are not
// variables like an int is, for example.


package treeped.representation;

import java.lang.StringBuffer;


public class RemoplaIntConstVar extends RemoplaExpr {

    private int value;
    private String name;
    private boolean used;

    public RemoplaIntConstVar(String name, int value, boolean used) {
        assert name != null;
        this.name = name;
        this.used = used;
        this.value = value;
    }

    public RemoplaIntConstVar(String name, boolean local, boolean used) {
        assert name != null;
        this.name = name;
        this.used = used;
        this.value = -1;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        assert name != null;
        this.name = name;
    }

    /** @return whether the variable is used.  Variables not marked as used are
     * not declared by their methods/global instance.
     */
    public boolean getUsed() {
        return used;
    }

    /** Set whether a variable is marked as used (unused variables not declared
     * in toString output of RemoplaMethod/Remopla
     */
    public void setUsed(boolean used) {
        this.used = used;
    }

     /** Write the declaration to a string buffer, without a trailing ";" or ","
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer getDeclaration(StringBuffer out) {
        getDeclarationPrefix(out).append(" ").append(getName());
        return getDeclarationSuffix(out);
    }

    public String getDeclaration() {
        return getDeclaration(new StringBuffer()).toString();
    }



    /** Write the declaration prefix to a string buffer
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer getDeclarationPrefix(StringBuffer out) {
        return out.append("define");
    }

    /** Write the declaration suffix to a string buffer.
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer getDeclarationSuffix(StringBuffer out) {
        return out.append(" ").append(getValue());
    }

    public void accept(RemoplaExprVisitor visitor) {
        visitor.visit(this);
    }

    /** Writes the variable to out.  For use within code.
      * Use getDeclaration for the full declaration.
      *  @param out string buffer to write to
      *  @return the given string buffer
      */
    public StringBuffer toString(StringBuffer out) {
        out.append(name);
        return out;
    }


}
