/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaInvokeAssignStmt.java
// Author: Matthew Hague
//
// Assignments of the form a = b(args);
// These cannot occur as part of a parallel assign, and are dealt with
// separately.

package treeped.representation;
import java.util.List;

public class RemoplaInvokeAssignStmt extends RemoplaStmt {

    private RemoplaExpr lhs;
    private RemoplaMethodCall rhs;

    public RemoplaInvokeAssignStmt(RemoplaExpr lhs, RemoplaMethodCall rhs) {
        assert lhs != null && rhs != null;
        this.lhs = lhs;
        this.rhs = rhs;
    }

    public RemoplaExpr getLeft() {
        return lhs;
    }

    public RemoplaMethodCall getRight() {
        return rhs;
    }

    public void accept(RemoplaStmtVisitor visitor) {
        visitor.visit(this);
    }

    public StringBuffer toStringBody(StringBuffer out) {
        lhs.toString(out).append(" = ");
        return rhs.toString(out).append(";");
    }

}


