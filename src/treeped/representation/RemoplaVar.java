/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaVar.java Author: Matthew Hague
//
// Variables in Remopla
//
// See notes on setUsed and getUsed.
//
// Note on suffixes and prefixes of declarations: This is a hack to get around
// Remopla's style of integer array definitions.  The suffix is the declaration
// preceding the name, the suffix is the declaration following the name.
// Ideally we would have
//
//      int iarray(8)[n]
//
// to declare an array of 8-bit integers of size n.  Then array declarations
// would simply be the array type with the array size appended.  However,
// Remopla uses
//
//      int iarray[n](8)
//
// Similarly, two-dimensional arrays,
//
//      type twodarray[n][m]
//
// Are matrices of n rows and m columns.  That is, an n-array of m-arrays.
// Using the idealised,
//
//      typedecl[dim]
//
// we would have,
//
//      type twodarray[m][n]
//
// Since this style of definition is unnatural, we gladly submit to a more
// cumbersome implementation.


package treeped.representation;

import java.lang.StringBuffer;


public abstract class RemoplaVar extends RemoplaExpr implements Cloneable {


    boolean used = false;
    boolean local;
    private String name;

    /** @param name the variable name
     * @param local true if the variable is a local variable
     */
    public RemoplaVar(String name, boolean local) {
        assert name != null;
        this.local = local;
        this.name = name;
    }

    /** @param name the variable name
     * @param local true if the variable is a local variable
     * @param used whether the variable is used
     */
    public RemoplaVar(String name, boolean local, boolean used) {
        assert name != null;
        this.local = local;
        this.name = name;
        this.used = used;
    }


    /** Writes the variable to out.  For use within code.
      * Use getDeclaration for the full declaration.
      *  @param out string buffer to write to
      *  @return the given string buffer
      */
    public StringBuffer toString(StringBuffer out) {
        out.append(name);
        return out;
    }

    /** Returns the variable as a string for use in within normal code.  Use
      * getDeclaration for the declaration.
      *  @return variable as a string
      */
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        assert name != null;
        this.name = name;
    }

    /** @return whether the variable is used.  Variables not marked as used are
     * not declared by their methods/global instance.
     */
    public boolean getUsed() {
        return used;
    }

    /** Set whether a variable is marked as used (unused variables not declared
     * in toString output of RemoplaMethod/Remopla
     */
    public void setUsed(boolean used) {
        this.used = used;
    }

    /** Set whether the variable is a local variable **/
    public void setLocal(boolean local) {
        this.local = local;
    }

    /** @return true if the variable is a local variable **/
    public boolean getLocal() {
        return local;
    }


    /** Write the declaration to a string buffer, without a trailing ";" or ","
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer getDeclaration(StringBuffer out) {
        getDeclarationPrefix(out).append(" ").append(getName());
        return getDeclarationSuffix(out);
    }

    public String getDeclaration() {
        return getDeclaration(new StringBuffer()).toString();
    }

    /** @return the part of the declaration before the variable name
      */
    public String getDeclarationPrefix() {
        return getDeclarationPrefix(new StringBuffer()).toString();
    }

    /** @return the part of the declaration after the variable name
      */
    public String getDeclarationSuffix() {
        return getDeclarationSuffix(new StringBuffer()).toString();
    }

    /** Write the declaration prefix to a string buffer
      *  @param out the buffer
      *  @return the buffer
      */
    public abstract StringBuffer getDeclarationPrefix(StringBuffer out);

    /** Write the declaration suffix to a string buffer.  A default "do nothing"
      * implementation is provided since most variables don't have a suffix.
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer getDeclarationSuffix(StringBuffer out) {
        return out;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public abstract void accept(RemoplaVarVisitor visitor);


    public boolean equals(Object o) {
        if (o instanceof RemoplaVar) {
            RemoplaVar v = (RemoplaVar)o;
            return (this.name.equals(v.name) &&
                    this.local == v.local);
        }
        return false;
    }

    public int hashCode() {
        return name.hashCode() + (local ? 0 : 1);
    }
}
