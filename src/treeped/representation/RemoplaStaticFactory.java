/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaStaticFactory.java
// Author: Matthew Hague
//
// A collection of static constructors for Remopla code.  For convenience.


package treeped.representation;

import java.util.ArrayList;
import java.util.List;


public class RemoplaStaticFactory {

    // For posterity:
    //  :'<,'>s/\s*\(.\{-}\)(.\{-})\(,\|;\)/\r\r\tstatic public RemoplaBinExpr \L\1\E(RemoplaExpr l, RemoplaExpr r) {\r\t\t return new RemoplaBinExpr(l, Op.  \1, r);\r\t}/g



	static public RemoplaBinExpr add(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.ADD, r);
	}

	static public RemoplaBinExpr sub(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.SUB, r);
	}

	static public RemoplaBinExpr div(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.DIV, r);
	}

	static public RemoplaBinExpr mul(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.MUL, r);
	}

	static public RemoplaBinExpr and(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.AND, r);
	}


	static public RemoplaBinExpr or(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.OR, r);
	}

	static public RemoplaBinExpr xor(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.XOR, r);
	}

	static public RemoplaBinExpr eq(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.EQ, r);
	}

	static public RemoplaBinExpr neq(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.NEQ, r);
	}

	static public RemoplaBinExpr lt(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.LT, r);
	}


	static public RemoplaBinExpr lte(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.LTE, r);
	}

	static public RemoplaBinExpr gt(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.GT, r);
	}

	static public RemoplaBinExpr gte(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.GTE, r);
	}

	static public RemoplaBinExpr shl(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.SHL, r);
	}

	static public RemoplaBinExpr shr(RemoplaExpr l, RemoplaExpr r) {
		 return new RemoplaBinExpr(l, RemoplaBinExpr.Op.SHR, r);
	}

    static public RemoplaUnaryExpr not(RemoplaExpr e) {
        return new RemoplaUnaryExpr(RemoplaUnaryExpr.Op.NEG, e);
    }


    /** @param lhs expression to assign to (non-null)
     * @param rhs value of assignment (expression) (non-null)
     * @return a new RemoplaAssignStmt assigning lhs = rhs
     */
    static public RemoplaAssignStmt assign(RemoplaExpr lhs, RemoplaExpr rhs) {
        assert lhs != null && rhs != null;
        return new RemoplaAssignStmt(lhs, rhs);
    }

    static public RemoplaIfStmt ifthenelse(RemoplaExpr test,
                                           RemoplaStmt thenClause,
                                           RemoplaStmt elseClause) {
        RemoplaIfStmt ifStmt = new RemoplaIfStmt();
        ifStmt.addClause(test, thenClause);
        if (elseClause != null)
            ifStmt.addElse(elseClause);
        return ifStmt;
    }

    static public RemoplaIfStmt ifthenelse(RemoplaExpr test,
                                           List<RemoplaStmt> thenClause,
                                           List<RemoplaStmt> elseClause) {
        RemoplaIfStmt ifStmt = new RemoplaIfStmt();
        ifStmt.addClause(test, thenClause);
        if (elseClause != null)
            ifStmt.addElse(elseClause);
        return ifStmt;
    }



    /** returns a RemoplaAssignStmt initialised with addForAllParallelAssignment
     */
    static public RemoplaAssignStmt assignForAll(RemoplaIntVar ri, int min, int max, RemoplaExpr lhs, RemoplaExpr rhs) {
        return new RemoplaAssignStmt(ri, min, max, lhs, rhs);
    }

    static public RemoplaIfStmt ifthen(RemoplaExpr test,
                                       RemoplaStmt thenClause) {
        return ifthenelse(test, thenClause, null);
    }

    /** @param a the array (non-null)
     * @param i the index (non-null)
     * @return a RemoplaArrayRefExpr to a[i]
     */
    static public RemoplaArrayRefExpr aRef(RemoplaArrayVar a, RemoplaExpr i) {
        return new RemoplaArrayRefExpr(a, i);
    }

    /** @param a the array (non-null)
     * @param i the index (non-null)
     * @param i2 the second index (non-null)
     * @return a RemoplaArrayRefExpr to a[i]
     */
    static public RemoplaArrayRefExpr aRef(RemoplaArrayVar a, RemoplaExpr i, RemoplaExpr i2) {
        return new RemoplaArrayRefExpr(a, i, i2);
    }

    static public RemoplaArrayVar oneDimIntArr(String name, int min, int max, boolean local, boolean used) {
        RemoplaIntVar base = new RemoplaIntVar(name, local, used);
        return new RemoplaArrayVar(base, min, max, local, used);
    }

    public static RemoplaBreakStmt rbreak() {
        return new RemoplaBreakStmt();
    }

    public static RemoplaDoStmt rdo() {
        return new RemoplaDoStmt();
    }

    public static RemoplaGotoStmt rgoto(String label) {
        return new RemoplaGotoStmt(label);
    }

    public static RemoplaInvokeAssignStmt invAssign(RemoplaExpr lhs, String callString, List<RemoplaExpr> parameters) {
        return new RemoplaInvokeAssignStmt(lhs, new RemoplaMethodCall(callString, parameters));
    }

    public static RemoplaInvokeAssignStmt invAssign(RemoplaExpr lhs, String callString, RemoplaExpr... parameters) {
        return new RemoplaInvokeAssignStmt(lhs, new RemoplaMethodCall(callString, parameters));
    }

    public static RemoplaInvokeStmt invoke(String callString, List<RemoplaExpr> parameters) {
        return new RemoplaInvokeStmt(new RemoplaMethodCall(callString, parameters));
    }

    public static RemoplaInvokeStmt invoke(String callString, RemoplaExpr... parameters) {
        return new RemoplaInvokeStmt(new RemoplaMethodCall(callString, parameters));
    }

    /** Convenience method for constructor **/
    public static RemoplaMethod method(RemoplaMethod.ReturnType returnType,
                                       String methodName,
                                       RemoplaVar... parameters) {
        return new RemoplaMethod(returnType, methodName, parameters);
    }

    public static RemoplaMethod.ReturnType rvoid() {
        return RemoplaMethod.ReturnType.VOID;
    }

    public static RemoplaMethod.ReturnType rint() {
        return RemoplaMethod.ReturnType.INT;
    }

    public static RemoplaMethod.ReturnType rbool() {
        return RemoplaMethod.ReturnType.BOOL;
    }

    public static RemoplaMethodCall call(String callString, List<RemoplaExpr> parameters) {
        return new RemoplaMethodCall(callString, parameters);
    }

    public static RemoplaMethodCall call(String callString, RemoplaExpr... parameters) {
        return new RemoplaMethodCall(callString, parameters);
    }

    static public RemoplaReturnStmt rreturn() {
        return new RemoplaReturnStmt(null);
    }

    static public RemoplaReturnStmt rreturn(RemoplaExpr val) {
        return new RemoplaReturnStmt(val);
    }

    /** @param guard the guard of the skip statement
     * @return a new RemoplaSkipStmt with the given guard
     */
    static public RemoplaSkipStmt skip(RemoplaExpr guard) {
        return new RemoplaSkipStmt(guard);
    }


    /** @param guard the guard of the skip statement
     * @return a new RemoplaSkipStmt with the given guard
     */
    static public RemoplaSkipStmt skip() {
        return new RemoplaSkipStmt();
    }


    static public RemoplaNullStmt rnull(String warning) {
        return new RemoplaNullStmt(warning);
    }

    static public RemoplaUndefExpr undef() {
        return new RemoplaUndefExpr();
    }

    static public RemoplaBoolConstant rbool(boolean val) {
        return new RemoplaBoolConstant(val);
    }

    static public RemoplaIntConstant rint(long val) {
        return new RemoplaIntConstant(val);
    }


    static public List<RemoplaStmt> block(RemoplaStmt... stmts) {
        List<RemoplaStmt> block = new ArrayList<RemoplaStmt>(stmts.length);
        for (int i = 0; i < stmts.length; i++) {
            block.add(stmts[i]);
        }
        return block;
    }
}
