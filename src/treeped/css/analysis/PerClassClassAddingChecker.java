/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.analysis;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.javabdd.BDD;

import de.tum.in.wpds.Fa;
import de.tum.in.wpds.PreSat;
import de.tum.in.wpds.Semiring;

import treeped.css.Namer;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.translation.HTMLToMopedTranslator;
import treeped.exceptions.VariableNotFoundException;
import treeped.main.CmdOptions;
import treeped.remoplatopds.BDDSemiring;
import treeped.remoplatopds.RemoplaToPDS;
import treeped.remoplatopds.StmtOptimisations;
import treeped.remoplatopds.VarManager;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaStmt;



/**
 * Class adding checker that does a different check for each different target
 * CSS class.
 */
public class PerClassClassAddingChecker
extends ClassAddingChecker {

    static Logger logger = Logger.getLogger(PerClassClassAddingChecker.class);

    public PerClassClassAddingChecker(AbstractHTML<SimpleRuleGuard> ahtml)
    throws Exception {
        this.ahtml = ahtml;

        HTMLToMopedTranslator tr = new HTMLToMopedTranslator();
        HTMLToMopedTranslator.RemoplaAndVars rvs
            = tr.translateSimpleHTML(ahtml, ahtml.getRhsClasses());

        this.remopla = rvs.remopt.remopla;
        Map<RemoplaStmt, StmtOptimisations> stmtOptimisations
            = rvs.remopt.stmtOptimisations;

        this.vars = rvs.vars;
        this.pds = RemoplaToPDS.translate(this.remopla, stmtOptimisations);

        // Creates variable manager
        this.manager = new VarManager(CmdOptions.getBddPackage(),
                                      CmdOptions.getNodeNum(),
                                      CmdOptions.getCacheSize(),
                                      1, // no integers, just bools
                                      this.remopla,
                                      stmtOptimisations);

        // initial fun label
        RemoplaMethod nodeFunMethod = this.remopla.getMethod(Namer.getNodeMethodName());
        this.nodeFun = nodeFunMethod.getStmts().get(0).getLabel();
    }

    public List<PushdownRule> getWitness(PushdownJustification justification) {
        logger.error("PerClassClassAddingChecker doesn't do witnesses yet");
        System.exit(-1);
        // TODO: this
        return null;
    }


    protected CheckResult doCheck(CSSClass addClass,
                                  Collection<CSSClass> initClasses,
                                  Collection<CSSClass> parentClasses,
                                  Collection<CSSClass> childClasses,
                                  boolean isRoot)
    throws Exception {

        //  + Build an automaton that accepts single element stack
        //    with addClass set to true
        //  + Do the pre*
        //  + True iff config that corresponds to I_{initClasses,
        //    parentClasses, isRoot} is accepted by the pre*

        Semiring possInit = getPossibleInitSemiring(addClass);


        // create the semiring describing the initial state we're testing
        BDD initBDD = createInitBDD(isRoot,
                                    initClasses,
                                    parentClasses,
                                    childClasses);
        BDDSemiring initRing = new BDDSemiring(manager, initBDD);

        Semiring res = possInit.id().andWith(initRing);

        boolean result = !res.isZero();

        res.free();

        CheckResult cres;
        if (getJustification()) {
            // TODO: get witness (memoize it!)
            throw new Exception("TODO: implemenent witness generation!");
        } else {
            cres = new CheckResult(result);
        }
        return cres;
    }


    protected CheckResult doCheckFullTree(CSSClass addClass,
                                          Collection<CSSClass> initClasses,
                                          Collection<CSSClass> parentClasses,
                                          Collection<CSSClass> childClasses,
                                          boolean isRoot)
    throws Exception {
        //  + Build an automaton that accepts all stacks with ya = true on top.
        //  (and at least two stack elements, but that's only for convenience)
        //  + Do the pre*
        //  + True iff config that corresponds to I_{initClasses,
        //    parentClasses, isRoot} is accepted by the pre*

        Semiring possInit = getFullTreePossibleInitSemiring(addClass);


        // create the semiring describing the initial state we're testing
        BDD initBDD = createInitBDD(isRoot,
                                    initClasses,
                                    parentClasses,
                                    childClasses);
        BDDSemiring initRing = new BDDSemiring(manager, initBDD);

        Semiring res = possInit.id().andWith(initRing);

        boolean result = !res.isZero();

        res.free();

        CheckResult cres;
        if (getJustification()) {
            // TODO: get witness (memoize it!)
            throw new Exception("TODO: implemenent witness generation!");
        } else {
            cres = new CheckResult(result);
        }
        return cres;
    }




    /**
     * Memoization of createPossibleInitSemiring
     */
    private Map<CSSClass, Semiring> possInitMemo = new HashMap<CSSClass, Semiring>();
    private Semiring getPossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {
        Semiring res = possInitMemo.get(addClass);
        if (res == null) {
            res = createPossibleInitSemiring(addClass);
            possInitMemo.put(addClass, res);
        }
        return res;
    }

    /**
     * Uses moped to get a semiring giving all possible initial variable
     * assignments that can eventually lead to the addition of class addClass at
     * the root node.
     *
     * @param addClass find representation of all initial variable assignments
     * that can lead to addClass being added to the root
     * @return a semiring representation of the above
     */
    private Semiring createPossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {

        // Initializes target FA
        BDD targetBDD = createTargetBDD(addClass);
        // the target aut just needs one transition
        Fa fa = new Fa();
        fa.add(new BDDSemiring(manager, targetBDD),
               Fa.q_i,
               nodeFun,
               Fa.q_f);

        // pre*
        PreSat sat = new PreSat(pds, manager);
        PreSat.PreSatResult preFa = sat.prestar(fa, null);

        // get copy of result semiring
        Semiring res = preFa.fa.getWeight(Fa.q_i, nodeFun, Fa.q_f);

        if (res != null)
            res = res.id();
        else
            res = new BDDSemiring(manager, manager.zero());

        // free all others
        preFa.free();

        return res;
    }


    /**
     * Memoization of createFullTreePossibleInitSemiring
     */
    private Map<CSSClass, Semiring> fullTreePossInitMemo
        = new HashMap<CSSClass, Semiring>();

    private Semiring getFullTreePossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {
        Semiring res = fullTreePossInitMemo.get(addClass);
        if (res == null) {
            res = createFullTreePossibleInitSemiring(addClass);
            fullTreePossInitMemo.put(addClass, res);
        }
        return res;
    }

    /**
     * Uses moped to get a semiring giving all possible initial variable
     * assignments that can eventually lead to the addition of class addClass at
     * some child node except the root node.
     *
     * @param addClass find representation of all initial variable assignments
     * that can lead to addClass being added to some node except root
     * @return a semiring representation of the above
     */
    private Semiring createFullTreePossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {

        // Initializes target FA
        BDD targetBDD = createTargetBDD(addClass);
        BDD one = manager.initVars();
        String q = Namer.getMidState();

        // the target aut just needs
        //
        //   q0 -- targetBDD --> q -- one --> qf
        //                     /   \
        //                     |   |
        //                     \one/
        //
        //
        // that is class add true on top, and anything under
        Fa fa = new Fa();
        fa.add(new BDDSemiring(manager, targetBDD),
               Fa.q_i,
               nodeFun,
               q);
        for (String l : remopla.getLabels()) {
            fa.add(new BDDSemiring(manager, one.id()),
                   q,
                   l,
                   q);
            fa.add(new BDDSemiring(manager, one.id()),
                   q,
                   nodeFun,
                   Fa.q_f);
        }
        one.free();

        // pre*
        PreSat sat = new PreSat(pds, manager);
        PreSat.PreSatResult preFa = sat.prestar(fa, null);

        // get copy of result semiring
        Semiring res = preFa.fa.getWeight(Fa.q_i, nodeFun, Fa.q_f);

        if (res != null)
            res = res.id();
        else
            res = new BDDSemiring(manager, manager.zero());

        // free all others
        preFa.free();

        return res;
    }






    /**
     * Create bdd for initial config (x1,...,xn) nodeFun(root,y1,...,yn,z1,...,zn)
     * where
     *
     *      pop = pop
     *      root = isRoot
     *      x1,...,xn = childClasses
     *      y1,...,yn = initClasses
     *      z1,...,zn = parentClasses
     *
     * @param isRoot whether the parent of the node being tested is root
     * @param initClasses the classes the initial node has
     * @param parentClasses the classes its parent has
     * @param childClasses the classes its child has
     * @return a bdd that can be the weight on an fa transition, asserting the
     * values of the variables as above
     */
    private BDD createInitBDD(boolean isRoot,
                              Collection<CSSClass> initClasses,
                              Collection<CSSClass> parentClasses,
                              Collection<CSSClass> childClasses)
    throws VariableNotFoundException {
        BDD bdd = manager.initVars();

        for (CSSClass c : vars.getXVarClasses()) {
            manager.initBoolVar(vars.getXVar(c),
                                childClasses.contains(c),
                                bdd);
        }

        for (CSSClass c : vars.getYVarClasses()) {
            manager.initBoolVar(vars.getYVar(c),
                                initClasses.contains(c),
                                bdd);
        }

        for (CSSClass c : vars.getZVarClasses()) {
            manager.initBoolVar(vars.getZVar(c),
                                parentClasses.contains(c),
                                bdd);
        }

        manager.initBoolVar(vars.getRootVar(), isRoot, bdd);
        manager.initBoolVar(vars.getPopVar(), true, bdd);

        return bdd;
    }


    /**
     * Create bdd for target config (x1,...,xn) nodeFun(root,y1,...,yn,z1,...,zn)
     * where yi for class addClass is true (anything else can be anything)
     *
     * @param addClass the classes to add
     * @return a bdd that can be the weight on an fa transition, asserting the
     * values of the variables as above
     */
    private BDD createTargetBDD(CSSClass addClass)
    throws VariableNotFoundException {
        BDD bdd = manager.initVars();

        manager.initBoolVar(vars.getYVar(addClass),
                            true,
                            bdd);

        return bdd;
    }


}
