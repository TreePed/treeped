/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.analysis;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import de.tum.in.wpds.Pds;

import treeped.css.parser.AbstractHTMLReader;
import treeped.css.representation.AHTMLTree;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.Rule;
import treeped.css.representation.RuleAddClass;
import treeped.css.representation.RuleGuard;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.translation.AHTMLToSimpleAHTML;
import treeped.main.CmdOptions;
import treeped.representation.Remopla;


public class HTMLAnalyser {

    static Logger logger = Logger.getLogger(HTMLAnalyser.class);

    /**
     * NO_CHECK_PERFORMED -- CmdOptions says translateOnly
     * NO_TREE -- no tree given in ahtml
     * NO_REDUNDANCIES -- no redundant classes and (if css stylesheet classes
     *                    specified in CSS) no classes used in tree that are
     *                    not in CSS
     * REDUNDANCIES -- redundant classes and no classes in tree not in CSS (if def)
     * NO_REDUNDANCIES_UNDEFINED_CLASSES -- as above but some classes used in tree not in CSS file
     * REDUNDANCIES_UNDEFINED_CLASSES -- as above but undefined classes
     */
    public enum ResultType {
        NO_CHECK_PERFORMED,
        NO_TREE,
        NO_REDUNDANCIES,
        REDUNDANCIES,
        NO_REDUNDANCIES_UNDEFINED_CLASSES,
        REDUNDANCIES_UNDEFINED_CLASSES;
    }

    public class AnalysisResult {
        private ResultType type;
        private Set<CSSClass> usedClasses;
        private Set<CSSClass> redundancies;
        private Set<CSSClass> undefinedClasses;
        private Remopla remopla;
        private Pds pds;
        private Witness witness;
        private Map<Rule<SimpleRuleGuard>,
                    Rule<RuleGuard>> originalRuleMap;
        private AbstractHTML<SimpleRuleGuard> simpleAHtml;

        AnalysisResult(AbstractHTML<SimpleRuleGuard> simpleAHtml,
                       Remopla remopla,
                       Pds pds) {
            this.type = ResultType.NO_CHECK_PERFORMED;
            this.simpleAHtml = simpleAHtml;
            this.remopla = remopla;
            this.pds = pds;
        }

        AnalysisResult(ResultType type,
                       AbstractHTML<SimpleRuleGuard> simpleAHtml,
                       Remopla remopla,
                       Pds pds,
                       Witness witness) {
            assert type == ResultType.NO_TREE;
            this.type = type;
            this.simpleAHtml = simpleAHtml;
            this.redundancies = null;
            this.remopla = remopla;
            this.pds = pds;
            this.witness = witness;
        }

        AnalysisResult(ResultType type,
                       Set<CSSClass> usedClasses,
                       AbstractHTML<SimpleRuleGuard> simpleAHtml,
                       Remopla remopla,
                       Pds pds,
                       Witness witness) {
            assert type == ResultType.NO_REDUNDANCIES;
            this.type = type;
            this.redundancies = null;
            this.simpleAHtml = simpleAHtml;
            this.remopla = remopla;
            this.pds = pds;
            this.witness = witness;
            this.usedClasses = usedClasses;
        }

        AnalysisResult(ResultType type,
                       Set<CSSClass> usedClasses,
                       Set<CSSClass> classes,
                       AbstractHTML<SimpleRuleGuard> simpleAHtml,
                       Remopla remopla,
                       Pds pds,
                       Witness witness) {
            assert type == ResultType.REDUNDANCIES ||
                   type == ResultType.NO_REDUNDANCIES_UNDEFINED_CLASSES;
            this.type = type;
            if (type == ResultType.REDUNDANCIES)
                this.redundancies = classes;
            else
                this.undefinedClasses = classes;
            this.simpleAHtml = simpleAHtml;
            this.remopla = remopla;
            this.pds = pds;
            this.witness = witness;
            this.usedClasses = usedClasses;
        }

        AnalysisResult(ResultType type,
                       Set<CSSClass> usedClasses,
                       Set<CSSClass> redundantClasses,
                       Set<CSSClass> undefinedClasses,
                       AbstractHTML<SimpleRuleGuard> simpleAHtml,
                       Remopla remopla,
                       Pds pds,
                       Witness witness) {
            assert type == ResultType.REDUNDANCIES_UNDEFINED_CLASSES;
            this.type = type;
            this.redundancies = redundantClasses;
            this.undefinedClasses = undefinedClasses;
            this.simpleAHtml = simpleAHtml;
            this.remopla = remopla;
            this.pds = pds;
            this.witness = witness;
            this.usedClasses = usedClasses;
        }

        public ResultType getType() {
            return type;
        }

        public AbstractHTML<SimpleRuleGuard> getSimpleAHtml() {
            return simpleAHtml;
        }

        public Collection<CSSClass> getUsedClasses() {
            return usedClasses;
        }

        public Collection<CSSClass> getRedundancies() {
            if (redundancies == null)
                return null;
            else
                return Collections.unmodifiableSet(redundancies);
        }

        public Collection<CSSClass> getUndefinedClasses() {
            if (undefinedClasses == null)
                return null;
            else
                return Collections.unmodifiableSet(undefinedClasses);
        }

        public Remopla getRemopla() {
            return remopla;
        }

        public Pds getPds() {
            return pds;
        }

        public Witness getWitness() {
            return witness;
        }

        public void setOriginalRuleMap(Map<Rule<SimpleRuleGuard>,
                                           Rule<RuleGuard>> originalRuleMap) {
            this.originalRuleMap = originalRuleMap;
        }

        public Map<Rule<SimpleRuleGuard>,
                   Rule<RuleGuard>> getOriginalRuleMap() {
            return originalRuleMap;
        }

        public String toString() {
            String s;
            switch(type) {
                case NO_CHECK_PERFORMED:
                    s = "No check performed.";
                    break;
                case NO_TREE:
                    s = "No tree specified.";
                    break;
                case NO_REDUNDANCIES:
                    s = "No redundancies.";
                    break;
                case REDUNDANCIES:
                    s = "Redundancies: " + getRedundancies();
                    break;
                case NO_REDUNDANCIES_UNDEFINED_CLASSES:
                    // don't output undefined used classes since it's not clear
                    // on actual meaning or relevance
                    s = "No Redundancies.";
                    break;
                case REDUNDANCIES_UNDEFINED_CLASSES:
                    s = "Redundancies: " + getRedundancies();
                    break;
                default:
                    s = "Error: don't know result type " + type + ".";
            }

            if (usedClasses != null)
                s += "\nUsed: " + usedClasses;

            return s;
        }
    }


    private enum WorkItemType {
        SIMPLE_CHECK, CLASS_ADDING_CHECK
    }

    private class WorkItem {
        public WorkItemType type;
        public AHTMLTree node;
        public Set<CSSClass> changedClasses;
    }

    /**
     * Stores the worklist for a model checking instance, there are two kinds of
     * work item: SIMPLE_CHECK which is to do all the straightforward
     * applications on the given tree, and CLASS_ADDING_CHECK where you have to
     * do a class adding test on a node.
     *
     * remove() prioritises simple checks over class adding checks
     *
     * Also stores witness info object -- partly because it avoids carrying
     * argument around elsewhere (lazy/bad), but also because it avoids adding
     * new nodes to the worklist and the witness info everywhere (less bad)
     */
    private class Worklist {
        Set<AHTMLTree> classAddingChecks = new HashSet<AHTMLTree>();
        Set<AHTMLTree> changedNodes = new HashSet<AHTMLTree>();
        Map<AHTMLTree, Set<CSSClass>> newClasses
            = new HashMap<AHTMLTree, Set<CSSClass>>();
        WitnessInfo witnessInfo;
        // for adding justifications to witnessinfo
        int iterationNumber = 0;

        // rather than create one each time
        WorkItem item = new WorkItem();

        /**
         * @param initTree create the worklist with an initial tree
         */
        Worklist(AHTMLTree initTree) {
            witnessInfo = new WitnessInfo(initTree);
            for (AHTMLTree node : initTree.depthFirstIterator()) {
                addClassesInit(node, node.getCSSClasses());
            }
        }

        boolean isEmpty() {
            return changedNodes.isEmpty() && classAddingChecks.isEmpty();
        }

        /**
         * Remove an item from the worklist.  The WorkItem removed will be
         * reused in the next call to remove, so be careful of that...
         */
        WorkItem remove() {
            if (changedNodes.isEmpty()) {
                if (classAddingChecks.isEmpty())
                    return null;

                item.type = WorkItemType.CLASS_ADDING_CHECK;
                Iterator<AHTMLTree> i = classAddingChecks.iterator();
                item.node = i.next();

                i.remove();
            } else {
                item.type = WorkItemType.SIMPLE_CHECK;
                item.node = changedNodes.iterator().next();
                item.changedClasses = getAddNewClasses(item.node);

                removeNode(item.node);
            }

            return item;
        }

        /**
         * addClass from a simple rule application (no pushdown)
         *
         * @param node the node to add to the worklist if it does not already
         * contain the class c
         * @param c the node's new class
         * @param child the child that was used to satisfy the rule (or null if
         * none)
         * @param appliedRule the rule applied to add the class
         */
        void addClass(AHTMLTree node,
                      CSSClass c,
                      AHTMLTree child,
                      Rule<SimpleRuleGuard> appliedRule) {
            if (!node.getCSSClasses().contains(c)) {
                addClassInit(node, c);
                witnessInfo.addSimpleJustification(node,
                                                   child,
                                                   c,
                                                   appliedRule,
                                                   iterationNumber++);
            }
        }

        /**
         * addClass from a simple rule application (no pushdown)
         *
         * @param node the node to add to the worklist if it does not already
         * contain the class c
         * @param c the node's new class
         * @param justification if added by a pushdown check give this
         * justification
         */
        void addClass(AHTMLTree node,
                      CSSClass c,
                      PushdownJustification justification) {
            if (!node.getCSSClasses().contains(c)) {
                addClassInit(node, c);
                witnessInfo.addPushdownJustification(node,
                                                     c,
                                                     justification,
                                                     iterationNumber++);
            }
        }


        /**
         * @param node the node to add to the worklist only adds classes that
         * don't already label the node, does not add at all if all classes
         * already label the node
         * @param c the node's new class
         * @param child the child that was used to satisfy the rule (or null if
         * none)
         * @param appliedRule the rule applied to add the class
         */
        void addClasses(AHTMLTree node,
                        Collection<CSSClass> cs,
                        AHTMLTree child,
                        Rule<SimpleRuleGuard> appliedRule) {
            for (CSSClass c : cs)
                addClass(node, c, child, appliedRule);
        }

        /**
         * @param node the node to add to the worklist only adds classes that
         * don't already label the node, does not add at all if all classes
         * already label the node
         * @param c the node's new class
         * @param justification pushdown justification for adding classes
         */
        void addClasses(AHTMLTree node,
                        Collection<CSSClass> cs,
                        PushdownJustification justification) {
            for (CSSClass c : cs)
                addClass(node, c, justification);
        }


        /**
         * Like addClasses() but does not check that the classes are not already
         * on the node label (used to initialise the worklist)
         *
         * @param node the node to add to the worklist
         * @param cs the node's new classeseseses
         */
        void addClassesInit(AHTMLTree node, Collection<CSSClass> cs) {
            for (CSSClass c : cs) {
                addClassInit(node, c);
            }
        }

        /**
         * @param c the class added by a full tree check
         * @param justification the justification for the full tree check
         */
        void addFullTreeClass(CSSClass c,
                              PushdownJustification justification) {
            witnessInfo.addNonRootPushdownJustification(c,
                                                        justification,
                                                        iterationNumber++);
        }

        /**
         * Like addClass() but does not check that the class is not already
         * on the node label (used to initialise the worklist)
         *
         * @param node the node to add to the worklist
         * @param c the node's new class
         */
        void addClassInit(AHTMLTree node, CSSClass c) {
            changedNodes.add(node);
            getAddNewClasses(node).add(c);

            // because node changed we have to class adding check it and its
            // children and parent again
            addClassAddingCheck(node);
            for (AHTMLTree child : node.getChildren()) {
                addClassAddingCheck(child);
            }
            if (!node.isRoot()) {
                addClassAddingCheck(node.getParent());
            }
        }


        void addClassAddingCheck(AHTMLTree node) {
            classAddingChecks.add(node);
        }

        /**
         * @return the witness info object for generating traces
         */
        WitnessInfo getWitnessInfo() {
            return witnessInfo;
        }

        private Set<CSSClass> getAddNewClasses(AHTMLTree node) {
            Set<CSSClass> newcs = newClasses.get(node);
            if (newcs == null) {
                newcs = new HashSet<CSSClass>();
                newClasses.put(node, newcs);
            }
            return newcs;
        }

        private void removeNode(AHTMLTree node) {
            changedNodes.remove(node);
            newClasses.remove(node);
        }

    }


    public HTMLAnalyser() {
        // do nothing as yet
    }

    /**
     * Returns an analysis result saying whether there are any CSS classes that
     * cannot be used anywhere in any tree derivable from the
     * initial tree.
     *
     * @param ahtml a string representation of the ahtml
     * @return the result of analysis
     */
    public AnalysisResult analyseHTMLString(String ahtml)
    throws Exception {
        return analyseHTMLString(ahtml, true);
    }

    /**
     * Returns an analysis result saying whether there are any CSS classes that
     * cannot be used anywhere in any tree derivable from the
     * initial tree.
     *
     * @param htmlFile the name of a file where the ahtml can be found
     * @param fullTree whether do check classes appearing in whole tree or just
     * at root
     * @param witnessClass a class to produce a witness to if addable (can be
     * null)
     * @return the result of analysis
     */
    public AnalysisResult analyseHTMLFile(String htmlFile,
                                          boolean fullTree,
                                          CSSClass witnessClass)
    throws Exception {
        AbstractHTML<RuleGuard> ahtml = AbstractHTMLReader.loadFromFile(htmlFile);
        return analyseHTML(ahtml, fullTree, witnessClass);
    }

    /**
     * Returns an analysis result saying whether there are any CSS classes that
     * cannot be used anywhere in any tree derivable from the
     * initial tree.
     *
     * @param ahtml an ahtml object, can have rules without simple guards
     * @param fullTree whether do check classes appearing in whole tree or just
     * at root
     * @param witnessClass a class to produce a witness to if addable (can be
     * null)
     * @return the result of analysis
     */
    public AnalysisResult analyseHTML(AbstractHTML<RuleGuard> ahtml,
                                      boolean fullTree,
                                      CSSClass witnessClass)
    throws Exception {
        return analyseHTML(ahtml, false, fullTree, witnessClass);
    }

    /**
     * Returns an analysis result saying whether there are any CSS classes that
     * cannot be used.  If fullTree is true, the result is all classes that
     * cannot be used anywhere in any tree derivable from the initial tree.  If
     * fullTree is false, then the result is all classes that cannot label the
     * root node in a any tree derivable from the initial tree.
     *
     * @param ahtml a string representation of the ahtml
     * @param fullTree whether to check the full tree or just the root
     * @return the result of analysis
     */
    public AnalysisResult analyseHTMLString(String ahtml, boolean fullTree)
    throws Exception {
        AbstractHTML<RuleGuard> abshtml = AbstractHTMLReader.loadFromString(ahtml);
        return analyseHTML(abshtml, fullTree, null);
    }

    /**
     * Returns an analysis result saying whether there are any CSS classes that
     * cannot be used.  If fullTree is true, the result is all classes that
     * cannot be used anywhere in any tree derivable from the initial tree.  If
     * fullTree is false, then the result is all classes that cannot label the
     * root node in a any tree derivable from the initial tree.
     *
     * @param htmlFile the name of a file where the ahtml can be found
     * @return the result of analysis
     */
    public AnalysisResult analyseHTMLFile(String htmlFile)
    throws Exception {
        return analyseHTMLFile(htmlFile, true, null);
    }

    /**
     * Returns an analysis result saying whether there are any CSS classes that
     * cannot be used.  If fullTree is true, the result is all classes that
     * cannot be used anywhere in any tree derivable from the initial tree.  If
     * fullTree is false, then the result is all classes that cannot label the
     * root node in a any tree derivable from the initial tree.
     *
     * @param ahtml an ahtml object, can have rules without simple guards
     * @param fullTreeTran whether to do a full tree check by translating to
     * simple html where full tree is equal to checking at the root
     * @param fullTree whether to check the full tree or just the root (in built
     * in to checking algorithm, rather than by reduction as above)
     * @param witnessClass a class to produce a witness to if addable (can be
     * null)
     * @return the result of analysis
     */
    public AnalysisResult analyseHTML(AbstractHTML<RuleGuard> ahtml,
                                      boolean fullTreeTran,
                                      boolean fullTree,
                                      CSSClass witnessClass)
    throws Exception {
        AHTMLToSimpleAHTML.Result simp
            = AHTMLToSimpleAHTML.translate(ahtml,
                                           fullTreeTran);

        AnalysisResult res = analyseSimpleHTML(simp.ahtml,
                                               fullTree,
                                               witnessClass);
        res.setOriginalRuleMap(simp.originalRuleMap);
        return res;
    }



    /**
     * Returns an analysis result saying whether there are any CSS classes that
     * cannot appear on the root node of any tree derivable from the
     * initial tree.
     *
     * @param ahtml an ahtml object with only simple rule guards
     * @param fullTree whether do check classes appearing in whole tree or just
     * at root
     * @param witnessClass a class to produce a witness to if addable (can be
     * null)
     * @return the result of analysis
     */
    public AnalysisResult analyseSimpleHTML(AbstractHTML<SimpleRuleGuard> ahtml,
                                            boolean fullTree,
                                            CSSClass witnessClass)
    throws Exception {

        ClassAddingChecker classAdder
//            = new OneBDDClassAddingChecker(ahtml);
//            = new PerClassClassAddingChecker(ahtml);
            = new MultiRemoplaClassAddingChecker(ahtml);

        if (CmdOptions.getWitnessClass() != null)
            classAdder.setGetJustification(true);

        if (!CmdOptions.getModelCheck()) {
            return new AnalysisResult(ahtml,
                                      classAdder.remopla,
                                      classAdder.pds);
        }

        AHTMLTree tree = ahtml.getTree();

        if (tree == null) {
            return new AnalysisResult(ResultType.NO_TREE,
                                      ahtml,
                                      classAdder.remopla,
                                      classAdder.pds,
                                      null);
        }

        Worklist worklist = new Worklist(tree);

        while (!worklist.isEmpty()) {
            WorkItem item = worklist.remove();

            switch (item.type) {
                case SIMPLE_CHECK:
                    doSimpleWorkItem(item, worklist, ahtml);
                    break;

                case CLASS_ADDING_CHECK:
                    doClassAddingCheck(item, worklist, classAdder, ahtml);
                    break;

                default:
                    logger.error("Unrecognised work item type: " + item.type);
                    break;
            }
        }

        return constructResult(fullTree,
                               tree,
                               classAdder,
                               ahtml,
                               witnessClass,
                               worklist);
    }


    /**
     * Does simple checks -- that is saturate given tree with labels via the
     * simple propagation rule that does not invoke the class adding checker
     *
     * @param item the worklist item for the simple check
     * @param worklist the worklist to deplete
     * @param ahtml the abstract html being analysed
     */
    private void doSimpleWorkItem(WorkItem item,
                                  Worklist worklist,
                                  AbstractHTML<SimpleRuleGuard> ahtml) {

        applyUpRules(item, worklist, ahtml);
        applyFlatRules(item, worklist, ahtml);
        applyDownRules(item, worklist, ahtml);
    }



    // create one set object instead of many
    static private Set<CSSClass> newClasses = new HashSet<CSSClass>();
    /**
     * Goes through all the rules with an up guard that might be satisfied by
     * change in item and updates tree and worklist accordingly.
     *
     * @param item the worklist item for the simple check
     * @param worklist the worklist to deplete
     * @param ahtml the abstract html being analysed
     */
    private void applyUpRules(WorkItem item,
                              Worklist worklist,
                              AbstractHTML<SimpleRuleGuard> ahtml) {
        assert item.type == WorkItemType.SIMPLE_CHECK;

        Collection<CSSClass> nodeClasses = item.node.getCSSClasses();

        newClasses.clear();
        for (CSSClass c : item.changedClasses) {
            for (RuleAddClass<SimpleRuleGuard> r
                    : ahtml.getAddClassUpRules(c)) {
                SimpleRuleGuard g = r.getGuard();
                if (nodeClasses.containsAll(g.getUpClasses())) {
                    for (AHTMLTree child : item.node.getChildren()) {
                        Collection<CSSClass> childcs = child.getCSSClasses();
                        AHTMLTree downNode = passesGuardDown(child, g);
                        if (childcs.containsAll(g.getFlatClasses()) &&
                            downNode != null) {

                            Collection<CSSClass> addcs = r.getRhsClasses();
                            worklist.addClasses(child,
                                                addcs,
                                                child == downNode ? null : downNode,
                                                r);
                            child.addCSSClasses(addcs);
                        }
                    }
                }
            }
        }
    }


    /**
     * Goes through all the rules with a flat guard that might be satisfied by
     * change in item and updates tree and worklist accordingly.
     *
     * @param item the worklist item for the simple check
     * @param worklist the worklist to deplete
     * @param ahtml the abstract html being analysed
     */
    private void applyFlatRules(WorkItem item,
                                Worklist worklist,
                                AbstractHTML<SimpleRuleGuard> ahtml) {
        assert item.type == WorkItemType.SIMPLE_CHECK;

        for (CSSClass c : item.changedClasses) {
            for (RuleAddClass<SimpleRuleGuard> r
                    : ahtml.getAddClassFlatRules(c)) {

                SimpleRuleGuard g = r.getGuard();
                AHTMLTree downNode = passesGuardDown(item.node, g);
                if (passesGuardUpFlat(item.node, g) &&
                    downNode != null) {

                    Collection<CSSClass> addcs = r.getRhsClasses();
                    worklist.addClasses(item.node,
                                        addcs,
                                        downNode == item.node ? null : downNode,
                                        r);
                    item.node.addCSSClasses(addcs);
                }
            }
        }
    }


    /**
     * Goes through all the rules with a down guard that might be satisfied by
     * change in item and updates tree and worklist accordingly.
     *
     * @param item the worklist item for the simple check
     * @param worklist the worklist to deplete
     * @param ahtml the abstract html being analysed
     */
    private void applyDownRules(WorkItem item,
                                Worklist worklist,
                                AbstractHTML<SimpleRuleGuard> ahtml) {
        assert item.type == WorkItemType.SIMPLE_CHECK;

        if (item.node.isRoot())
            return;

        AHTMLTree parent = item.node.getParent();

        Collection<CSSClass> nodeClasses = item.node.getCSSClasses();

        for (CSSClass c : item.changedClasses) {
            for (RuleAddClass<SimpleRuleGuard> r
                    : ahtml.getAddClassDownRules(c)) {

                SimpleRuleGuard g = r.getGuard();

                if (nodeClasses.containsAll(g.getDownClasses()) &&
                    passesGuardUpFlat(parent, g)) {

                    Collection<CSSClass> addcs = r.getRhsClasses();
                    worklist.addClasses(parent,
                                        addcs,
                                        item.node,
                                        r);
                    parent.addCSSClasses(addcs);
                }
            }
        }

    }


    /**
     * @param node a node of the tree
     * @param g a simple rule guard
     * @return true if the node matches the up and flat components of the guard
     */
    private boolean passesGuardUpFlat(AHTMLTree node, SimpleRuleGuard g) {
        Collection<CSSClass> cs = g.getUpClasses();

        AHTMLTree parent = node.getParent();
        if (   (node.isRoot() && !cs.isEmpty())
            || (!node.isRoot() && !parent.getCSSClasses().containsAll(cs)))
            return false;

        cs = g.getFlatClasses();
        if (!node.getCSSClasses().containsAll(cs))
            return false;

        return true;
    }


    /**
     * @param node a node of the tree
     * @param g a rule guard
     * @return  the child that satisfies the guard, or null if not satisfied, or
     * node if the down guard is empty
     */
    private AHTMLTree passesGuardDown(AHTMLTree node,
                                    SimpleRuleGuard g) {
        Collection<CSSClass> cs = g.getDownClasses();

        if (cs.isEmpty())
            return node;

        for (AHTMLTree child : node.getChildren()) {
            if (child.getCSSClasses().containsAll(cs))
                return child;
        }

        return null;
    }

    /**
     * Does a class adding check work item -- finds all classes "c" that can be
     * added to the current node as the result of adding children and what not.
     * That is, positive answer to the class adding problem that uses moped for
     * its solution.
     *
     * @param item the worklist item for the check
     * @param worklist the worklist
     * @param info the class adding info by getClassAddingInfo
     * @param ahtml the ahtml being analysed
     */
    private void doClassAddingCheck(WorkItem item,
                                    Worklist worklist,
                                    ClassAddingChecker classAdder,
                                    AbstractHTML<SimpleRuleGuard> ahtml)
    throws Exception {
        // is there a better way than this?
        for (CSSClass c : ahtml.getRhsClasses()) {
            if (!item.node.hasCSSClass(c)) {
                ClassAddingChecker.CheckResult res
                    = classAdder.check(item.node, c);
                if(res.getResult()) {
                    worklist.addClass(item.node, c, res.getJustification());
                    item.node.addCSSClass(c);
                }
            }
        }
    }



    /**
     * Go through the saturated tree and find which classes are redundant
     * and/or undefined.
     *
     * @param fullTree whether redundant means does not appear on the root or
     * does not appear anywhere in any derivable tree
     * @param tree the saturated tree (from worklist alg above)
     * @param checker a class adding checker (can be null if not full tree)
     * @param ahtml the ahtml
     * @param witnessClass a class to produce a trace to if addable (or null)
     * @param witnessInfo the witness info object
     * @return the set of redundant css classes
     */
    private AnalysisResult constructResult(boolean fullTree,
                                           AHTMLTree tree,
                                           ClassAddingChecker checker,
                                           AbstractHTML<SimpleRuleGuard> ahtml,
                                           CSSClass witnessClass,
                                           Worklist worklist)
    throws Exception {
        Set<CSSClass> redundancies;
        Set<CSSClass> undefinedClasses;

        Collection<CSSClass> comparisonClasses = ahtml.getFileClasses();
        if (comparisonClasses == null)
            comparisonClasses = ahtml.getClasses();

        if (!fullTree) {
            redundancies = new HashSet<CSSClass>(comparisonClasses);
            redundancies.removeAll(tree.getCSSClasses());
            redundancies.removeAll(ahtml.getHiddenClasses());

            undefinedClasses = new HashSet<CSSClass>(tree.getCSSClasses());
            undefinedClasses.removeAll(comparisonClasses);
            undefinedClasses.removeAll(ahtml.getHiddenClasses());
        } else {
            Set<CSSClass> toCheck = new HashSet<CSSClass>(ahtml.getClasses());
            toCheck.removeAll(ahtml.getHiddenClasses());

            Set<CSSClass> usedClasses = getUsedClasses(tree, ahtml);

            redundancies = new HashSet<CSSClass>(comparisonClasses);
            redundancies.removeAll(ahtml.getHiddenClasses());
            redundancies.removeAll(usedClasses);

            toCheck.addAll(redundancies);

            Collection<CSSClass> rhsClasses = ahtml.getRhsClasses();

            for (CSSClass c : toCheck) {
                if (rhsClasses.contains(c)) {
                    for (AHTMLTree node : tree.depthFirstIterator()) {
                        ClassAddingChecker.CheckResult res
                            = checker.checkFullTree(node, c);
                        if (res.getResult()) {
                            usedClasses.add(c);
                            worklist.addFullTreeClass(c, res.getJustification());
                            break;
                        }
                    }
                }
            }

            redundancies.removeAll(usedClasses);

            undefinedClasses = new HashSet<CSSClass>(usedClasses);
            undefinedClasses.removeAll(comparisonClasses);
        }

        Set<CSSClass> usedClasses = new HashSet<CSSClass>(comparisonClasses);
        usedClasses.removeAll(redundancies);
        usedClasses.removeAll(ahtml.getHiddenClasses());


        Witness witness = null;
        if (witnessClass != null && !redundancies.contains(witnessClass)) {
            witness = worklist.getWitnessInfo().getWitness(witnessClass);
        }


        // TODO: return right result
        if (redundancies.isEmpty()) {
            if (undefinedClasses.isEmpty())
                return new AnalysisResult(ResultType.NO_REDUNDANCIES,
                                          usedClasses,
                                          ahtml,
                                          checker.remopla,
                                          checker.pds,
                                          witness);
            else
                return new AnalysisResult(ResultType.NO_REDUNDANCIES_UNDEFINED_CLASSES,
                                          usedClasses,
                                          undefinedClasses,
                                          ahtml,
                                          checker.remopla,
                                          checker.pds,
                                          witness);
        } else {
            if (undefinedClasses.isEmpty())
                return new AnalysisResult(ResultType.REDUNDANCIES,
                                          usedClasses,
                                          redundancies,
                                          ahtml,
                                          checker.remopla,
                                          checker.pds,
                                          witness);
            else
                return new AnalysisResult(ResultType.REDUNDANCIES_UNDEFINED_CLASSES,
                                          usedClasses,
                                          redundancies,
                                          undefinedClasses,
                                          ahtml,
                                          checker.remopla,
                                          checker.pds,
                                          witness);
        }
    }


    /**
     * @param tree an ahtml tree
     * @param ahtml the ahtml
     * @return the set of classes of variable classes that appear anywhere in the
     * tree and are not hidden
     */
    private Set<CSSClass> getUsedClasses(AHTMLTree tree,
                                         AbstractHTML<SimpleRuleGuard> ahtml) {
        Set<CSSClass> used = new HashSet<CSSClass>();
        for (AHTMLTree node : tree.depthFirstIterator()) {
            used.addAll(node.getCSSClasses());
        }
        used.removeAll(ahtml.getHiddenClasses());
        return used;
    }

}
