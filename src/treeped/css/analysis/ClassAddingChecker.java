/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.analysis;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import de.tum.in.wpds.Pds;

import treeped.css.Namer;
import treeped.css.representation.AHTMLTree;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.translation.HTMLToMopedTranslator;
import treeped.css.translation.TranVars;
import treeped.main.CmdOptions;
import treeped.remoplatopds.RemoplaToPDS;
import treeped.remoplatopds.StmtOptimisations;
import treeped.remoplatopds.VarManager;
import treeped.remoplatopds.RemoplaAndOptimisations;
import treeped.representation.Remopla;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaStmt;




public abstract class ClassAddingChecker {

    /**
     * Results may have justifications if setGetJustifications() flag is set
     * so we have a class to encapsulate all the result info
     */
    public class CheckResult {
        private boolean result;
        private PushdownJustification justification = null;

        public CheckResult(boolean result) {
            this.result = result;
        }

        public CheckResult(boolean result,
                           PushdownJustification justification) {
            this.result = result;
            this.justification = justification;
        }

        public boolean hasJustification() {
            return (justification != null);
        }

        public boolean getResult() {
            return result;
        }

        public PushdownJustification getJustification() {
            return justification;
        }

        public String toString() {
            return "result: " + result;
        }
    }

    static Logger logger = Logger.getLogger(ClassAddingChecker.class);

    /**
     * The ahtml being checked
     */
    protected AbstractHTML<SimpleRuleGuard> ahtml = null;

    /**
     * The pds representation of the remopla (constructed with RemoplaToPds)
     */
    protected Pds pds = null;

    /**
     * the remopla representation of the problem
     */
    protected Remopla remopla = null;

    /**
     * The label corresponding to the start of the nodeFun function (there is
     * an implicit assumption that this label is always reached with all
     * variable values (i.e. start of the loop)
     */
    protected String nodeFun = null;

    /**
     * a bdd varmanager for model checking with the pds
     */
    protected VarManager manager = null;


    /**
     * the tranvars object from the translation from remopla
     */
    protected TranVars vars = null;

    private final static Set<CSSClass> emptySet
        = Collections.unmodifiableSet(new HashSet<CSSClass>());

    private boolean getJustification = false;

    protected ClassAddingChecker() {
        // family can set up the fields themselves
    }


    /**
     * @param ahtml the ahtml
     */
    public ClassAddingChecker(AbstractHTML<SimpleRuleGuard> ahtml)
    throws Exception {
        this.ahtml = ahtml;

        HTMLToMopedTranslator tr = new HTMLToMopedTranslator();
        HTMLToMopedTranslator.RemoplaAndVars rvs
            = tr.translateSimpleHTML(ahtml, ahtml.getClasses());

        this.remopla = rvs.remopt.remopla;
        Map<RemoplaStmt, StmtOptimisations> stmtOptimisations
            = rvs.remopt.stmtOptimisations;

        this.vars = rvs.vars;
        this.pds = RemoplaToPDS.translate(this.remopla, stmtOptimisations);

        // Creates variable manager
        this.manager = new VarManager(CmdOptions.getBddPackage(),
                                      CmdOptions.getNodeNum(),
                                      CmdOptions.getCacheSize(),
                                      1, // no integers, just bools
                                      this.remopla,
                                      stmtOptimisations);

        // initial fun label
        RemoplaMethod nodeFunMethod = this.remopla.getMethod(Namer.getNodeMethodName());
        this.nodeFun = nodeFunMethod.getStmts().get(0).getLabel();
    }


    /**
     * @param getJustification whether to generate witnesses or not
     */
    public void setGetJustification(boolean getJustification) {
        this.getJustification = getJustification;
    }

    /**
     * @return true if set to generate justifications
     */
    public boolean getJustification() {
        return getJustification;
    }


    /**
     * Checks the class adding problem.  That is, starting from a single node
     * tree with labelling classes initClasses, and addClass eventually be
     * added to the node.  Also an environment is given (assumption function in
     * the paper) that says whether the (omitted, logical) parent node of the
     * single node in the tree is the root, and what classes it has labelling
     * it.
     *
     * @param addClass check if this class can eventually be added
     * @param initClasses the initial class labelling of the node
     * @param parentClasses the classes labelling the logical parent of the single node
     * @param childClasses the classes labelling a logical child of the single
     * node (have to run separately for each child if more than one)
     * @param isRoot whether the node is the root node of the logical tree
     * @return the result of the check
     */
    public CheckResult check(CSSClass addClass,
                             Collection<CSSClass> initClasses,
                             Collection<CSSClass> parentClasses,
                             Collection<CSSClass> childClasses,
                             boolean isRoot)
    throws Exception {
        if (!ahtml.getRhsClasses().contains(addClass))
            return new CheckResult(false);
        else
            return doCheck(addClass,
                           initClasses,
                           parentClasses,
                           childClasses,
                           isRoot);
    }

    /**
     * @param node a node of the tree
     * @param c a class
     * @return result of the check, holds true if it is possible for class c to
     * be added to node via some rules that add children
     */
    public CheckResult check(AHTMLTree node, CSSClass c)
    throws Exception {
        Collection<CSSClass> parentClasses = emptySet;
        if (!node.isRoot())
            parentClasses = node.getParent().getCSSClasses();

        if (node.isLeaf()) {
            CheckResult res = check(c,
                                    node.getCSSClasses(),
                                    parentClasses,
                                    emptySet,
                                    false);

            if (res.getResult() && res.hasJustification()) {
                res.getJustification().setNode(node);
            }

            return res;
        } else {
            for (AHTMLTree child : node.getChildren()) {
                CheckResult res = check(c,
                                        node.getCSSClasses(),
                                        parentClasses,
                                        child.getCSSClasses(),
                                        false);
                if (res.getResult()) {
                    if (res.hasJustification()) {
                        res.getJustification().setNode(node);
                        res.getJustification().setChild(child);
                    }
                    return res;
                }
            }
            return new CheckResult(false);
        }
    }

    /**
     * Checks a variant of the class adding problem.  That is, starting from a
     * single node tree with labelling classes initClasses, and addClass
     * eventually be added to some node in a tree derivable from the current
     * one.  Also an environment is given (assumption function in the paper)
     * that says whether the (omitted, logical) parent node of the single node
     * in the tree is the root, and what classes it has labelling it.
     *
     * Note: doesn't check whether class could be added at the root node, since
     * it is already assumed that we know everything about the root node from
     * calls to check().
     *
     * @param addClass check if this class can eventually be added
     * @param initClasses the initial class labelling of the node
     * @param parentClasses the classes labelling the logical parent of the single node
     * @param childClasses the classes labelling a logical child of the single
     * node (have to run separately for each child if more than one)
     * @param isRoot whether the node is the root node of the logical tree
     * @return a check result holding true iff addClass can be added anywhere in
     * the subtree generated
     */
    public CheckResult checkFullTree(CSSClass addClass,
                                     Collection<CSSClass> initClasses,
                                     Collection<CSSClass> parentClasses,
                                     Collection<CSSClass> childClasses,
                                     boolean isRoot)
    throws Exception {
        if (!ahtml.getRhsClasses().contains(addClass))
            return new CheckResult(false);
        else
            return doCheckFullTree(addClass,
                                   initClasses,
                                   parentClasses,
                                   childClasses,
                                   isRoot);
    }




    /**
     * @param node a node of the tree
     * @param c a class
     * @return checkresult holding true if it is possible for class c to be
     * added to some node in a tree derivable from the node via some rules that
     * add children
     */
    public CheckResult checkFullTree(AHTMLTree node, CSSClass c)
    throws Exception {
        Collection<CSSClass> parentClasses = emptySet;
        if (!node.isRoot())
            parentClasses = node.getParent().getCSSClasses();

        if (node.isLeaf()) {
            CheckResult res = checkFullTree(c,
                                            node.getCSSClasses(),
                                            parentClasses,
                                            emptySet,
                                            false);

            if (res.getResult() && res.hasJustification()) {
                res.getJustification().setNode(node);
            }

            return res;
        } else {
            for (AHTMLTree child : node.getChildren()) {
                CheckResult res = checkFullTree(c,
                                                node.getCSSClasses(),
                                                parentClasses,
                                                child.getCSSClasses(),
                                                false);
                if (res.getResult()) {
                    if (res.hasJustification()) {
                        res.getJustification().setNode(node);
                        res.getJustification().setChild(child);
                    }
                    return res;
                }
            }
            return new CheckResult(false);
        }
    }

    /**
     * @param justification a pushdown justification for a class adding returned
     * by the class adder
     * @return the list of pushdown rules witnessing the addition
     */
    public abstract List<PushdownRule> getWitness(PushdownJustification justification);


    /**
     * As check but provides implementation for case when addClass is a class
     * appearing on the rhs of a rule (i.e. could be added)
     */
    protected abstract
    CheckResult doCheck(CSSClass addClass,
                        Collection<CSSClass> initClasses,
                        Collection<CSSClass> parentClasses,
                        Collection<CSSClass> childClasses,
                        boolean isRoot)
    throws Exception;

    /**
     * As checkFullTree but provides implementation for case when addClass is a
     * class appearing on the rhs of a rule (i.e. could be added)
     */
    protected abstract
    CheckResult doCheckFullTree(CSSClass addClass,
                                Collection<CSSClass> initClasses,
                                Collection<CSSClass> parentClasses,
                                Collection<CSSClass> childClasses,
                                boolean isRoot)
    throws Exception;


}
