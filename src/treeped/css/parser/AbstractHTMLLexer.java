// Generated from src/treeped/css/parser/AbstractHTML.g4 by ANTLR 4.2.2
package treeped.css.parser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AbstractHTMLLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__23=1, T__22=2, T__21=3, T__20=4, T__19=5, T__18=6, T__17=7, T__16=8, 
		T__15=9, T__14=10, T__13=11, T__12=12, T__11=13, T__10=14, T__9=15, T__8=16, 
		T__7=17, T__6=18, T__5=19, T__4=20, T__3=21, T__2=22, T__1=23, T__0=24, 
		ID=25, WS=26;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"'Up'", "'Up+'", "'AddClass('", "'True'", "'And'", "'Up*'", "')'", "','", 
		"'node'", "'<'", "'='", "'Down'", "'Down*'", "'Or'", "'</'", "'{'", "'>'", 
		"'/>'", "'AddChild('", "'}'", "'class'", "'CSS Classes:'", "'Down+'", 
		"'\"'", "ID", "WS"
	};
	public static final String[] ruleNames = {
		"T__23", "T__22", "T__21", "T__20", "T__19", "T__18", "T__17", "T__16", 
		"T__15", "T__14", "T__13", "T__12", "T__11", "T__10", "T__9", "T__8", 
		"T__7", "T__6", "T__5", "T__4", "T__3", "T__2", "T__1", "T__0", "ID", 
		"WS"
	};


	public AbstractHTMLLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "AbstractHTML.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\34\u00be\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3"+
		"\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3"+
		"\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\20"+
		"\3\21\3\21\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30"+
		"\3\30\3\30\3\31\3\31\3\32\3\32\7\32\u00a4\n\32\f\32\16\32\u00a7\13\32"+
		"\3\33\6\33\u00aa\n\33\r\33\16\33\u00ab\3\33\3\33\3\33\3\33\3\33\3\33\7"+
		"\33\u00b4\n\33\f\33\16\33\u00b7\13\33\3\33\3\33\5\33\u00bb\n\33\3\33\3"+
		"\33\2\2\34\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33"+
		"\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\3\2"+
		"\7\4\2C\\c|\5\2\62;C\\c|\5\2\13\f\16\17\"\"\3\2,,\3\2\61\61\u00c2\2\3"+
		"\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2"+
		"\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31"+
		"\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2"+
		"\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2"+
		"\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\3\67\3\2\2\2\5:\3\2\2\2\7>\3\2\2"+
		"\2\tH\3\2\2\2\13M\3\2\2\2\rQ\3\2\2\2\17U\3\2\2\2\21W\3\2\2\2\23Y\3\2\2"+
		"\2\25^\3\2\2\2\27`\3\2\2\2\31b\3\2\2\2\33g\3\2\2\2\35m\3\2\2\2\37p\3\2"+
		"\2\2!s\3\2\2\2#u\3\2\2\2%w\3\2\2\2\'z\3\2\2\2)\u0084\3\2\2\2+\u0086\3"+
		"\2\2\2-\u008c\3\2\2\2/\u0099\3\2\2\2\61\u009f\3\2\2\2\63\u00a1\3\2\2\2"+
		"\65\u00ba\3\2\2\2\678\7W\2\289\7r\2\29\4\3\2\2\2:;\7W\2\2;<\7r\2\2<=\7"+
		"-\2\2=\6\3\2\2\2>?\7C\2\2?@\7f\2\2@A\7f\2\2AB\7E\2\2BC\7n\2\2CD\7c\2\2"+
		"DE\7u\2\2EF\7u\2\2FG\7*\2\2G\b\3\2\2\2HI\7V\2\2IJ\7t\2\2JK\7w\2\2KL\7"+
		"g\2\2L\n\3\2\2\2MN\7C\2\2NO\7p\2\2OP\7f\2\2P\f\3\2\2\2QR\7W\2\2RS\7r\2"+
		"\2ST\7,\2\2T\16\3\2\2\2UV\7+\2\2V\20\3\2\2\2WX\7.\2\2X\22\3\2\2\2YZ\7"+
		"p\2\2Z[\7q\2\2[\\\7f\2\2\\]\7g\2\2]\24\3\2\2\2^_\7>\2\2_\26\3\2\2\2`a"+
		"\7?\2\2a\30\3\2\2\2bc\7F\2\2cd\7q\2\2de\7y\2\2ef\7p\2\2f\32\3\2\2\2gh"+
		"\7F\2\2hi\7q\2\2ij\7y\2\2jk\7p\2\2kl\7,\2\2l\34\3\2\2\2mn\7Q\2\2no\7t"+
		"\2\2o\36\3\2\2\2pq\7>\2\2qr\7\61\2\2r \3\2\2\2st\7}\2\2t\"\3\2\2\2uv\7"+
		"@\2\2v$\3\2\2\2wx\7\61\2\2xy\7@\2\2y&\3\2\2\2z{\7C\2\2{|\7f\2\2|}\7f\2"+
		"\2}~\7E\2\2~\177\7j\2\2\177\u0080\7k\2\2\u0080\u0081\7n\2\2\u0081\u0082"+
		"\7f\2\2\u0082\u0083\7*\2\2\u0083(\3\2\2\2\u0084\u0085\7\177\2\2\u0085"+
		"*\3\2\2\2\u0086\u0087\7e\2\2\u0087\u0088\7n\2\2\u0088\u0089\7c\2\2\u0089"+
		"\u008a\7u\2\2\u008a\u008b\7u\2\2\u008b,\3\2\2\2\u008c\u008d\7E\2\2\u008d"+
		"\u008e\7U\2\2\u008e\u008f\7U\2\2\u008f\u0090\7\"\2\2\u0090\u0091\7E\2"+
		"\2\u0091\u0092\7n\2\2\u0092\u0093\7c\2\2\u0093\u0094\7u\2\2\u0094\u0095"+
		"\7u\2\2\u0095\u0096\7g\2\2\u0096\u0097\7u\2\2\u0097\u0098\7<\2\2\u0098"+
		".\3\2\2\2\u0099\u009a\7F\2\2\u009a\u009b\7q\2\2\u009b\u009c\7y\2\2\u009c"+
		"\u009d\7p\2\2\u009d\u009e\7-\2\2\u009e\60\3\2\2\2\u009f\u00a0\7$\2\2\u00a0"+
		"\62\3\2\2\2\u00a1\u00a5\t\2\2\2\u00a2\u00a4\t\3\2\2\u00a3\u00a2\3\2\2"+
		"\2\u00a4\u00a7\3\2\2\2\u00a5\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\64"+
		"\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a8\u00aa\t\4\2\2\u00a9\u00a8\3\2\2\2\u00aa"+
		"\u00ab\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00bb\3\2"+
		"\2\2\u00ad\u00ae\7\61\2\2\u00ae\u00af\7,\2\2\u00af\u00b5\3\2\2\2\u00b0"+
		"\u00b4\n\5\2\2\u00b1\u00b2\7,\2\2\u00b2\u00b4\n\6\2\2\u00b3\u00b0\3\2"+
		"\2\2\u00b3\u00b1\3\2\2\2\u00b4\u00b7\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b5"+
		"\u00b6\3\2\2\2\u00b6\u00b8\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b8\u00b9\7,"+
		"\2\2\u00b9\u00bb\7\61\2\2\u00ba\u00a9\3\2\2\2\u00ba\u00ad\3\2\2\2\u00bb"+
		"\u00bc\3\2\2\2\u00bc\u00bd\b\33\2\2\u00bd\66\3\2\2\2\b\2\u00a5\u00ab\u00b3"+
		"\u00b5\u00ba\3\2\3\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}