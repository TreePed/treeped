/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.parser;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import treeped.css.parser.AbstractHTMLParser.ProgContext;
import treeped.css.representation.AHTMLTree;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.RuleAddChild;
import treeped.css.representation.RuleAddClass;
import treeped.css.representation.RuleAndGuard;
import treeped.css.representation.RuleDownGuard;
import treeped.css.representation.RuleDownPlusGuard;
import treeped.css.representation.RuleGuard;
import treeped.css.representation.RuleOrGuard;
import treeped.css.representation.RuleUpGuard;
import treeped.css.representation.RuleUpPlusGuard;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.representation.RuleUpStarGuard;
import treeped.css.representation.RuleDownStarGuard;


public class AbstractHTMLReader {

    static Logger logger = Logger.getLogger(AbstractHTMLReader.class);

    public static AbstractHTML<RuleGuard> loadFromString(String s)
    throws IOException {
        return loadFromReader(new StringReader(s));
    }

    public static AbstractHTML<RuleGuard> loadFromReader(Reader r)
    throws IOException {
        return loadFromCharStream(new ANTLRInputStream(r));
    }


    public static AbstractHTML<RuleGuard> loadFromFile(String htmlFile)
    throws IOException {
        ANTLRFileStream reader = new ANTLRFileStream(htmlFile);
        return loadFromCharStream(reader);
    }

    private static AbstractHTML<RuleGuard> loadFromCharStream(CharStream s) {
        AbstractHTMLLexer lexer = new AbstractHTMLLexer(s);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        AbstractHTMLParser parser = new AbstractHTMLParser(tokens);
        ProgContext tree = parser.prog(); // parse

        ParseTreeWalker walker = new ParseTreeWalker(); // create standard walker
        ParserListener extractor = new ParserListener();
        walker.walk(extractor, tree);

        return extractor.getHTML();
    }

    private static class ParserListener extends AbstractHTMLBaseListener {
        AbstractHTML<RuleGuard> html = new AbstractHTML<RuleGuard>();

        public AbstractHTML<RuleGuard> getHTML() {
            return html;
        }

        public void exitAddClass(AbstractHTMLParser.AddClassContext ctx) {
            Set<CSSClass> changeClassSet
                = getClassSet(ctx.changeClasses);

            RuleGuard g = getRuleGuard(ctx.guard);

            html.addRule(new RuleAddClass<RuleGuard>(g, changeClassSet));
        }

        public void exitAddChild(AbstractHTMLParser.AddChildContext ctx) {
            Set<CSSClass> changeClassSet
                = getClassSet(ctx.changeClasses);

            RuleGuard g = getRuleGuard(ctx.guard);

            html.addRule(new RuleAddChild<RuleGuard>(g, changeClassSet));
        }

        public void exitAHTMLTree(AbstractHTMLParser.AHTMLTreeContext ctx) {
            AHTMLTree tree = getAHTMLTree(ctx.rootNode);
            html.setTree(tree);
        }

        public void exitCSSClasses(AbstractHTMLParser.CSSClassesContext ctx) {
            Set<CSSClass> classSet = new HashSet<CSSClass>();

            for (TerminalNode tn : ctx.classSet().ID()) {
                classSet.add(new CSSClass(tn.getText()));
            }

            html.setFileClasses(classSet);
        }


        private RuleGuard getRuleGuard(AbstractHTMLParser.AbshtmlguardContext ctx) {
            RuleGuardExtractor e = new RuleGuardExtractor();
            return e.extract(ctx);
        }

        private
        AHTMLTree getAHTMLTree(AbstractHTMLParser.AbshtmltreeContext ctx) {
            AHTMLTree node;
            if (ctx.nodeclassdecl() != null) {
                Set<CSSClass> cs = getNodeClasses(ctx.nodeclassdecl());
                node = new AHTMLTree(cs);
                html.addClasses(cs);
            } else {
                node = new AHTMLTree();
            }

            for (AbstractHTMLParser.AbshtmltreeContext cctx
                    : ctx.abshtmltree()) {
                AHTMLTree child = getAHTMLTree(cctx);
                node.add(child);
            }

            return node;
        }

        // same as getClassSet, should really unify
        private
        Set<CSSClass> getNodeClasses(
                          AbstractHTMLParser.NodeclassdeclContext ctx
                      ) {
            Set<CSSClass> classSet = new HashSet<CSSClass>();

            for (TerminalNode tn : ctx.ID()) {
                classSet.add(new CSSClass(tn.getText()));
            }

            return classSet;
        }
    }

    private static class RuleGuardExtractor extends AbstractHTMLBaseListener {
        private RuleGuard result;

        public RuleGuardExtractor() { }

        public RuleGuard extract(AbstractHTMLParser.AbshtmlguardContext ctx) {
            ctx.enterRule(this);
            return getResult();
        }

        public void enterSimpleGuard(AbstractHTMLParser.SimpleGuardContext ctx) {
            Set<CSSClass> ups = null;
            Set<CSSClass> flats = null;
            Set<CSSClass> downs = null;

            if (ctx.up != null)
                ups = getClassSet(ctx.up.guardClasses);

            if (ctx.flat != null)
                flats = getClassSet(ctx.flat.guardClasses);

            if (ctx.down != null)
                downs = getClassSet(ctx.down.guardClasses);

            setResult(new SimpleRuleGuard(ups, flats, downs));
        }

        public
        void enterDownPlusGuard(AbstractHTMLParser.DownPlusGuardContext ctx) {
            ctx.abshtmlguard().enterRule(this);
            setResult(new RuleDownPlusGuard(getResult()));
        }

        public
        void enterUpPlusGuard(AbstractHTMLParser.UpPlusGuardContext ctx) {
            ctx.abshtmlguard().enterRule(this);
            setResult(new RuleUpPlusGuard(getResult()));
        }

        public
        void enterDownStarGuard(AbstractHTMLParser.DownStarGuardContext ctx) {
            ctx.abshtmlguard().enterRule(this);
            setResult(new RuleDownStarGuard(getResult()));
        }

        public
        void enterUpStarGuard(AbstractHTMLParser.UpStarGuardContext ctx) {
            ctx.abshtmlguard().enterRule(this);
            setResult(new RuleUpStarGuard(getResult()));
        }

        public
        void enterUpGuard(AbstractHTMLParser.UpGuardContext ctx) {
            ctx.abshtmlguard().enterRule(this);
            setResult(new RuleUpGuard(getResult()));
        }

        public
        void enterDownGuard(AbstractHTMLParser.DownGuardContext ctx) {
            ctx.abshtmlguard().enterRule(this);
            setResult(new RuleDownGuard(getResult()));
        }

        public
        void enterAndGuard(AbstractHTMLParser.AndGuardContext ctx) {
            List<RuleGuard> subfs = new LinkedList<RuleGuard>();
            for (AbstractHTMLParser.AbshtmlguardContext subc : ctx.abshtmlguard()) {
                subc.enterRule(this);
                subfs.add(getResult());
            }
            setResult(new RuleAndGuard(subfs));
        }

        public
        void enterOrGuard(AbstractHTMLParser.OrGuardContext ctx) {
            List<RuleGuard> subfs = new LinkedList<RuleGuard>();
            for (AbstractHTMLParser.AbshtmlguardContext subc : ctx.abshtmlguard()) {
                subc.enterRule(this);
                subfs.add(getResult());
            }
            setResult(new RuleOrGuard(subfs));
        }

        public
        void enterTrueGuard(AbstractHTMLParser.TrueGuardContext ctx) {
            setResult(new SimpleRuleGuard());
        }

        private void setResult(RuleGuard g) {
            result = g;
        }

        private RuleGuard getResult() {
            return result;
        }

    }

    private static
    Set<CSSClass> getClassSet(AbstractHTMLParser.ClassSetContext ctx) {
        Set<CSSClass> classSet = new HashSet<CSSClass>();

        for (TerminalNode tn : ctx.ID()) {
            classSet.add(new CSSClass(tn.getText()));
        }

        return classSet;
    }


}
