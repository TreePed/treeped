/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.representation;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


public abstract class RuleFmlaListGuard extends RuleGuard {

    List<RuleGuard> subfmlas = new LinkedList<RuleGuard>();
    String prefix;

    /**
     * Super constructor for a rule of form Op { subfmla, ..., subfmla }
     *
     * @param subfmlas the sub formulas
     * @param prefix  the textual representation of Op
     */
    public RuleFmlaListGuard(List<RuleGuard> subfmlas, String prefix) {
        this.subfmlas.addAll(subfmlas);
        this.prefix = prefix;
    }


    public List<RuleGuard> getSubFormulas() {
        return Collections.unmodifiableList(subfmlas);
    }

    public Collection<CSSClass> getGuardClasses() {
        Collection<CSSClass> classes = new HashSet<CSSClass>();
        for (RuleGuard sub : subfmlas)
            classes.addAll(sub.getGuardClasses());
        return classes;
    }

    public StringBuffer toString(StringBuffer out) {
        out.append(prefix);
        out.append("{ ");
        for (RuleGuard sub : subfmlas) {
            out.append(sub);
            out.append(" ");
        }
        out.append("}");
        return out;
    }

    public boolean equals(Object o) {
        if (o instanceof RuleFmlaListGuard) {
            RuleFmlaListGuard g = (RuleFmlaListGuard)o;
            return (  getClass().equals(g.getClass())
                   && subfmlas.equals(g.subfmlas));
        }
        return false;
    }

    public int hashCode() {
        return prefix.hashCode() + subfmlas.hashCode();
    }

}


