/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


/**
 * Simple rule guards are Up, Down, Flat
 */

package treeped.css.representation;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SimpleRuleGuard extends RuleGuard {

    Set<CSSClass> upClasses = new HashSet<CSSClass>();
    Set<CSSClass> flatClasses = new HashSet<CSSClass>();
    Set<CSSClass> downClasses = new HashSet<CSSClass>();

    Set<CSSClass> allClasses = new HashSet<CSSClass>();

    /**
     * Essentially builds "true"
     */
    public SimpleRuleGuard() { }

    /**
     * A rule guard Up S1 & S2 & Down S3
     *
     * I.e. parent contains classes S1, current node classes S2, and some child
     * S3.
     *
     * @param upClasses the value of S1, can be null instead of emptyset
     * @param flatClasses the value of S2, can be null instead of emptyset
     * @param downClasses the value of S3, can be null instead of emptyset
     */
    public SimpleRuleGuard(Collection<CSSClass> upClasses,
                           Collection<CSSClass> flatClasses,
                           Collection<CSSClass> downClasses) {
        if (upClasses != null) {
            this.upClasses.addAll(upClasses);
            this.allClasses.addAll(upClasses);
        }
        if (flatClasses != null) {
            this.flatClasses.addAll(flatClasses);
            this.allClasses.addAll(flatClasses);
        }
        if (downClasses != null) {
            this.downClasses.addAll(downClasses);
            this.allClasses.addAll(downClasses);
        }
    }

    /**
     * As above but for singleton sets, can still be null for empty.
     */
    public SimpleRuleGuard(CSSClass upc,
                           CSSClass flatc,
                           CSSClass downc) {
        if (upc != null) {
            this.upClasses.add(upc);
            this.allClasses.addAll(upClasses);
        }

        if (flatc != null) {
            this.flatClasses.add(flatc);
            this.allClasses.addAll(flatClasses);
        }

        if (downc != null) {
            this.downClasses.add(downc);
            this.allClasses.addAll(downClasses);
        }
    }

    public Collection<CSSClass> getUpClasses() {
        return Collections.unmodifiableSet(upClasses);
    }

    public Collection<CSSClass> getFlatClasses() {
        return Collections.unmodifiableSet(flatClasses);
    }

    public Collection<CSSClass> getDownClasses() {
        return Collections.unmodifiableSet(downClasses);
    }

    public Collection<CSSClass> getGuardClasses() {
        return Collections.unmodifiableSet(allClasses);
    }

    /**
     * @param cs some new classes to add to the up guard
     */
    public void addUpClasses(Collection<CSSClass> cs) {
        upClasses.addAll(cs);
    }

    /**
     * @param cs some new classes to add to the flat guard
     */
    public void addFlatClasses(Collection<CSSClass> cs) {
        flatClasses.addAll(cs);
    }

    /**
     * @param cs some new classes to add to the down guard
     */
    public void addDownClasses(Collection<CSSClass> cs) {
        downClasses.addAll(cs);
    }

    public StringBuffer toString(StringBuffer out) {
        boolean space = false;

        if (!upClasses.isEmpty()) {
            out.append("Up ");
            classesToString(upClasses, out);
            space = true;
        }
        if (!flatClasses.isEmpty()) {
            if (space)
                out.append(" ");
            classesToString(flatClasses, out);
            space = true;
        }
        if (!downClasses.isEmpty()) {
            if (space)
                out.append(" ");
            out.append("Down ");
            classesToString(downClasses, out);
        }

        if (upClasses.isEmpty() &&
            flatClasses.isEmpty() &&
            downClasses.isEmpty()) {
            out.append("True");
        }

        return out;
    }

    public void accept(RuleGuardVisitor v) {
        v.visit(this);
    }


    public boolean equals(Object o) {
        if (o instanceof SimpleRuleGuard) {
            SimpleRuleGuard g = (SimpleRuleGuard)o;
            return (  upClasses.equals(g.upClasses)
                   && downClasses.equals(g.downClasses)
                   && flatClasses.equals(g.flatClasses));
        }
        return false;
    }

    public int hashCode() {
        return upClasses.hashCode() +
               flatClasses.hashCode() +
               downClasses.hashCode();
    }

}
