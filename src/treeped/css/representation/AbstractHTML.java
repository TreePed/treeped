/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// AbstractHTML.java
// author: Matthew Hague
//
// Representation of HTML as event rules


package treeped.css.representation;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import org.apache.log4j.Logger;


/**
 * An AbstractHTML object contains a set of abstract html rules and an abstract
 * html tree (that may be null if no tree specified)
 *
 * Abstract HTML may have "hidden" css classes that correspond to classes added
 * by internal transformations by the model checker.
 */
public class AbstractHTML<GuardType extends RuleGuard> {
    static Logger logger = Logger.getLogger(AbstractHTML.class);


    Set<Rule<GuardType>> rules = new HashSet<Rule<GuardType>>();
    Set<CSSClass> classes = new HashSet<CSSClass>();
    Set<CSSClass> hiddenClasses = new HashSet<CSSClass>();
    AHTMLTree tree = null;
    // the classes specified in the classes as opposed to the ones that appear
    // in the rules or html tree
    Set<CSSClass> fileClasses = null;
    // the classes that can appear on the rhs of a rule
    Set<CSSClass> rhsClasses = new HashSet<CSSClass>();

    // for fast lookup
    Map<CSSClass, Set<RuleAddClass<GuardType>>> addClassFlatRules
        = new HashMap<CSSClass, Set<RuleAddClass<GuardType>>>();
    Map<CSSClass, Set<RuleAddClass<GuardType>>> addClassUpRules
        = new HashMap<CSSClass, Set<RuleAddClass<GuardType>>>();
    Map<CSSClass, Set<RuleAddClass<GuardType>>> addClassDownRules
        = new HashMap<CSSClass, Set<RuleAddClass<GuardType>>>();

    private final Set<RuleAddClass<GuardType>> emptySet
        = Collections.unmodifiableSet(new HashSet<RuleAddClass<GuardType>>());

    public AbstractHTML() {
        // do nothing
    }

    /**
     * Build AbstractHTML object with the given set of rules and null tree
     *
     * @param rules the set of rules (creates a local copy)
     */
    public AbstractHTML(Collection<Rule<GuardType>> rules) {
        for (Rule<GuardType> r : rules)
            addRule(r);
    }

    /**
     * Build AbstractHTML object with the given set of rules and given tree
     *
     * @param rules the set of rules (creates a local copy)
     * @param tree the ahtml tree (does not copy)
     */
    public AbstractHTML(Collection<Rule<GuardType>> rules,
                        AHTMLTree tree) {
        for (Rule<GuardType> r : rules)
            addRule(r);
        this.tree = tree;
        this.fileClasses = null;
    }


    /**
     * Build AbstractHTML object with the given set of rules and given tree
     *
     * @param rules the set of rules (creates a local copy)
     * @param tree the ahtml tree (does not copy)
     * @param classes the classes defined in the style sheet
     */
    public AbstractHTML(Collection<Rule<GuardType>> rules,
                        AHTMLTree tree,
                        Collection<CSSClass> fileClasses) {
        for (Rule<GuardType> r : rules)
            addRule(r);
        this.tree = tree;
        this.fileClasses = new HashSet<CSSClass>(fileClasses);
    }


    public Collection<Rule<GuardType>> getRules() {
        return Collections.unmodifiableSet(rules);
    }

    public Collection<CSSClass> getClasses() {
        return Collections.unmodifiableSet(classes);
    }

    /**
     * @return the set of classes that may appear on the rhs of a rule
     */
    public Collection<CSSClass> getRhsClasses() {
        return Collections.unmodifiableSet(rhsClasses);
    }

    public Collection<CSSClass> getFileClasses() {
        if (fileClasses == null)
            return null;
        else
            return Collections.unmodifiableSet(fileClasses);
    }

    /**
     * @param fileClasses the classes defined in the css file (rather than
     * appearing in some rule or tree node)
     */
    public void setFileClasses(Collection<CSSClass> fileClasses) {
        if (fileClasses == null) {
            this.fileClasses = null;
        } else {
            this.fileClasses = new HashSet<CSSClass>(fileClasses);
        }
    }

    /**
     * @param c a css class
     * @return all rules whose guards are not up or down guards and contain the
     * class c
     */
    public Collection<RuleAddClass<GuardType>> getAddClassFlatRules(CSSClass c) {
        return safeLookUp(addClassFlatRules, c);
    }

    /**
     * @param c a css class
     * @return all rules whose guards are up guards and contain the class c
     */
    public Collection<RuleAddClass<GuardType>> getAddClassUpRules(CSSClass c) {
        return safeLookUp(addClassUpRules, c);
    }

    /**
     * @param c a css class
     * @return all rules whose guards are down guards and contain the class c
     */
    public Collection<RuleAddClass<GuardType>> getAddClassDownRules(CSSClass c) {
        return safeLookUp(addClassDownRules, c);
    }


    public void addRule(Rule<GuardType> r) {
        rules.add(r);
        classes.addAll(r.getGuard().getGuardClasses());
        classes.addAll(r.getRhsClasses());
        rhsClasses.addAll(r.getRhsClasses());

        final AbstractHTML<GuardType> ahtml = this;
        r.accept(new RuleVisitor<GuardType>() {
            public void visit(final RuleAddClass<GuardType> r) {
                r.getGuard().accept(new AbstractRuleGuardVisitor() {
                    public void visit(SimpleRuleGuard g) {
                        addToMap(ahtml.addClassUpRules,
                                 g.getUpClasses());

                        addToMap(ahtml.addClassFlatRules,
                                 g.getFlatClasses());

                        addToMap(ahtml.addClassDownRules,
                                 g.getDownClasses());
                    }


                    public void defaultCase(RuleGuard g) {
                        // we don't care about the other rules at the moment
                    }

                    private void addToMap(Map<CSSClass,
                                              Set<RuleAddClass<GuardType>>> map,
                                          Collection<CSSClass> cs) {
                        for (CSSClass c : cs) {
                            ahtml.mapAdd(map, c, r);
                        }
                    }
                });
            }

            public void visit(RuleAddChild<GuardType> r) {
                // we don't keep maps for these rules cos we don't need them
            }

       });
    }

    public void setTree(AHTMLTree tree) {
        this.tree = tree;
    }

    public AHTMLTree getTree() {
        return tree;
    }

    public void addClass(CSSClass c) {
        classes.add(c);
    }

    public void addClasses(Collection<CSSClass> cs) {
        classes.addAll(cs);
    }


    public void addHiddenClass(CSSClass c) {
        classes.add(c);
        hiddenClasses.add(c);
    }

    public void addHiddenClasses(Collection<CSSClass> cs) {
        classes.addAll(cs);
        hiddenClasses.addAll(cs);
    }


    public Collection<CSSClass> getHiddenClasses() {
        return Collections.unmodifiableSet(hiddenClasses);
    }

    private void mapAdd(Map<CSSClass, Set<RuleAddClass<GuardType>>> map,
                        CSSClass c,
                        RuleAddClass<GuardType> r) {
        Set<RuleAddClass<GuardType>> rs = map.get(c);
        if (rs == null) {
            rs = new HashSet<RuleAddClass<GuardType>>();
            map.put(c, rs);
        }
        rs.add(r);
    }

    /**
     * @param map the map to lookup in
     * @param c the class to look for
     * @return unmodifiable collection read from map or empty collection if not
     * found
     */
    private Collection<RuleAddClass<GuardType>>
    safeLookUp(Map<CSSClass,
                   Set<RuleAddClass<GuardType>>> map,
                   CSSClass c) {
        Set<RuleAddClass<GuardType>> rs = map.get(c);
        if (rs == null)
            return emptySet;
        else
            return Collections.unmodifiableSet(rs);
    }


    /** Writes the ahtml program to a string buffer
      *  @param out the buffer to write to
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        assert out != null;

        out.append("#classes (total) = ");
        out.append(classes.size());
        out.append("\n");
        if (fileClasses != null) {
            out.append("#classes (defined) = ");
            out.append(fileClasses.size());
            out.append("\n");
        }
        out.append("#rules = ");
        out.append(rules.size());
        out.append("\n");

        if (tree != null) {
            int num = 0;
            for (AHTMLTree n : tree.depthFirstIterator())
                num++;
            out.append("#nodes = ");
            out.append(num);
            out.append("\n");
        }

        for (Rule<GuardType> r : rules) {
            r.toString(out);
            out.append("\n");
        }

        if (tree != null)
            tree.toString(out);

        if (fileClasses != null) {
            out.append("\nCSS Classes: ");
            out.append(fileClasses);
        }

        return out;
    }


    public String toString() {
        return toString(new StringBuffer()).toString();
    }


}
