/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 *
 * Class for rule guards that have a single subformula
 */

package treeped.css.representation;

import java.util.Collection;

import org.apache.log4j.Logger;


public abstract class RuleSubFmlaGuard extends RuleGuard {

    static Logger logger = Logger.getLogger(RuleSubFmlaGuard.class);

    RuleGuard subfmla;
    String prefix;

    /**
     * Super constructor for a rule of form Op ( subfmla )
     *
     * @param subfmla the sub formula
     * @param prefix  the textual representation of Op
     */
    public RuleSubFmlaGuard(RuleGuard subfmla, String prefix) {
        this.subfmla = subfmla;
        this.prefix = prefix;
    }


    public RuleGuard getSubFormula() {
        return subfmla;
    }

    public Collection<CSSClass> getGuardClasses() {
        return subfmla.getGuardClasses();
    }

    public StringBuffer toString(StringBuffer out) {
        out.append(prefix);
        out.append(" (");
        out.append(subfmla);
        out.append(")");
        return out;
    }

    public boolean equals(Object o) {
        if (o instanceof RuleSubFmlaGuard) {
            RuleSubFmlaGuard g = (RuleSubFmlaGuard)o;
            return (  getClass().equals(g.getClass())
                   && subfmla.equals(g.subfmla));
        }
        return false;
    }

    public int hashCode() {
        return prefix.hashCode() + subfmla.hashCode();
    }
}
