/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Basic class for converting "real" HTML to abstract HTML for the static
 * analysis.  I'm going to assume that all documents are of the form:
 */


package treeped.css.translation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Collection;
import java.util.LinkedList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.log4j.Logger;
import org.mozilla.javascript.CompilerEnvirons;
import org.mozilla.javascript.IRFactory;
import org.mozilla.javascript.ast.AstRoot;
import org.mozilla.javascript.ast.AstNode;
import org.mozilla.javascript.Token;
import org.mozilla.javascript.ast.InfixExpression;
import org.mozilla.javascript.ast.NodeVisitor;
import org.mozilla.javascript.ast.FunctionCall;
import org.mozilla.javascript.ast.FunctionNode;
import org.mozilla.javascript.ast.Name;
import org.mozilla.javascript.ast.ParenthesizedExpression;
import org.mozilla.javascript.ast.PropertyGet;
import org.mozilla.javascript.ast.StringLiteral;
import org.mozilla.javascript.ast.ObjectLiteral;
import org.mozilla.javascript.ast.ObjectProperty;
import org.mozilla.javascript.ast.Assignment;
import org.mozilla.javascript.ast.VariableDeclaration;
import org.mozilla.javascript.ast.VariableInitializer;
import org.w3c.css.sac.AttributeCondition;
import org.w3c.css.sac.CombinatorCondition;
import org.w3c.css.sac.Condition;
import org.w3c.css.sac.ConditionalSelector;
import org.w3c.css.sac.DescendantSelector;
import org.w3c.css.sac.ElementSelector;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SelectorList;
import org.w3c.css.sac.SimpleSelector;
import org.w3c.dom.css.CSSPageRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSRuleList;
import org.w3c.dom.css.CSSStyleRule;
import org.w3c.dom.css.CSSStyleSheet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.DataNode;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import com.steadystate.css.parser.CSSOMParser;
import com.steadystate.css.parser.SACParserCSS3;

import treeped.css.representation.AbstractHTML;
import treeped.css.representation.AbstractRuleGuardVisitor;
import treeped.css.representation.Rule;
import treeped.css.representation.RuleGuard;
import treeped.css.representation.RuleAddClass;
import treeped.css.representation.RuleAddChild;
import treeped.css.representation.CSSClass;
import treeped.css.representation.AHTMLTree;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.Namer;
import treeped.main.CmdOptions;
import treeped.util.DoubleMultiMap;
import treeped.util.MultiMap;
import static treeped.css.representation.RuleStaticFactory.*;
import treeped.css.hacks.CSSRuleWrapper;

class TranslateJQueryException extends Exception {
    static final long serialVersionUID = 1;
    public TranslateJQueryException(String msg) {
        super(msg);
    }
}


public class RealToAbsHTMLTranslator {

    private static final String BODY_TAG = "body";
    private static final String LINK_TAG = "link";
    private static final String SCRIPT_TAG = "script";
    private static final String STYLE_TAG = "style";

    private static final String REL_ATTR = "rel";
    private static final String TYPE_ATTR = "type";
    private static final String HREF_ATTR = "href";

    private static final String STYLESHEET_REL = "stylesheet";
    private static final String STYLESHEET_TYPE = "text/css";

    // supported jQuery calls
    private static final String ADD = "add";
    private static final String ADD_BACK = "addBack";
    private static final String ADD_CLASS = "addClass";
    private static final String AFTER = "after";
    private static final String AJAX = "ajax";
    private static final String ANIMATE = "animate";
    private static final String APPEND = "append";
    private static final String ATTR = "attr";
    private static final String BIND = "bind";
    private static final String BLUR = "blur";
    private static final String CHILDREN = "children";
    private static final String CLICK = "click";
    private static final String CLOSEST = "closest";
    private static final String CSS = "css";
    private static final String DATA = "data";
    private static final String DOLLAR = "$";
    private static final String EACH = "each";
    private static final String END = "end";
    private static final String EQ = "eq";
    private static final String EXTEND = "extend";
    private static final String FADE_IN = "fadeIn";
    private static final String FADE_OUT = "fadeOut";
    private static final String FIND = "find";
    private static final String FILTER = "filter";
    private static final String FIRST = "first";
    private static final String FN = "fn";
    private static final String FOCUS = "focus";
    private static final String HAS = "has";
    private static final String HAS_CLASS = "hasClass";
    private static final String HEIGHT = "height";
    private static final String HOVER = "hover";
    private static final String HTML = "html";
    private static final String INDEX = "index";
    private static final String IS = "is";
    private static final String IS_FUNCTION = "isFunction";
    private static final String NEXT = "next";
    private static final String NEXT_ALL = "nextAll";
    private static final String NOT = "not";
    private static final String ON = "on";
    private static final String ONE = "one";
    private static final String PARENT = "parent";
    private static final String PARENTS = "parents";
    private static final String PREPEND = "prepend";
    private static final String PREV = "prev";
    private static final String PREV_ALL = "prevAll";
    private static final String PREVENT_DEFAULT = "preventDefault";
    private static final String READY = "ready";
    private static final String REMOVE = "remove";
    private static final String REMOVE_CLASS = "removeClass";
    private static final String RESIZE = "resize";
    private static final String SHOW = "show";
    private static final String SLIDE_DOWN = "slideDown";
    private static final String SLIDE_UP = "slideUp";
    private static final String STOP = "stop";
    private static final String TEXT = "text";
    private static final String TRIGGER = "trigger";
    private static final String TRIGGER_HANDLER = "triggerHandler";
    private static final String UNBIND = "unbind";
    private static final String VAL = "val";
    private static final String WIDTH = "width";

    private static final String DOCUMENT = "document";
    private static final String THIS = "this";

    private static final String CSS_RULE_INFO_DESC = "css";
    private static final String SCRIPT_INFO_DESC = "script";

    // ignore all scripts that match this regular expression
    private static final Pattern IGNORE_SCRIPT_RE = Pattern.compile(".*jquery-\\d+.\\d+.\\d+.*");


    public class FileInfo {
        String desc;
        int block;
        int lineNo;

        /**
         * @param desc some textual info
         * @param block the block number (e.g. 2nd script, 3rd style element &c)
         * @param lineNo the line no, may be relative to block beginning
         */
        public FileInfo(String desc, int block, int lineNo) {
            this.desc = desc;
            this.block = block;
            this.lineNo = lineNo;
        }

        public String toString() {
            return desc + "(" + block + "):" + lineNo;
        }
    }


    /**
     * No point declaring loads of these
     */
    private static final CSSOMParser cssParser = new CSSOMParser(new SACParserCSS3());



    private static final CSSClass anyTreeClass
        = new CSSClass(Namer.getAnyTreeClass());
    private static final CSSClass documentClass
        = new CSSClass(Namer.getDocumentClass());

    static Logger logger = Logger.getLogger(RealToAbsHTMLTranslator.class);

    private final AbstractHTML<RuleGuard> ahtml = new AbstractHTML<RuleGuard>();

    private final Map<Rule<RuleGuard>, Collection<FileInfo>> ruleInfoMap
        = new HashMap<Rule<RuleGuard>, Collection<FileInfo>>();


    /**
     * Use classes to represent whether a rule is fired
     * (see mapCSSClassToCSSRule)
     */
    private final MultiMap<CSSClass, CSSRule> cssClassToCSSRule
        = new MultiMap<CSSClass, CSSRule>();
    private final DoubleMultiMap<CSSClass,
                                 CSSRule,
                                 String> cssClassToRuleSelectors
        = new DoubleMultiMap<CSSClass, CSSRule, String>();
    /**
     * Some location info on CSSRules
     */
    private final Map<CSSRule, String> cssRuleInfo
        = new HashMap<CSSRule, String>();

    /**
     * sometimes we have bits where we can add any tree, true if we need to add
     * rules for building a tree labelled by any classes appearing in ahtml
     */
    private boolean addAnyTreeRules = false;

    /**
     * On a first pass we get all the user defined $.fn.blah functions in the
     * file
     */
    private Map<String, Set<FunctionNode>> userJQueryFunctions
        = new HashMap<String, Set<FunctionNode>>();

    /**
     * if translating inside a function
     *
     * function (args) {
     *      ...
     *      $(q).blah
     *      ...
     * }
     *
     * and we've set q to contextVarName then contextGuards is the stack of guard that is
     * "stored" in q.  ContextGuards are also stored in "this".
     *
     * The args are stored in contextArgs.
     *
     * TODO: contextArgs assumes somehow syntactic scoping and no parameter names are assigned to.  This is a hack, can it be tidier?
     *
     * Any assignments of jquery objects (not occuring in a loop or branch) that
     * are statically determinable are put in contextQueries.
     */
    private Deque<RuleGuard> contextGuards = null;
    private String contextVarId = null;
    private Map<String, AstNode> contextArgs = null;
    private Map<String, Deque<RuleGuard>> contextQueries
        = new HashMap<String, Deque<RuleGuard>>();

    /**
     * local copy of CmdOptions.getJQueryFunctions()
     */
    private Set<String> jqueryClassFuns;

    /**
     * how many script/style blocks have been translated (in lieu of filename...)
     */
    private int curScriptNumber = 0;
    private int curStyleNumber = 0;

    public RealToAbsHTMLTranslator(Document html)
            throws IOException, TranslateJQueryException {
        jqueryClassFuns = CmdOptions.getJQueryFunctions();

        doScripts(html);
        doStyles(html);

        // assuming only one...
        Elements bodies = html.getElementsByTag(BODY_TAG);
        for (Element body : bodies) {
            addBody(body);
        }

        ahtml.addHiddenClass(documentClass);

        if (getAddAnyTreeRules()) {
            addAnyTreeRules();
        }
    }

    public AbstractHTML<RuleGuard> getAbstractHTML() {
        return ahtml;
    }

    /**
     * @return a map from rules of the ahtml to the location info in html file
     */
    public Map<Rule<RuleGuard>, Collection<FileInfo>> getFileInfo() {
        return ruleInfoMap;
    }


    /**
     * @param c a css class
     * @return the collection of css rules that the class corresponds to (i.e.
     * if this class is used, the rule is matched somehow). Or null if it
     * doesn't represent anything.
     */
    public Collection<CSSRule> getCSSRulesForClass(CSSClass c) {
        return cssClassToCSSRule.get(c);
    }

    /**
     * @param c a css class representing rule r (from getCSSRulesForClass)
     * @param r a css rule
     * @return the set of selectors of r indicated as matched by use of class
     * c, or null if none
     */
    public Collection<String> getSelectorsForClassRule(CSSClass c, CSSRule r) {
        return cssClassToRuleSelectors.get(c, r);
    }

    /**
     * @param r a css rule
     * @return a string containing some info to help the user find the rule in
     * the css file or null if nothing known.
     */
    public String getCSSRuleInfo(CSSRule r) {
        return cssRuleInfo.get(r);
    }


    /**
     * Counts the number of selectors in translated (identical selectors appearing in
     * different rules are considered different)
     */
    public int getNumberOfCSSRulesAndSelectors() {
        int num = 0;
        for (CSSClass c : cssClassToRuleSelectors.keySet()) {
            for (CSSRule r : cssClassToRuleSelectors.keySet(c))
                num += cssClassToRuleSelectors.get(c, r).size();
        }
        return num;
    }

    ////////////////////////////////////////////////////////////////
    // Private methods


    /**
     * Adds to the ahtml the rules corresponding to the script in the file.
     *
     * @param html the html document
     */
    private void doScripts(Document html) {
        try {
            Collection<AstRoot> scripts = getScripts(html);

            mapUserJQueryFunctions(scripts);
            addScripts(scripts);
        } catch (IOException e) {
            logger.error("Failed to translate scripts with IOException:");
            logger.error(e);
        }
    }


    /**
     * Goes through the html and gets the AstNodes for each of the script elements.
     *
     * @param html the html document
     * @return a list of ast trees for the parser script elements
     */
    private Collection<AstRoot> getScripts(Document html)
            throws IOException {
        Collection<AstRoot> scripts = new LinkedList<AstRoot>();

        Elements htmlScripts = html.getElementsByTag(SCRIPT_TAG);
        for (Element script : htmlScripts) {
            if (script.hasAttr("src")) {
                String src = script.attr("src");
                Matcher toignore = IGNORE_SCRIPT_RE.matcher(src);
                if (toignore.matches()) {
                    logger.info("Ignoring script " + src);
                    continue;
                }

                try {
                    // try to load it as a file, don't test external js so resolve
                    // im
                    String fileName = html.baseUri() + src;

                    URL url = new URL(fileName);
                    InputStreamReader in = new InputStreamReader(url.openStream());
                    BufferedReader bin = new BufferedReader(in);

                    CompilerEnvirons env = new CompilerEnvirons();
                    env.setRecoverFromErrors(true);
                    IRFactory factory = new IRFactory(env,
                                                      new JSErrorReporter());
                    AstRoot rootNode = factory.parse(bin, null, 0);

                    scripts.add(rootNode);
                } catch (IOException e) {
                    logger.info("Ignoring script " + src);
                }
            }

            for (DataNode data : script.dataNodes()) {
                // use Rhino to get javascript AST
                // tutorial: http://ramkulkarni.com/blog/parsing-javascript-code-using-mozilla-rhino/
                CompilerEnvirons env = new CompilerEnvirons();
                env.setRecoverFromErrors(true);
                StringReader reader = new StringReader(data.getWholeData());
                IRFactory factory = new IRFactory(env, new JSErrorReporter());
                AstRoot rootNode = factory.parse(reader, null, 0);

                scripts.add(rootNode);
            }
        }

        return scripts;
    }


    /**
     * Adds rules to the abstract html representing path-insensitive analysis of
     * the given scripts
     *
     * @param scripts a list of parser javascript elements
     */
    private void addScripts(Collection<AstRoot> scripts) {
        final RealToAbsHTMLTranslator parent = this;
        for (AstRoot rootNode : scripts) {
            curScriptNumber++;
            rootNode.visit(new NodeVisitor() {
                public boolean visit(AstNode node) {
                    return parent.translateAstNode(node);
                }
            });
        }
    }


    /**
     * Translates the given AstNode, adding new rules to the ahtml
     *
     * @param node the node to translate
     * @return true iff the complete node has been translated and there's no
     * need for a visitor to delve deeper into the tree
     */
    private boolean translateAstNode(AstNode node) {
        // Note: there are loads of types, see here:
        // http://tool.oschina.net/uploads/apidocs/rhino/org/mozilla/javascript/Token.html
        switch (node.getType()) {
            case Token.CALL:
                if (this.isJQueryCall(node)) {
                    this.translateJQueryCall((FunctionCall)node);
                    return false;
                } else {
                    return true;
                }

            case Token.FUNCTION:
                this.translateFunction((FunctionNode)node,
                                        null,
                                        -1,
                                        null);
                return false;

            case Token.AND:
            case Token.ADD:
            case Token.ARRAYLIT:
            case Token.BLOCK:
            case Token.COLON:
            case Token.COMMA:
            case Token.DEC:
            case Token.DIV:
            case Token.DO:
            case Token.EMPTY:
            case Token.EXPR_RESULT:
            case Token.EXPR_VOID:
            case Token.EQ:
            case Token.FALSE:
            case Token.FOR:
            case Token.GETELEM:
            case Token.GETPROP:
            case Token.GE:
            case Token.GT:
            case Token.HOOK:
            case Token.IF:
            case Token.IN:
            case Token.INC:
            case Token.LP:
            case Token.LT:
            case Token.MOD:
            case Token.MUL:
            case Token.NAME:
            case Token.NE:
            case Token.NEG:
            case Token.NEW:
            case Token.NOT:
            case Token.NULL:
            case Token.NUMBER:
            case Token.OBJECTLIT:
            case Token.OR:
            case Token.POS:
            case Token.REGEXP:
            case Token.RETURN:
            case Token.SCRIPT:
            case Token.SHEQ:
            case Token.SHNE:
            case Token.STRING:
            case Token.SUB:
            case Token.THIS:
            case Token.TRUE:
            case Token.TYPEOF:
            case Token.WHILE:
                return true;

            case Token.ASSIGN:
                Assignment assign = (Assignment)node;
                // don't translate user defs (they appear elsewhere)
                if (isJQueryFnAssign(assign)) {
                    return false;
                } else if (isJQueryAssignableCall(assign.getRight())) {
                    translateJQueryAssign(assign.getLeft(),
                                          assign.getRight());
                    return false;
                } else {
                    clobberAssign(assign.getLeft());
                    return true;
                }

            case Token.ASSIGN_ADD:
            case Token.ASSIGN_SUB:
                assign = (Assignment)node;
                clobberAssign(assign.getLeft());
                return true;

            case Token.VAR:
                if (node instanceof VariableDeclaration)
                    return true;
                else if (node instanceof VariableInitializer) {
                    VariableInitializer i = (VariableInitializer)node;
                    AstNode inode = i.getInitializer();
                    if (inode != null) {
                        if (isJQueryAssignableCall(inode)) {
                            translateJQueryAssign(i.getTarget(), inode);
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                } else {
                    // carry on regardless
                    return true;
                }

            default:
                // ignore things we don't recognise and carry on
                // regardless
                logger.error("Unrecognised node type in translateAstNode: " +
                             node.debugPrint());
                return false;
        }
    }

    /**
     * "Detects" jQuery function calls by looking for an initial $ call.
     *
     * @param n the function call node of the AST
     * @return true if f is a chain of jquery function calls (beginning with $)
     */
    private boolean isJQueryCall(AstNode n) {
        if (n.getType() != Token.CALL)
            return false;

        FunctionCall f = (FunctionCall)n;

        AstNode target = f.getTarget();
        while (target.getType() != Token.NAME) {
            switch (target.getType()) {
            case Token.CALL:
                target = ((FunctionCall)target).getTarget();
                break;
            case Token.GETPROP:
                target = ((PropertyGet)target).getTarget();
                break;

            case Token.FUNCTION:
            case Token.LP:
            case Token.NEW:
            case Token.REGEXP:
            case Token.SETELEM:
                // known failures
                return false;

            default:
                logger.error("Don't know how to determine if jquery call," +
                             "saying no for " +
                             f.debugPrint() +
                             " got as far as " +
                             target.debugPrint());
                return false;
            }
        }

        String id = ((Name)target).getIdentifier();

        return DOLLAR.equals(id) || isJQueryVariable(id);
    }


    /**
     * "Detects" jQuery function calls that resolve to a jQuery object
     *
     * @param n the function call node of the AST
     * @return true if f is a chain of jquery function calls returning a jQuery
     * object
     */
    private boolean isJQueryAssignableCall(AstNode n) {
        if (n.getType() != Token.CALL)
            return false;

        FunctionCall f = (FunctionCall)n;

        AstNode target = f.getTarget();
        while (target.getType() != Token.NAME) {
            switch (target.getType()) {
            case Token.CALL:
                target = ((FunctionCall)target).getTarget();
                break;
            case Token.GETPROP:
                PropertyGet pg = (PropertyGet)target;


                // catch functions we know don't return jQuery
                Name name = pg.getProperty();
                String funId = name.getIdentifier();
                if (EXTEND.equals(funId)) {
                    return false;
                }

                target = pg.getTarget();

                break;

            case Token.NEW:
                // known failures
                return false;

            default:
                logger.error("Don't know how to determine if jquery call," +
                             "saying no for " +
                             f.debugPrint() +
                             " got as far as " +
                             target.debugPrint());
                return false;
            }
        }

        String id = ((Name)target).getIdentifier();

        return DOLLAR.equals(id) || isJQueryVariable(id);
    }

    /**
     * @param id the name of a variable
     * @return true if it is eventually bound to a jquery stack
     */
    private boolean isJQueryVariable(String id) {
        if (contextQueries != null && contextQueries.containsKey(id))
            return true;

        if (contextArgs != null && contextArgs.containsKey(id)) {
            AstNode n = contextArgs.get(id);
            switch (n.getType()) {
                case Token.CALL:
                    return isJQueryCall(n);
                case Token.NAME:
                    return isJQueryVariable(((Name)n).getIdentifier());
                default:
                    logger.error("Don't know how to determine if jquery var: " +
                                 n.debugPrint());
                    return false;
            }
        }

        return false;
    }


    /**
     * Pull rules out of a function.  If the rules have "no context" e.g.
     * translations of
     *
     *     $('li.goofy').blah.addClass("highlight")
     *
     * which will match the same tree nodes in all cases, then add the rule to
     * the ahtml.  For rules with context, in particular
     *
     *     function (query) {
     *         $(query).blah.addClass("highlight")
     *     }
     *
     * then the rule for the addClass the part of the guard corresponding to
     * query needs to be passed as the second argument to this function so that
     * a complete rule can be formed.
     *
     * @param f the function node to translate
     * @param contextGuards guard stack corresponding to query above, or null if
     * @param thisArgIndex the context variable will be "this" but if thisArgIndex > -1 then the thisArgIndex-th param will also be a context var (unless the function doesn't have a thisArgIndex-th argument, then only this will be used)
     * @param args the arguments passed to the function or null if none
     */
    private void translateFunction(FunctionNode f,
                                   Deque<RuleGuard> contextGuards,
                                   int thisArgIndex,
                                   List<AstNode> args) {
        Deque<RuleGuard> oldContextGuards = null;
        String oldContextVarId = null;
        Map<String, AstNode> oldContextArgs = null;

        if (contextGuards != null) {
            String newContextVarId = null;

            if (thisArgIndex > -1) {
                List<AstNode> params = f.getParams();
                if (params.size() <= thisArgIndex) {
                    newContextVarId = null;
                } else {
                    AstNode param = params.get(thisArgIndex);
                    if (param.getType() != Token.NAME) {
                        logger.error("Function to be translated as context function, but argument " +
                                     thisArgIndex +
                                     " is not a simple name!  Ignoring: " +
                                     f.debugPrint());
                        return;
                    }

                    newContextVarId = ((Name)param).getIdentifier();
                }
            } else {
                newContextVarId = null;
            }

            oldContextGuards = this.contextGuards;
            this.contextGuards = new ArrayDeque<RuleGuard>(contextGuards);
            oldContextVarId = this.contextVarId;
            this.contextVarId = newContextVarId;
        }

        if (args != null) {
            oldContextArgs = this.contextArgs;
            if (oldContextArgs != null)
                this.contextArgs = new HashMap<String, AstNode>(oldContextArgs);
            else
                this.contextArgs = new HashMap<String, AstNode>();
            List<AstNode> params = f.getParams();

            if (params.size() != args.size()) {
                logger.error("Mismatch in params and args length: " +
                             params +
                             " vs. " +
                             args);
                return;
            }

            Iterator<AstNode> pi = params.iterator();
            Iterator<AstNode> ai = args.iterator();
            while (pi.hasNext()) {
                AstNode param = pi.next();
                AstNode arg = ai.next();
                if (param.getType() != Token.NAME) {
                    logger.error("Don't know how to assign a non-name parameter: " +
                                 param.debugPrint());
                    return;
                }

                String id = ((Name)param).getIdentifier();
                switch (arg.getType()) {
                case Token.NAME:
                    // don't keep pointers to names unless they're queries
                    String argId = ((Name)arg).getIdentifier();
                    // if we'd make a loop...
                    if (this.contextArgs.containsKey(argId)) {
                        if (oldContextArgs != null &&
                            oldContextArgs.containsKey(argId)) {
                            this.contextArgs.put(id, oldContextArgs.get(argId));
                        } else if (this.contextQueries.containsKey(argId)) {
                            this.contextArgs.put(id, arg);
                        }
                    }
                    break;

                default:
                    this.contextArgs.put(id, arg);
                }
            }
        }

        Map<String, Deque<RuleGuard>> oldContextQueries = this.contextQueries;
        this.contextQueries
            = new HashMap<String, Deque<RuleGuard>>(oldContextQueries);

        final RealToAbsHTMLTranslator parent = this;
        f.getBody().visit(new NodeVisitor() {
            public boolean visit(AstNode node) {
                return parent.translateAstNode(node);
            }
        });

        if (contextGuards != null) {
            this.contextGuards = oldContextGuards;
            this.contextVarId = oldContextVarId;
        }

        if (args != null) {
            this.contextArgs = oldContextArgs;
        }

        this.contextQueries = oldContextQueries;
    }


    /**
     * Deal with jQuery function call, from a statement something like
     *
     *     $("blah").parent().next()
     *
     * @param call the function call object
     * @return a stack of guards for an abstract html rule, the top being for
     * the complete rule, and the lower ones corresponding to jQuery's internal
     * stack (e.g. guard for blah, guard for parent, guard for next on top)
     */
    private Deque<RuleGuard> translateJQueryCall(FunctionCall call) {
        Deque<RuleGuard> subgs = null;
        try {
            AstNode target = call.getTarget();
            List<AstNode> args = call.getArguments();

            if (target.getType() == Token.NAME) {
                String id = ((Name)target).getIdentifier();
                // this has to be $ to be of the expected form
                if (DOLLAR.equals(id)) {
                    subgs = translateDollar(args);
                } else {
                    throw new TranslateJQueryException("Base not dollar or known jquery variable.");
                }
            } else if (target.getType() == Token.GETPROP) {
                PropertyGet pg = (PropertyGet)target;
                Name name = pg.getProperty();
                String funId = name.getIdentifier();

                subgs = translateJQueryCallTarget(pg.getTarget());

                if (isClassFun(funId)) {
                    CSSClass c = new CSSClass(Namer.getJQueryCallClass(funId));
                    RuleGuard g = subgs.peek();
                    addRule(new RuleAddClass<RuleGuard>(g, c), call);
                }

                // first deal with function calls that lead to rules being added
                if (ADD_CLASS.equals(funId)) {
                    translateAddClassCall(args, subgs, call);
                } else if (AFTER.equals(funId)) {
                    translateAfterCall(args, subgs, call);
                } else if (APPEND.equals(funId) || PREPEND.equals(funId)) {
                    translateAppendCall(args, subgs, call);
                } else if (AJAX.equals(funId)) {
                    translateAjaxCall(args, subgs);
                } else if (BIND.equals(funId)) {
                    translateBindCall(args, subgs, call);
                } else if (BLUR.equals(funId)) {
                    translateGenericActionCall(args, subgs);
                } else if (EACH.equals(funId)) {
                    translateEachCall(args, subgs);
                } else if (FOCUS.equals(funId)) {
                    translateGenericActionCall(args, subgs);
                } else if (HOVER.equals(funId)) {
                    translateHoverCall(args, subgs);
                } else if (HTML.equals(funId)) {
                    translateHtmlCall(args, subgs, call);
                } else if (READY.equals(funId)) {
                    translateReadyCall(args, subgs);
                } else if (REMOVE_CLASS.equals(funId)) {
                    // deliberately don't do anything with the remove class call

                // Now do the filtering function calls
                } else if (ADD.equals(funId)) {
                    translateAddCall(args, subgs);
                } else if (ADD_BACK.equals(funId)) {
                    RuleGuard subg = subgs.pop();
                    RuleGuard g = or(subg, subgs.peek());
                    subgs.push(subg);
                    subgs.push(g);
                } else if (CHILDREN.equals(funId)) {
                    translateChildrenCall(args, subgs);
                } else if (CLICK.equals(funId)) {
                    translateClickCall(args, subgs);
                } else if (CLOSEST.equals(funId)) {
                    translateClosestCall(args, subgs);
                } else if (END.equals(funId)) {
                    subgs.pop();
                } else if (EQ.equals(funId)) {
                    subgs.push(subgs.peek());
                } else if (FILTER.equals(funId)) {
                    // ignore filter...
                    subgs.push(subgs.peek());
                } else if (FIND.equals(funId)) {
                    RuleGuard findGuard = getGuardFromCSSSelectorArgs(args);
                    if (findGuard == null)
                        throw new TranslateJQueryException("Bad args to find.");
                    subgs.push(and(findGuard, upPlus(subgs.peek())));
                } else if (FIRST.equals(funId)) {
                    // ignore first...
                    subgs.push(subgs.peek());
                } else if (HAS.equals(funId)) {
                    RuleGuard hasGuard = getGuardFromCSSSelectorArgs(args);
                    if (hasGuard == null)
                        throw new TranslateJQueryException("Bad args to has.");
                    subgs.push(and(downPlus(hasGuard), subgs.peek()));
                } else if (NEXT.equals(funId)) {
                    translateNextCall(args, subgs);
                } else if (NEXT_ALL.equals(funId)) {
                    translateNextAllCall(args, subgs);
                } else if (NOT.equals(funId)) {
                    // ignore not...
                    subgs.push(subgs.peek());
                } else if (ON.equals(funId)) {
                    translateOnCall(args, subgs, call);
                } else if (ONE.equals(funId)) {
                    // use same as on because the semantics are the same to us
                    translateOnCall(args, subgs, call);
                } else if (PARENT.equals(funId)) {
                    translateParentCall(args, subgs);
                } else if (PARENTS.equals(funId)) {
                    RuleGuard parentsGuard = getGuardFromCSSSelectorArgs(args);
                    if (parentsGuard == null)
                        throw new TranslateJQueryException("Bad args to parents.");
                    subgs.push(and(parentsGuard, downPlus(subgs.peek())));
                } else if (PREV.equals(funId)) {
                    translatePrevCall(args, subgs);
                } else if (PREV_ALL.equals(funId)) {
                    translatePrevAllCall(args, subgs);
                } else if (RESIZE.equals(funId)) {
                    translateGenericActionCall(args, subgs);
                } else if (TRIGGER.equals(funId)) {
                    subgs.push(subgs.peek());
                } else if (TRIGGER_HANDLER.equals(funId)) {
                    subgs.push(subgs.peek());
                } else if (UNBIND.equals(funId)) {
                    subgs.push(subgs.peek());

                // the "do nothing" functions
                } else if (ANIMATE.equals(funId)) {
                    // ignore animate
                } else if (ATTR.equals(funId)) {
                    // ignore attr
                } else if (CSS.equals(funId)) {
                    // ignore data
                } else if (DATA.equals(funId)) {
                    // ignore data
                } else if (FADE_IN.equals(funId)) {
                    // ignore fade in
                } else if (FADE_OUT.equals(funId)) {
                    // ignore fade in
                } else if (HAS_CLASS.equals(funId)) {
                    // ignore hasClass
                } else if (HEIGHT.equals(funId)) {
                    // ignore height
                } else if (INDEX.equals(funId)) {
                    // ignore index
                } else if (IS.equals(funId)) {
                    // ignore is
                } else if (IS_FUNCTION.equals(funId)) {
                    // ignore is function
                } else if (PREVENT_DEFAULT.equals(funId)) {
                    // ignore preventDefault
                } else if (REMOVE.equals(funId)) {
                    // ignore remove
                } else if (SHOW.equals(funId)) {
                    // ignore fade in
                } else if (SLIDE_DOWN.equals(funId)) {
                    // ignore fade in
                } else if (SLIDE_UP.equals(funId)) {
                    // ignore fade in
                } else if (STOP.equals(funId)) {
                    // ignore fade in
                } else if (TEXT.equals(funId)) {
                    // ignore fade in
                } else if (VAL.equals(funId)) {
                    // ignore val
                } else if (WIDTH.equals(funId)) {
                    // ignore width

                // Special jQuery calls that we should ignore but perhaps should
                // handle better
                } else if (EXTEND.equals(funId)) {
                    subgs.push(trueGuard());
                } else {
                    // maybe it was user defined
                    Collection<FunctionNode> userDefs
                        = getUserJQueryFunction(funId);

                    if (userDefs == null) {
                        throw new TranslateJQueryException("Unrecognised fun name " +
                                                           funId + ".");
                    } else {
                        for (FunctionNode fn : userDefs)
                            translateFunction(fn, subgs, -1, args);
                    }
                }
            } else {
                throw new TranslateJQueryException("Unrecognised token type.");
            }

            return subgs;
        } catch (TranslateJQueryException e) {
            logger.error("Could not translate call (" +
                         e.getMessage() + ") " +
                         call.debugPrint());
            logger.error("Pushing true.");
            if (subgs == null)
                subgs = new ArrayDeque<RuleGuard>();
            subgs.push(trueGuard());
            return subgs;
        }
    }


    private Deque<RuleGuard> translateDollar(List<AstNode> args)
            throws TranslateJQueryException {
        Deque<RuleGuard> subgs = new ArrayDeque<RuleGuard>();

        // first try see if it's just a ready call
        FunctionNode farg = getSingleFunctionArg(args);
        if (farg != null) {
            translateFunction(farg, null, -1, null);
            return makeTrueStack();
        }


        // either guard will be stored here or subgs will be loaded
        RuleGuard g;
        // Assume here that dollar implicitly clears all context
        if (args.size() == 0) {
            // think it should be true...
            subgs.push(trueGuard());
        } else {
            AstNode argLast = args.get(args.size() - 1);

            Name argVar = getNameFromNode(argLast);
            if (argVar != null) {
                String varId = argVar.getIdentifier();
                if (DOCUMENT.equals(varId)) {
                    subgs.push(flat(documentClass));
                // if this use context...
                } else if (THIS.equals(varId) && (contextGuards != null)) {
                    subgs.addAll(contextGuards);
                } else if (varId.equals(contextVarId)) {
                    subgs.addAll(contextGuards);
                } else if (contextQueries.containsKey(varId)) {
                    subgs.addAll(contextQueries.get(varId));
                } else {
                    subgs.push(trueGuard());
                }
            } else if (argLast.getType() == Token.THIS &&
                       (contextGuards != null)) {
                subgs.addAll(contextGuards);
            } else {
                g = getGuardFromCSSSelector(argLast);
                if (g == null)
                    throw new TranslateJQueryException("Bad CSS selector in $ arg");
                subgs.push(g);
            }
        }

        if (args.size() == 2) {
            // refine context with selector first arg
            RuleGuard g2 = getGuardFromCSSSelector(args.get(0));
            if (g2 == null)
                throw new TranslateJQueryException("Bad CSS selector in $ arg");

            // if we loaded a guard into g
            subgs.push(and(g2, subgs.peek()));
        } else if (args.size() > 2) {
            throw new TranslateJQueryException("Too many args to $");
        }

        return subgs;
    }

    /**
     * @param node the node corresponding to the target of a jquery call
     * @return the guard stack corresponding to the translation of the node
     */
    private Deque<RuleGuard> translateJQueryCallTarget(AstNode node) {
        switch (node.getType()) {
            case Token.CALL:
                return translateJQueryCall((FunctionCall)node);

            case Token.NAME:
                String id = ((Name)node).getIdentifier();

                if (DOLLAR.equals(id))
                    return makeTrueStack();
                else if (isJQueryVariable(id)) {
                    return translateJQueryVariable(id);
                } else {
                    // don't know about this name
                    return makeTrueStack();
                }

            case Token.GETPROP:
                // don't know how to get properties
                return makeTrueStack();

            default:
                logger.error("Unknown token in jQuery guard: " +
                             node.debugPrint() +
                             "\nReturning true.");
                return makeTrueStack();
        }
    }

    /**
     * @param id the name of a variable for which isJQueryVariable holds
     * @return the jquery stack associated to the variable
     */
    private Deque<RuleGuard> translateJQueryVariable(String id) {
        if (contextQueries != null && contextQueries.containsKey(id))
            return new ArrayDeque<RuleGuard>(contextQueries.get(id));

        if (contextArgs != null && contextArgs.containsKey(id)) {
            AstNode n = contextArgs.get(id);
            switch (n.getType()) {
                case Token.CALL:
                    return translateJQueryCall((FunctionCall)n);
                case Token.NAME:
                    return translateJQueryVariable(((Name)n).getIdentifier());
                default:
                    logger.error("Failed to translateJQueryVariable: " +
                                 n.debugPrint());
                    return null;
            }
        }

        return null;
    }




    /**
     * @param args the args to the add class call
     * @param subgs the rule guard context stack of the jquery
     * @param call the node of the call
     */
    private void translateAddClassCall(List<AstNode> args,
                                       Deque<RuleGuard> subgs,
                                       AstNode call)
            throws TranslateJQueryException {
        RuleGuard g = subgs.peek();

        Set<CSSClass> newClasses = getCSSClassesFromArgs(args);
        if (newClasses == null)
            throw new TranslateJQueryException("Bad classes in addclass");

        RuleAddClass<RuleGuard> r
            = new RuleAddClass<RuleGuard>(g, newClasses);

        addRule(r, call);
    }


    /**
     * @param args a list of arguments to a function
     * @return if args is a single string arg, pull a set of css classes out of
     * it (e.g. an arg to addClass and return the set.  Else null
     */
    private Set<CSSClass> getCSSClassesFromArgs(List<AstNode> args) {
        String cssDesc = getSingleStringArg(args);
        if (cssDesc == null)
            return null;

         String[] classes = cssDesc.split("[. ]");
         Set<CSSClass> s = new HashSet<CSSClass>();
         for (int i = 0; i < classes.length; ++i) {
            // remove anything like :first
            String c = classes[i].replaceAll(":.*", "");
            if (!c.isEmpty())
                s.add(makeClassCSSClass(c));
         }

         return s;
    }

    /**
     * @param args the args to the append call
     * @param subgs the rule guard context stack of the jquery
     * @param call the node the call is at
     */
    private void translateAfterCall(List<AstNode> args,
                                    Deque<RuleGuard> subgs,
                                    AstNode call)
            throws TranslateJQueryException {
        RuleGuard g = subgs.peek();
        translateGenericAddTree(down(g), args, call);
    }


    /**
     * @param g a guard saying where to add the tree below
     * @param args the args to the add call
     * @param call the call of the function
     */
    private void translateGenericAddTree(RuleGuard g,
                                         List<AstNode> args,
                                         AstNode call) {
        String appendHtml = getSingleHtmlStringArg(args);
        if (appendHtml == null) {
            // if not a constant, overapproximate by appending anything
            addAnyTreeAtGuardRules(g, call);
        } else {
            // TODO: translate string to HTML and add rules to create it
            addCreateTreeFromHtmlRules(g, appendHtml, call);
        }
    }

    /**
     * @param args the args to the append call
     * @param subgs the rule guard context stack of the jquery
     * @param call the node the call is at
     */
    private void translateAppendCall(List<AstNode> args,
                                     Deque<RuleGuard> subgs,
                                     AstNode call)
            throws TranslateJQueryException {
        RuleGuard g = subgs.peek();
        translateGenericAddTree(g, args, call);
    }


    /**
     * @param args a list of arguments
     * @return null if args.size()!=1 or cannot extract a fixed html string from
     * the first arg
     */
    private String getSingleHtmlStringArg(List<AstNode> args) {
        if (args.size() != 1)
            return null;
        else
            return getHtmlStringFromArg(args.get(0));
    }


    /**
     * @param arg a argument
     * @return a constant html string or null if one cannot be extracted
     */
    private String getHtmlStringFromArg(AstNode arg) {
        switch (arg.getType()) {
        case Token.STRING:
            return getStringFromNode(arg);

        case Token.NUMBER:
            return "";

        case Token.MUL:
            // can't multiply HTML!
            return "";

        case Token.SUB:
            // can't subtract HTML!
            return "";

        case Token.LP:
            ParenthesizedExpression lp = (ParenthesizedExpression)arg;
            return getHtmlStringFromArg(lp.getExpression());

        case Token.ADD:
            InfixExpression e = (InfixExpression)arg;
            String lhs = getHtmlStringFromArg(e.getLeft());
            if (lhs == null)
                return null;

            String rhs = getHtmlStringFromArg(e.getRight());
            if (rhs == null)
                return null;

            return lhs + rhs;


        case Token.NAME:
            String id = ((Name)arg).getIdentifier();
            if (contextArgs != null && contextArgs.containsKey(id)) {
                AstNode val = contextArgs.get(id);
                if (val.getType() != Token.NAME) {
                    return getHtmlStringFromArg(contextArgs.get(id));
                }
            }
            return CmdOptions.getVariableHtml() ?  null : "";

        case Token.GETELEM:
        case Token.GETPROP:
            return CmdOptions.getVariableHtml() ?  null : "";

        case Token.CALL:
            if (isAssumedTextCall(arg) &&
                !CmdOptions.getVariableHtml()) {
                return "";
            } else {
                return null;
            }

        default:
            return null;
        }
    }


    /**
     * @param args the args to the add() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateAddCall(List<AstNode> args,
                                  Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        if (args.size() > 0) {
            RuleGuard argGuard = getGuardFromCSSSelectorArgs(args);
            if (argGuard == null)
                throw new TranslateJQueryException("Bad args to add().");
            subgs.push(or(argGuard, subgs.peek()));
        } else {
            subgs.push(up(subgs.peek()));
        }
    }


    /**
     * @param args the args to the children() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateChildrenCall(List<AstNode> args,
                                       Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        if (args.size() > 0) {
            RuleGuard argGuard = getGuardFromCSSSelectorArgs(args);
            if (argGuard == null)
                throw new TranslateJQueryException("Bad args to children.");
            subgs.push(and(argGuard, up(subgs.peek())));
        } else {
            subgs.push(up(subgs.peek()));
        }
    }


    /**
     * @param args the args to the bind() call
     * @param subgs the rule guard context stack of the jquery
     * @param callLine the ast node containing the call for warning reporting
     */
    private void translateBindCall(List<AstNode> args,
                                   Deque<RuleGuard> subgs,
                                   AstNode callLine)
            throws TranslateJQueryException {

        if (args.size() > 0) {
            FunctionNode farg = getFunctionFromNode(args.get(args.size() - 1));
            if (farg != null) {
                translateFunction(farg, subgs, -1, null);
            } else {
                ObjectLiteral obj = getSingleObjectLiteralArg(args);
                if (obj != null) {
                    translateEventsInObjectLit(obj, subgs);
                } else {
                    logger.warn("Don't recognise bind arguments, assuming no translation needed of call to bind in " + callLine.debugPrint());
                }
            }
        } else {
            throw new TranslateJQueryException("Call to bind() with no arguments.");
        }
    }


    private void translateEventsInObjectLit(ObjectLiteral arg,
                                            Deque<RuleGuard> subgs) {
        // Assume here that all properties that are functions on the right may
        // be called, and that other things can be ignored (this is not true,
        // e.g. context:, but fine for the example we have so far...
        for (ObjectProperty p : arg.getElements()) {
        AstNode rhs = p.getRight();
            if (rhs.getType() == Token.FUNCTION) {
                translateFunction((FunctionNode)rhs, subgs, -1, null);
            }
        }
    }


    /**
     * @param args the args to the click() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateClickCall(List<AstNode> args,
                                    Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        FunctionNode farg = getSingleFunctionArg(args);

        if (farg == null)
            throw new TranslateJQueryException("Bad fun arg in click.");

        // if function has no arguments, carry "this" through
        // using def of each as taking f(index, ele)
        translateFunction(farg, subgs, -1, null);
    }

    /**
     * @param args the args to the on() call
     * @param subgs the rule guard context stack of the jquery
     * @param call the ast node containing the call for warning reporting
     */
    private void translateOnCall(List<AstNode> args,
                                 Deque<RuleGuard> subgs,
                                 AstNode call)
            throws TranslateJQueryException {

        switch (args.size()) {
        case 3:
            RuleGuard g = getGuardFromCSSSelector(args.get(1));
            if (g == null)
                throw new TranslateJQueryException("on call has selector " + args.get(1) + " that is not understood.");

            subgs.push(and(g, upPlus(subgs.peek())));

            // fall through to case 2 for handling function call
        case 2:
            AstNode fn = args.get(args.size() - 1);
            FunctionNode farg = getFunctionFromNode(fn);

            if (farg == null) {
                logger.warn("Don't know how to translate function argument in call to on(e), assuming ok to ignore: " + call.debugPrint());
            } else {
                translateFunction(farg, subgs, 0, null);
            }
            break;

        default:
            throw new TranslateJQueryException("Don't know what to do with a call to on with neither 2 nor 3 arguments");
        }
    }



    /**
     * @param args the args to the ajax() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateAjaxCall(List<AstNode> args,
                                   Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        ObjectLiteral arg = getSingleObjectLiteralArg(args);
        translateEventsInObjectLit(arg, subgs);
    }


    /**
     * @param args the args to the html() call
     * @param subgs the rule guard context stack of the jquery
     * @param call the ast node of the html call
     */
    private void translateHtmlCall(List<AstNode> args,
                                   Deque<RuleGuard> subgs,
                                   AstNode call)
            throws TranslateJQueryException {

        // TODO: be smarter and detect when the arg is a literal that we can put
        // in directly

        // if we're adding something, allow it to add anything
        if (args.size() > 0) {
            RuleGuard g = subgs.peek();
            translateGenericAddTree(g, args, call);
        } else {
            // jQuery just returns the html, so do nothing
        }
    }




    /**
     * Translates any function, such as focus() that just takes a function to
     * fire on event.
     *
     * @param args the args to the call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateGenericActionCall(List<AstNode> args,
                                            Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        if (args.size() != 1)
            throw new TranslateJQueryException("focus has wrong number of args");

        // we assume the hover functions are actually being attached to
        // something (this overapproximates since the guard might not match
        // anything in reality)
        AstNode arg = args.get(0);
        if (arg.getType() == Token.FUNCTION) {
            translateFunction((FunctionNode)arg, subgs, -1, null);
        }
    }

    /**
     * @param args the args to the hover() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateHoverCall(List<AstNode> args,
                                    Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        if (args.size() != 1 && args.size() != 2)
            throw new TranslateJQueryException("hover has wrong number of args");

        // we assume the hover functions are actually being attached to
        // something (this overapproximates since the guard might not match
        // anything in reality)
        for (AstNode arg : args) {
            if (arg.getType() == Token.FUNCTION) {
                translateFunction((FunctionNode)arg, subgs, -1, null);
            }
        }
    }


    /**
     * @param args the args to the each() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateEachCall(List<AstNode> args,
                                   Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        FunctionNode farg = null;
        if (args.size() == 1) {
            farg = getSingleFunctionArg(args);
        } else if (args.size() == 2) {
            farg = getFunctionFromNode(args.get(1));
        } else {
            throw new TranslateJQueryException("Each call with more than two arguments, don't know what to do.");
        }

        if (farg == null)
            throw new TranslateJQueryException("Bad fun in each.");

        // if function has no arguments, carry "this" through
        // using def of each as taking f(index, ele)
        translateFunction(farg, subgs, 1, null);
    }



    /**
     * @param args the args to the ready() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateReadyCall(List<AstNode> args,
                                    Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        FunctionNode farg = getSingleFunctionArg(args);
        if (farg == null)
            throw new TranslateJQueryException("Bad fun in each.");

        translateFunction(farg, subgs, -1, null);
    }


    /**
     * @param args the args to the next() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateNextCall(List<AstNode> args,
                                   Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        translateSiblingSelector(args, subgs);
    }

    /**
     * @param args the args to the nextAll() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateNextAllCall(List<AstNode> args,
                                      Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        translateSiblingSelector(args, subgs);
    }


    /**
     * @param args the args to the prev() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translatePrevCall(List<AstNode> args,
                                   Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        translateSiblingSelector(args, subgs);
    }

    /**
     * @param args the args to the prevAll() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translatePrevAllCall(List<AstNode> args,
                                      Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        translateSiblingSelector(args, subgs);
    }

    /**
     *
     * @param args the args to the next/prev/nextAll/prevAll call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateSiblingSelector(List<AstNode> args,
                                          Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        switch (args.size()) {
        case 0:
            subgs.push(up(down(subgs.peek())));
            break;

        case 1:
            RuleGuard g = getGuardFromCSSSelector(args.get(0));
            subgs.push(and(g, up(down(subgs.peek()))));
            break;

        default:
            throw new TranslateJQueryException("Don't know how to translate a sibling selector with more than one argument.");

        }
    }

    /**
     * @param args the args to the closest() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateClosestCall(List<AstNode> args,
                                      Deque<RuleGuard> subgs)
            throws TranslateJQueryException {

        RuleGuard g = getGuardFromCSSSelectorArgs(args);
        subgs.push(and(g, downStar(subgs.peek())));
    }


    /**
     * @param args the args to the parent() call
     * @param subgs the rule guard context stack of the jquery
     */
    private void translateParentCall(List<AstNode> args,
                                     Deque<RuleGuard> subgs)
            throws TranslateJQueryException {
        switch(args.size()) {
        case 0:
            subgs.push(down(subgs.peek()));
            break;

        case 1:
            RuleGuard g = getGuardFromCSSSelectorArgs(args);
            if (g == null)
                throw new TranslateJQueryException("Could not get selector from parent() argument: " + args.get(0));
            subgs.push(and(down(subgs.peek()), g));
            break;

        default:
            throw new TranslateJQueryException("Call to parent with unexpected number of arguments: " + args);
        }
    }

    /**
     * @param g the guard to apply before adding any calls
     * @param call the node the call is at
     */
    private void addAnyTreeAtGuardRules(RuleGuard g, AstNode call) {
        addRule(new RuleAddClass<RuleGuard>(g, anyTreeClass), call);
        setAddAnyTreeRules(true);
    }


    private void addAnyTreeRules() {
        ahtml.addHiddenClass(anyTreeClass);

        RuleGuard any = flat(anyTreeClass);
        addRule(new RuleAddChild<RuleGuard>(any, anyTreeClass));

        Collection<CSSClass> hiddenClasses = ahtml.getHiddenClasses();

        for (CSSClass c : ahtml.getClasses()) {
            if (!anyTreeClass.equals(c) && !hiddenClasses.contains(c)) {
                addRule(new RuleAddClass<RuleGuard>(any, c));
            }
        }
    }


    /**
     * @return a stack with the single rule "true", convenience method for
     * aborted translations...
     */
    private static Deque<RuleGuard> makeTrueStack() {
        Deque<RuleGuard> trueS = new ArrayDeque<RuleGuard>();
        trueS.push(trueGuard());
        return trueS;
    }


    /**
     * Extracts a tree from the html body and adds it to the ahtml
     *
     * @param body the body element
     */
    private void addBody(Element body) {
        AHTMLTree doc = new AHTMLTree();
        doc.addCSSClass(documentClass);
        doc.add(getTree(body));
        ahtml.setTree(doc);
    }


    /**
     * Builds a tree from a given element
     */
    private AHTMLTree getTree(Element node) {
        AHTMLTree tree = new AHTMLTree();

        tree.addCSSClass(makeTagCSSClass(node.tagName()));

        if (!node.id().isEmpty())
            tree.addCSSClass(makeIdCSSClass(node.id()));

        for (String className : node.classNames()) {
            if (!className.isEmpty())
                tree.addCSSClass(makeClassCSSClass(className));
        }

        for (Element child : node.children()) {
            tree.add(getTree(child));
        }

        return tree;
    }

    private CSSClass makeTagCSSClass(String tag) {
        return new CSSClass(Namer.getTagClassName(tag));
    }

    private CSSClass makeIdCSSClass(String id) {
        return new CSSClass(Namer.getIDClassName(id));
    }

    /**
     * For making ahtml classes for css classes
     */
    private CSSClass makeClassCSSClass(String c) {
        return new CSSClass(Namer.getClassClassName(c));
    }


    /**
     * Tries to get a string literal argument.
     *
     * @param args the list of arguments to a function
     * @return null if there is not only one argument, or if there is only one,
     * it's not a string literal.  If there is only one and it is a string
     * literal, return the value of that string
     */
    private String getSingleStringArg(List<AstNode> args) {
        if (args.size() != 1)
            return null;
        else
            return getStringFromNode(args.get(0));
    }

    /**
     * @param node an ast node to try and get a string from
     * @return the value of the string if the node is a string constant, null
     * otherwise (will search through args and resolve addition)
     */
    private String getStringFromNode(AstNode node) {
        switch (node.getType()) {
        case Token.STRING:
            return ((StringLiteral)node).getValue();

        case Token.ADD:
            InfixExpression e = (InfixExpression)node;
            String lhs = getStringFromNode(e.getLeft());
            if (lhs == null)
                return null;

            String rhs = getStringFromNode(e.getRight());
            if (rhs == null)
                return null;

            return lhs + rhs;

        case Token.NAME:
            String id = ((Name)node).getIdentifier();
            if (contextArgs != null &&
                contextArgs.containsKey(id)) {
                AstNode val = contextArgs.get(id);
                if (val.getType() != Token.NAME)
                    return getStringFromNode(val);
                else
                    return null;
            } else {
                return null;
            }

        default:
            return null;
        }
    }

    /**
     * Tries to get a name argument.
     *
     * @param args the list of arguments to a function
     * @return null if there is not only one argument, or if there is only one,
     * it's not a name.  If there is only one and it is a name, return the name
     */
    private static Name getSingleNameArg(List<AstNode> args) {
        if (args.size() != 1)
            return null;
        else
            return getNameFromNode(args.get(0));
    }

    /**
     * @param node an ast node to try and get a name from
     * @return the name if the node is a name, null otherwise
     */
    private static Name getNameFromNode(AstNode node) {
        if (node.getType() != Token.NAME)
            return null;
        else
            return (Name)node;
    }


    /**
     * Tries to get a function node argument.
     *
     * @param args the list of arguments to a function
     * @return null if there is not only one argument, or if there is only one,
     * it's not a function.  If there is only one and it is a function,
     * return the function node
     */
    private static FunctionNode getSingleFunctionArg(List<AstNode> args) {
        if (args.size() != 1)
            return null;
        else
            return getFunctionFromNode(args.get(0));
    }

    /**
     * @param node an ast node to try and get a function from
     * @return the ast function node if the node is a function, null
     * otherwise
     */
    private static FunctionNode getFunctionFromNode(AstNode node) {
        if (node.getType() != Token.FUNCTION)
            return null;
        else
            return (FunctionNode)node;
    }

    /**
     * @param args a list of arguments to a jquery function
     * @return null if args is not exactly length 1, or if the single argument
     * is not a string literal, or if it is a literal, it can't be parsed into a
     * set of css classes.  Otherwise, return the guard corresponding to the
     * selector
     */
    private RuleGuard getGuardFromCSSArg(List<AstNode> args)
            throws TranslateJQueryException {
        if (args.size() != 1)
            return null;
        return getGuardFromCSSArg(args.get(0));
    }

    /**
     * @param arg an argument to a jquery function
     * @return null if args is not exactly length 1, or if the single argument
     * is not a string literal, or if it is a literal, it can't be parsed into a
     * set of css classes, or is not a variable that can be looked up in contextArgs.
     * Otherwise, return the guard for the css selector
     */
    private RuleGuard getGuardFromCSSArg(AstNode arg)
            throws TranslateJQueryException {
        String s = getStringFromNode(arg);
        if (s == null) {
            if (this.contextArgs == null)
                return null;
            Name v = getNameFromNode(arg);
            if (v == null)
                return null;
            AstNode nv = this.contextArgs.get(v.getIdentifier());
            if (nv == null)
                return null;
            s = getStringFromNode(nv);
        }
        return getGuardFromCSSSelector(s);
    }


    /**
     * @param cssDesc a string that is a css selector
     * @return the list of selectors in the css (i.e. the disjuncts of
     * the full selector)
     */
    private SelectorList getSelectorsFromCSSString(String cssDesc)
            throws IOException {
        StringReader reader = new StringReader(cssDesc);
        InputSource input = new InputSource(reader);
        return cssParser.parseSelectors(input);
    }

    /**
     * @param cssDesc something like "li.goofy:first"
     * @return a guard matching the css selector or null if failed
     */
    private RuleGuard getGuardFromCSSSelector(String cssDesc)
            throws TranslateJQueryException {
        try {
            SelectorList selectors = getSelectorsFromCSSString(cssDesc);

            if (selectors == null) {
                logger.error("No selectors in cssDesc: " + cssDesc);
                return null;
            }

            int len = selectors.getLength();
            if (len == 0) {
                logger.error("No selectors in cssDesc: " + cssDesc);
                return null;
            }

            RuleGuard g = getGuardFromCSSSelector(selectors.item(0));

            for (int i = 1; i < len; ++i) {
                g = or(g, getGuardFromCSSSelector(selectors.item(i)));
            }

            return g;
        } catch (IOException e) {
            logger.error("Unable to get selectors from " + cssDesc, e);
            return null;
        }
    }


    /**
     * @param sel a CSS selector
     * @return a guard for the selector, or null if fail
     */
    private RuleGuard getGuardFromCSSSelector(Selector sel)
            throws TranslateJQueryException {
        switch (sel.getSelectorType()) {
        case Selector.SAC_ANY_NODE_SELECTOR:
            return trueGuard();

        case Selector.SAC_CHILD_SELECTOR:
            DescendantSelector d = (DescendantSelector)sel;
            RuleGuard parent = getGuardFromCSSSelector(d.getAncestorSelector());
            RuleGuard node = getGuardFromCSSSelector(d.getSimpleSelector());
            return and(up(parent), node);

        case Selector.SAC_CONDITIONAL_SELECTOR:
            ConditionalSelector c = (ConditionalSelector)sel;
            RuleGuard selector = getGuardFromCSSSelector(c.getSimpleSelector());
            RuleGuard condition = getGuardFromCondition(c.getCondition());
            return and(selector, condition);

        case Selector.SAC_DESCENDANT_SELECTOR:
            d = (DescendantSelector)sel;
            parent = getGuardFromCSSSelector(d.getAncestorSelector());

            SimpleSelector simpleSel = d.getSimpleSelector();

            // pseudo selectors are "before and after" and correspond to
            // insertions below the ancestor node.  Just need to know that the
            // ancestor exists hence return that guard.
            if (simpleSel.getSelectorType()
                    == Selector.SAC_PSEUDO_ELEMENT_SELECTOR) {
                return parent;
            } else {
                node = getGuardFromCSSSelector(simpleSel);
                return and(upPlus(parent), node);
            }

        case Selector.SAC_ELEMENT_NODE_SELECTOR:
            ElementSelector e = (ElementSelector)sel;
            if (e.getLocalName() == null)
                return trueGuard();
            else
                return flat(makeTagCSSClass(e.getLocalName()));

        case Selector.SAC_PSEUDO_ELEMENT_SELECTOR:
            throw new TranslateJQueryException("getGuardFromCSSSelector() found a pseudo element selector, we should deal with these further up the syntax tree!");

        // ignore these for now
        case Selector.SAC_DIRECT_ADJACENT_SELECTOR:
        case Selector.SAC_CDATA_SECTION_NODE_SELECTOR:
        case Selector.SAC_COMMENT_NODE_SELECTOR:
        case Selector.SAC_NEGATIVE_SELECTOR:
        case Selector.SAC_ROOT_NODE_SELECTOR:
        case Selector.SAC_TEXT_NODE_SELECTOR:
        case Selector.SAC_PROCESSING_INSTRUCTION_NODE_SELECTOR:
        default:
            logger.error("Getting guard for unhandled CSS selector: " + sel);
            return null;
        }
    }


    /**
     * @param c the condition
     * @return a guard that is the condition
     */
    private RuleGuard getGuardFromCondition(Condition c) {
        switch(c.getConditionType()) {
        case Condition.SAC_AND_CONDITION:
            CombinatorCondition cc = (CombinatorCondition)c;
            RuleGuard lhs
                = getGuardFromCondition(cc.getFirstCondition());
            RuleGuard rhs
                = getGuardFromCondition(cc.getSecondCondition());
            return and(lhs, rhs);

        case Condition.SAC_ID_CONDITION:
            AttributeCondition a = (AttributeCondition)c;
            return flat(makeIdCSSClass(a.getValue()));

        case Condition.SAC_CLASS_CONDITION:
            a = (AttributeCondition)c;
            return flat(makeClassCSSClass(a.getValue()));

        case Condition.SAC_ATTRIBUTE_CONDITION:
        case Condition.SAC_PSEUDO_CLASS_CONDITION:
            // ignore
            return trueGuard();

        case Condition.SAC_BEGIN_HYPHEN_ATTRIBUTE_CONDITION:
        case Condition.SAC_CONTENT_CONDITION:
        case Condition.SAC_LANG_CONDITION:
        case Condition.SAC_NEGATIVE_CONDITION:
        case Condition.SAC_ONE_OF_ATTRIBUTE_CONDITION:
        case Condition.SAC_ONLY_CHILD_CONDITION:
        case Condition.SAC_ONLY_TYPE_CONDITION:
        case Condition.SAC_OR_CONDITION:
        case Condition.SAC_POSITIONAL_CONDITION:
        default:
            logger.error("Unhandled condition: " + c);
            return null;
        }
    }


    /**
     * @param args a list of arguments to a function
     * @return a guard corresponding to the selector of the first argument,
     *         throws exception if not a single arg or can't translate arg
     */
    private RuleGuard getGuardFromCSSSelectorArgs(List<AstNode> args)
            throws TranslateJQueryException {
        if (args.size() != 1)
            throw new TranslateJQueryException("Tried to get css selector from arg list without only a single arg.");

        return getGuardFromCSSSelector(args.get(0));
    }


    /**
     * @param arg a css selector as an argument to $()
     * @return a guard corresponding to the selector, throws exception if can't
     * be translated yet
     */
    private RuleGuard getGuardFromCSSSelector(AstNode arg)
            throws TranslateJQueryException {
        String s = getStringFromNode(arg);
        if (s == null) {
            logger.info("Non string CSS selector found, using true.");
            return trueGuard();
        }

        return getGuardFromCSSSelector(s);
    }




    /**
     * Tries to get an object literal argument.
     *
     * @param args the list of arguments to a function
     * @return null if there is not only one argument, or if there is only one,
     * it's not an object literal .  If there is only one and it is an object
     * literal, return it
     */
    private static ObjectLiteral getSingleObjectLiteralArg(List<AstNode> args) {
        if (args.size() != 1)
            return null;
        else
            return getObjectLiteralFromNode(args.get(0));
    }

    /**
     * @param node an ast node to try and get an object literal from it
     * @return the ast objectliteral if the node is one, null
     * otherwise
     */
    private static ObjectLiteral getObjectLiteralFromNode(AstNode node) {
        if (node.getType() != Token.OBJECTLIT)
            return null;
        else
            return (ObjectLiteral)node;
    }


    /**
     * @return true if the translation means that we need to add rule for
     * building an arbitrary tree
     */
    private boolean getAddAnyTreeRules() {
        return addAnyTreeRules;
    }

    /**
     * @param val true if we need to eventually add rules that can build any
     * tree
     */
    private void setAddAnyTreeRules(boolean val) {
        addAnyTreeRules = val;
    }


    /**
     * Goes through the javascript and finds statements that look like
     *
     *     $.fn.myFun = function ...
     *
     * so we can translate user defined jQuery functions.
     *
     * Results are stored in userJQueryFunctions map.
     *
     * @param scripts the asts of the parsed scripts in the file
     */
    private void mapUserJQueryFunctions(Collection<AstRoot> scripts) {
        final RealToAbsHTMLTranslator parent = this;
        for (AstRoot rootNode : scripts) {
            rootNode.visit(new NodeVisitor() {
                public boolean visit(AstNode node) {
                    switch (node.getType()) {
                        case Token.ASSIGN:
                            Assignment assign = (Assignment)node;
                            if (parent.isJQueryFnAssign(assign)) {
                                parent.addJQueryFnAssign(assign);
                            }
                            return true;

                        case Token.AND:
                        case Token.ARRAYLIT:
                        case Token.ASSIGN_ADD:
                        case Token.ASSIGN_SUB:
                        case Token.FUNCTION:
                        case Token.CALL:
                        case Token.COLON:
                        case Token.COMMA:
                        case Token.ADD:
                        case Token.BLOCK:
                        case Token.DEC:
                        case Token.DIV:
                        case Token.DO:
                        case Token.EMPTY:
                        case Token.EXPR_RESULT:
                        case Token.EXPR_VOID:
                        case Token.EQ:
                        case Token.FALSE:
                        case Token.FOR:
                        case Token.GETELEM:
                        case Token.GETPROP:
                        case Token.GE:
                        case Token.GT:
                        case Token.HOOK:
                        case Token.IF:
                        case Token.IN:
                        case Token.INC:
                        case Token.LP:
                        case Token.LT:
                        case Token.MUL:
                        case Token.MOD:
                        case Token.NAME:
                        case Token.NE:
                        case Token.NEG:
                        case Token.NEW:
                        case Token.NOT:
                        case Token.NULL:
                        case Token.NUMBER:
                        case Token.OBJECTLIT:
                        case Token.OR:
                        case Token.POS:
                        case Token.REGEXP:
                        case Token.RETURN:
                        case Token.SCRIPT:
                        case Token.SHEQ:
                        case Token.SHNE:
                        case Token.STRING:
                        case Token.SUB:
                        case Token.THIS:
                        case Token.TRUE:
                        case Token.TYPEOF:
                        case Token.WHILE:
                        case Token.VAR:
                            return true;

                        default:
                            // ignore things we don't recognise and carry on
                            // regardless
                            logger.error("Unrecognised node type: " +
                                         node.debugPrint());
                            return false;
                    }

                }
            });
        }
    }


    /**
     * @param assign a node of the ast
     * @return true if the node is an assignment $.fn.myFun = blah
     */
    private boolean isJQueryFnAssign(Assignment assign) {
        // check left is $.fn.myFunName

        AstNode left = assign.getLeft();
        if (left.getType() != Token.GETPROP)
            return false;

        PropertyGet pg1 = (PropertyGet)left;
        AstNode leftLeft = pg1.getTarget();
        if (leftLeft.getType() != Token.GETPROP)
            return false;
        PropertyGet pg2 = (PropertyGet)leftLeft;
        AstNode leftLeftLeft = pg2.getTarget();
        if (leftLeftLeft.getType() != Token.NAME)
            return false;
        if (!DOLLAR.equals(((Name)leftLeftLeft).getIdentifier()))
            return false;
        Name fn = pg2.getProperty();
        if (!FN.equals(fn.getIdentifier()))
            return false;

        return true;
    }


    /**
     * Takes a jQuery function definition and adds it to the map
     *
     * @param assign a node of the ast of form $.fn.myFun = blah
     */
    private void addJQueryFnAssign(Assignment assign) {
        PropertyGet left = (PropertyGet)assign.getLeft();
        String fnName = left.getProperty().getIdentifier();

        AstNode right = assign.getRight();
        if (right.getType() != Token.FUNCTION) {
            logger.error("Don't know how to deal with jQuery " +
                         "function definition (so setting to ignore)" +
                         assign.debugPrint());
            addJQueryFnMapping(fnName, null);
            return;
        }

        addJQueryFnMapping(fnName, (FunctionNode)right);
    }



    /**
     * Marks down that the user defined jQuery function fnName might have
     * definition fn
     *
     * @param fnName the name of the function
     * @param fn the ast node defining the function can be null if we don't know
     * what it is, in which case we just record there is a mapping
     */
    private void addJQueryFnMapping(String fnName, FunctionNode fn) {
        Set<FunctionNode> defs = userJQueryFunctions.get(fnName);
        if (defs == null) {
            defs = new HashSet<FunctionNode>();
            userJQueryFunctions.put(fnName, defs);
        }
        if (fn != null)
            defs.add(fn);
    }


    /**
     * @param fnName the name of a user defined jQuery function
     * @return a collection of possible user definitions for this function, or null if there aren't any
     */
    private Collection<FunctionNode> getUserJQueryFunction(String fnName) {
        return userJQueryFunctions.get(fnName);
    }



    /**
     * Add rules to create a given html tree at the location specified by g.  If
     * cannot interpret appendHtml as html, add any tree instead.
     *
     * @param g the guard for where to put the tree
     * @param appendHtml html string saying what subtree to add
     * @param call the node the call is at
     */
    private void addCreateTreeFromHtmlRules(RuleGuard g,
                                            String appendHtml,
                                            AstNode call) {
        try {
            Document html = Jsoup.parse(appendHtml,
                                        "http://treeped.ac.uk",
                                        Parser.xmlParser());
            for (Element e : html.children()) {
                addCreateTreeRules(g, e, call);
            }
        } catch (Exception e) {
            logger.info("Failed to create html out of html: " + appendHtml, e);
            addAnyTreeAtGuardRules(g, call);
        }
    }


    /**
     * Add rules to create the given element as a child of the element specified
     * by g
     *
     * @param g the guard giving the parent of e
     * @param e the subtree to add via new rules
     * @param call the ast node doing the create tree call
     */
    private void addCreateTreeRules(RuleGuard g,
                                    Element e,
                                    AstNode call) {
        Set<CSSClass> cs = new HashSet<CSSClass>();

        cs.add(makeTagCSSClass(e.tagName()));

        if (!e.id().isEmpty())
            cs.add(makeIdCSSClass(e.id()));

        for (String className : e.classNames()) {
            if (!className.isEmpty())
                cs.add(makeClassCSSClass(className));
        }

        Elements children = e.children();
        if (children.size() > 0) {
            CSSClass id = getNewNodeIdClass();
            cs.add(id);
            ahtml.addHiddenClass(id);
            RuleGuard pg = flat(id);
            for (Element child : children) {
                addCreateTreeRules(pg, child, call);
            }
        }

        addRule(new RuleAddChild<RuleGuard>(g, cs), call);
    }


    /**
     * When adding rules that build sub-trees we need new classes to uniquely
     * identify the nodes to add children to, use this to get one
     *
     * @return a new css class can be used as an id
     */
    int nextNewNodeId = 0;
    private CSSClass getNewNodeIdClass() {
        String name = Namer.getBuildTreeClassName(nextNewNodeId++);
        return new CSSClass(name);
    }


    /**
     * @param lhs the lhs node of the assignment
     * @param rhs the rhs node of the assignment
     * @return true if the assignment is an assignment of a jquery value to a
     * variable and doesn't occur in a loop or branch
     */
    private boolean isTrackableAssign(AstNode lhs, AstNode rhs) {
        if (lhs.getType() != Token.NAME || !isJQueryCall(rhs))
            return false;

        // check code not included in a loop or branch
        AstNode p = lhs.getParent();
        while (p != null) {
            switch (p.getType()) {
            case Token.DO:
            case Token.WHILE:
            case Token.FOR:
            case Token.IF:
            case Token.ELSE:
            case Token.IFEQ:
            case Token.IFNE:
            case Token.SWITCH:
                return false;

            default:
                // carry on
                p = p.getParent();
            }
        }

        return true;
    }

    /**
     * Translates an assignment and stores the result in the current args
     *
     * @param lhs a lhs node of assign
     * @param rhs a rhs node for which isJQueryCall is true
     */
    private void translateJQueryAssign(AstNode lhs, AstNode rhs) {
        Deque<RuleGuard> stack = translateJQueryCall((FunctionCall)rhs);

        if (lhs.getType() == Token.NAME) {
            String v = ((Name)lhs).getIdentifier();
            if (isTrackableAssign(lhs, rhs)) {
                contextQueries.put(v, stack);
            } else {
                // isn't trackable, overapproximate with true
                contextQueries.put(v, makeTrueStack());
            }
        }
    }

    /**
     * If some variable gets written over by something non-jquery, make sure we
     * don't think it's still assigned
     *
     * @param lhs what's being overwritten
     */
    private void clobberAssign(AstNode lhs) {
        if (lhs.getType() == Token.NAME) {
            String id = ((Name)lhs).getIdentifier();
            if (contextQueries != null)
                contextQueries.remove(id);
            if (contextArgs != null)
                contextArgs.remove(id);
        }
    }


    /**
     * @param f a function name
     * @return if this is on of the jquery funs trackable by -jqfun cmdoption
     */
    private boolean isClassFun(String f) {
        return jqueryClassFuns.contains(f);
    }


    /**
     * "Detects" function calls that return text values.  This may be a
     * dubious function...
     *
     * @param n the function call node of the AST
     * @return true if f is a chain of function calls that returns a text value (e.g. $("#hey").val() and variables don't contain trees :)...
     */
    private boolean isAssumedTextCall(AstNode n) {
        if (n.getType() != Token.CALL)
            return false;

        FunctionCall f = (FunctionCall)n;

        AstNode target = f.getTarget();
        while (target.getType() != Token.NAME) {
            switch (target.getType()) {
            case Token.CALL:
                target = ((FunctionCall)target).getTarget();
                break;
            case Token.GETPROP:
                target = ((PropertyGet)target).getProperty();
                break;

            default:
                logger.error("Don't know how to determine if jquery text call," +
                             "saying no for " +
                             f.debugPrint() +
                             " got as far as " +
                             target.debugPrint());
                return false;
            }
        }

        String id = ((Name)target).getIdentifier();

        return ATTR.equals(id) ||
               TEXT.equals(id) ||
               VAL.equals(id) ||
               WIDTH.equals(id) ;
    }


    /**
     * Adds an unjustified rule
     *
     * @param r new rule to add
     */
    private void addRule(Rule<RuleGuard> r) {
        ahtml.addRule(r);
    }

    /**
     * @param r the new rule of the ahtml
     * @param node the AstNode the rule comes from (null if none)
     */
    private void addRule(Rule<RuleGuard> r, AstNode node) {
        ahtml.addRule(r);
        if (node != null) {
            Collection<FileInfo> infos = ruleInfoMap.get(r);
            if (infos == null) {
                infos = new LinkedList<FileInfo>();
                ruleInfoMap.put(r, infos);
            }
            infos.add(new FileInfo(SCRIPT_INFO_DESC,
                                   curScriptNumber,
                                   node.getLineno()));
        }
    }

    /**
     * @param r the new rule of the ahtml
     * @param rule the css rule the rule comes from
     * @param selector the particular selector the rule corresponds to
     */
    private void addRule(Rule<RuleGuard> r,
                         CSSRule cssRule) {
        ahtml.addRule(r);
        // TODO: find a good way of tracking location data for witnesses
        // (though it's not so important since it's the last rule)
        if (cssRule != null) {
            Collection<FileInfo> infos = ruleInfoMap.get(r);
            if (infos == null) {
                infos = new LinkedList<FileInfo>();
                ruleInfoMap.put(r, infos);
            }

            infos.add(new FileInfo(CSS_RULE_INFO_DESC,
                                   curStyleNumber,
                                   -1));
        }
    }


    /**
     * Adds to the ahtml the rules corresponding to style sheet rule guards
     *
     * @param html the html document
     */
    private void doStyles(Document html)
            throws TranslateJQueryException {
        if (CmdOptions.getCheckCSSRules()) {
            try {
                addStyles(getStyles(html));
            } catch (IOException e) {
                logger.error("Failed to translate scripts with IOException:");
                logger.error(e);
            }
        }
    }


    /**
     * Goes through the html and gets and parses the style elements.
     *
     * @param html the html document
     * @return a list of parsed style sheets
     */
    private Collection<CSSStyleSheet> getStyles(Document html)
            throws IOException {
        Collection<CSSStyleSheet> styles = new LinkedList<CSSStyleSheet>();

        Elements htmlStyles = html.getElementsByTag(STYLE_TAG);
        for (Element style: htmlStyles) {
            for (DataNode data : style.dataNodes()) {
                StringReader reader = new StringReader(data.getWholeData());
                InputSource input = new InputSource(reader);
                styles.add(cssParser.parseStyleSheet(input, null, null));
            }
        }

        Elements htmlLinks = html.getElementsByTag(LINK_TAG);
        for (Element link: htmlLinks) {
            if (STYLESHEET_REL.equals(link.attr(REL_ATTR)) &&
                STYLESHEET_TYPE.equals(link.attr(TYPE_ATTR))) {
                String href = link.attr(HREF_ATTR);
                if (href != null && !href.isEmpty()) {
                    URL fullHref = new URL(new URL(html.baseUri()), href);
                    InputSource input = new InputSource(fullHref.toString());
                    CSSStyleSheet css
                        = cssParser.parseStyleSheet(input,
                                                    null,
                                                    html.baseUri());
                    styles.add(css);
                }
            }
        }

        return styles;
    }


    /**
     * Adds the given style sheet rules to the ahtml
     *
     * @param styles a list of parsed css elements
     */
    private void addStyles(Collection<CSSStyleSheet> styles)
            throws TranslateJQueryException {
        for (CSSStyleSheet sheet : styles) {
            curStyleNumber++;
            addStyle(sheet);
        }
    }


    /**
     * Add rules for rule guards of style sheet
     */
    private void addStyle(CSSStyleSheet sheet)
            throws TranslateJQueryException {
        CSSRuleList rules = sheet.getCssRules();
        int len = rules.getLength();
        for (int i = 0; i < len; ++i) {
            addCSSRule(rules.item(i));
        }
    }


    /**
     * Add a rule checking a css rule for redundancy
     */
    private void addCSSRule(CSSRule r) throws TranslateJQueryException {
        try {
            switch(r.getType()) {
            case CSSRule.STYLE_RULE:
                CSSStyleRule sr = (CSSStyleRule)r;
                SelectorList selectors = getSelectorsFromCSSString(sr.getSelectorText());
                for (int i = 0; i < selectors.getLength(); ++i) {
                    Selector selector = selectors.item(i);
                    RuleGuard g = getGuardFromCSSSelector(selector);
                    addCSSRuleWithGuard(g, r, selector);
                }
                break;

            case CSSRule.PAGE_RULE:
                CSSPageRule pr = (CSSPageRule)r;
                selectors = getSelectorsFromCSSString(pr.getSelectorText());
                for (int i = 0; i < selectors.getLength(); ++i) {
                    Selector selector = selectors.item(i);
                    RuleGuard g = getGuardFromCSSSelector(selector);
                    addCSSRuleWithGuard(g, r, selector);
                }
                break;

            case CSSRule.CHARSET_RULE:
            case CSSRule.FONT_FACE_RULE:
            case CSSRule.IMPORT_RULE:
            case CSSRule.MEDIA_RULE:
            case CSSRule.UNKNOWN_RULE:
            default:
                logger.warn("Ignoring css rule: " + r.getCssText());
            }
        } catch (IOException e) {
            logger.warn("CSS rule " + r.getCssText() + " could not be translated, ignoring.", e);
        }
    }



    /**
     * @param r the css rule the class is for
     * @param selector the particular selector in the rule it is for
     * @return a fresh css class for a css rule over all instances of
     * translation
     */
    private static int nextCSSRuleClass = 0;
    private CSSClass getRuleClass(CSSRule r, Selector selector) {
        CSSClass c = new CSSClass(Namer.getCSSRuleClassName(nextCSSRuleClass++));
        mapCSSClassToCSSRule(c, r, selector);
        return c;
    }


    /**
     * For adding rules relating to CSS rules.  If the guard is just a single
     * class, then don't bother adding a rule for it: just the class on its own
     * is enough.
     *
     * @param g the rule guard
     * @param cssRule the justifying css rule
     * @param selector the selector of the css rule this particular guard
     * corresponds to
     */
    private void addCSSRuleWithGuard(final RuleGuard g,
                                     final CSSRule cssRule,
                                     final Selector selector) {
        final RealToAbsHTMLTranslator self = this;
        g.accept(new AbstractRuleGuardVisitor() {

            // simple rule guards with only one class don't need a special rule
            public void visit(SimpleRuleGuard sg) {
                if (sg.getDownClasses().size() > 0 ||
                    sg.getFlatClasses().size() > 1 ||
                    sg.getUpClasses().size() > 0) {

                    makeRule(sg, cssRule, selector);
                } else {
                    CSSClass c = sg.getFlatClasses().iterator().next();
                    ahtml.addClass(c);
                    self.mapCSSClassToCSSRule(c, cssRule, selector);
                }
            }

            public void defaultCase(RuleGuard rg) {
                makeRule(rg, cssRule, selector);
            }

            private void makeRule(RuleGuard g,
                                  CSSRule cssRule,
                                  Selector selector) {
                CSSClass c = getRuleClass(cssRule, selector);
                self.addRule(new RuleAddClass<RuleGuard>(g, c),
                             cssRule);
            }
        });
    }


    /**
     * Registers that css class c can cause a match of rule r and the particular
     * selector it matches.
     *
     * @param c a css class
     * @param r a css rule
     * @param selector the particular css selector represented by the class (since rules may have multiple selectors)
     */
    private void mapCSSClassToCSSRule(CSSClass c, CSSRule r, Selector selector) {
        // wrap rule so equalities work out
        CSSRuleWrapper wrappedRule = new CSSRuleWrapper(r);
        cssClassToCSSRule.put(c, wrappedRule);
        cssClassToRuleSelectors.put(c, wrappedRule, selector.toString());
        cssRuleInfo.put(wrappedRule, wrappedRule.getUserDataString());
    }

}
