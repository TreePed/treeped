/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.translation;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import treeped.css.Namer;
import treeped.css.representation.AHTMLTree;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.Rule;
import treeped.css.representation.RuleAddChild;
import treeped.css.representation.RuleAddClass;
import treeped.css.representation.RuleAndGuard;
import treeped.css.representation.RuleDownGuard;
import treeped.css.representation.RuleDownPlusGuard;
import treeped.css.representation.RuleFmlaListGuard;
import treeped.css.representation.RuleGuard;
import treeped.css.representation.RuleGuardVisitor;
import treeped.css.representation.RuleOrGuard;
import treeped.css.representation.RuleSubFmlaGuard;
import treeped.css.representation.RuleUpGuard;
import treeped.css.representation.RuleUpPlusGuard;
import treeped.css.representation.RuleVisitor;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.representation.RuleUpStarGuard;
import treeped.css.representation.RuleDownStarGuard;

public class AHTMLToSimpleAHTML {

    static Logger logger = Logger.getLogger(AHTMLToSimpleAHTML.class);


    public static class Result {
        public AbstractHTML<SimpleRuleGuard> ahtml
            = new AbstractHTML<SimpleRuleGuard>();
        public Map<Rule<SimpleRuleGuard>,
                   Rule<RuleGuard>> originalRuleMap
                = new HashMap<Rule<SimpleRuleGuard>,
                              Rule<RuleGuard>>();
    }

    /**
     * Converts ahtml with full guards into one that only has simple guards,
     * adds new classes.  If fullTree is true, then the simple ahtml produced
     * will be such that a class appears on the root node if it could appear
     * anywhere in any tree in the original ahtml.  If fullTree is false, then
     * the classes that can label the root node are only the classes that could
     * label the root node in the original.
     *
     * Note: alternatively full tree can be built into the translation to
     * remopla and model checking algorithm.  See translation and analysis
     * classes for details.
     *
     * Note: introduces some intermediate CSS classes to the ahtml, marked as
     * "hidden".  In the full tree reduction, only the root may be labelled with
     * non-hidden classes (hidden classes are used to simulate the original
     * document, with the original classes only appearing on the root as
     * output)
     *
     * Note: probably shouldn't use the original ahtml object after translation,
     * parts of it will be in the result...
     *
     * @param ahtml a non-simple ahtml object
     * @param fullTree whether or not to produce an ahtml equivalent to full
     * tree analysis of ahtml
     * @return a simple ahtml version of the given ahtml analysis problem (i.e.
     * only simple rules, and only check the root) and a map from simple rules
     * to original rules  (see Result class)
     *
     */
    public static
    Result translate(AbstractHTML<RuleGuard> ahtml,
                     boolean fullTree) {

        final Map<RuleGuard, CSSClass> guardClasses = getGuardClasses(ahtml);
        final Map<CSSClass, CSSClass> hiddenClasses
            = fullTree ? getHiddenClasses(ahtml)
                       : null;


        final Result result = new Result();

        AHTMLTree tree = ahtml.getTree();
        renameTree(tree, hiddenClasses);
        result.ahtml.setTree(tree);

        result.ahtml.addClasses(ahtml.getClasses());
        result.ahtml.setFileClasses(ahtml.getFileClasses());
        result.ahtml.addHiddenClasses(ahtml.getHiddenClasses());

        if (fullTree) {
            for (CSSClass hidc : hiddenClasses.values())
                result.ahtml.addHiddenClass(hidc);
        }

        for (CSSClass hidc : guardClasses.values())
            result.ahtml.addHiddenClass(hidc);

        translateRules(guardClasses, hiddenClasses, result, ahtml);

        // add rules for doing full tree analysis
        if (fullTree) {
            for (CSSClass c : ahtml.getClasses()) {
                // { hidc }, AddClass(c)
                // Down c, AddClass(c)

                CSSClass hidc = hiddenClasses.get(c);

                RuleAddClass<SimpleRuleGuard> havec
                    = new RuleAddClass<SimpleRuleGuard>(
                              new SimpleRuleGuard(null, hidc, null),
                              c
                          );
                RuleAddClass<SimpleRuleGuard> downc
                    = new RuleAddClass<SimpleRuleGuard>(
                              new SimpleRuleGuard(null, null, c),
                              c
                          );

                result.ahtml.addRule(havec);
                result.ahtml.addRule(downc);
            }

        }

        return result;
    }

    private static
    Map<RuleGuard, CSSClass> getGuardClasses(AbstractHTML<RuleGuard> ahtml) {
        final Map<RuleGuard, CSSClass> guardClasses
            = new HashMap<RuleGuard, CSSClass>();

        for (Rule<RuleGuard> r : ahtml.getRules()) {
            getGuardClassesFromGuard(r.getGuard(), guardClasses, true);
        }

        return guardClasses;
    }

    /**
     * @param g guard to create new guard classes for
     * @param guardClasses map to add guard->new css class to
     * @param outer whether the formula is the complete formula for a rule (in
     * which case doesn't need a new class introducing for it) or is a
     * subformula (in which case we need to create a class for it)
     */
    private static
    void getGuardClassesFromGuard(RuleGuard g,
                                  final Map<RuleGuard, CSSClass> guardClasses,
                                  final boolean outer) {
        g.accept(new RuleGuardVisitor() {

            public void visit(RuleUpGuard g) {
                doSubFmlaRule(g, outer);
            }

            public void visit(RuleDownGuard g) {
                doSubFmlaRule(g, outer);
            }

            // transitive guards overrule outer since they're somehow appear
            // within themselves...
            public void visit(RuleUpPlusGuard g) {
                doSubFmlaRule(g, false);
            }

            // transitive guards overrule outer since they're somehow appear
            // within themselves...
            public void visit(RuleDownPlusGuard g) {
                doSubFmlaRule(g, false);
            }

            // transitive guards overrule outer since they're somehow appear
            // within themselves...
            public void visit(RuleUpStarGuard g) {
                doSubFmlaRule(g, false);
            }

            // transitive guards overrule outer since they're somehow appear
            // within themselves...
            public void visit(RuleDownStarGuard g) {
                doSubFmlaRule(g, false);
            }

            public void visit(RuleAndGuard g) {
                doListRule(g, outer);
            }

            // or guard translation always requires a class representing the rule
            // this is terrible programming, and inefficient wrt to the number of classes
            // overall best not to have or guard on the outside
            // TODO: fix this...
            public void visit(RuleOrGuard g) {
                doListRule(g, false);
            }

            public void visit(SimpleRuleGuard g) {
                if (!outer)
                    addGuardClass(g);
            }

            private void doSubFmlaRule(RuleSubFmlaGuard g, boolean outer) {
                if (!outer)
                    addGuardClass(g);
                getGuardClassesFromGuard(g.getSubFormula(),
                                         guardClasses,
                                         false);
            }

            private void doListRule(RuleFmlaListGuard g, boolean outer) {
                if (!outer)
                    addGuardClass(g);
                for (RuleGuard subg : g.getSubFormulas()) {
                    getGuardClassesFromGuard(subg,
                                             guardClasses,
                                             false);
                }
            }


            private void addGuardClass(RuleGuard g) {
                if (guardClasses.get(g) == null) {
                    String className = Namer.getRuleGuardCSSClassPrefix() +
                                       guardClasses.size();
                    guardClasses.put(g, new CSSClass(className));
                }
            }
        });
    }

    /**
     * @param ahtml an abstract html object
     * @return a map from classes appearing in ahtml to hidden versions of them
     */
    private static
    Map<CSSClass, CSSClass> getHiddenClasses(AbstractHTML<RuleGuard> ahtml) {
        Map<CSSClass, CSSClass> map = new HashMap<CSSClass, CSSClass>();
        for (CSSClass c : ahtml.getClasses()) {
            map.put(c, new CSSClass(Namer.getHiddenName(c.getName())));
        }
        return map;
    }


    /**
     * Walks the tree and renames all the classes according to renames.
     *
     * @param node a tree
     * @param renames a map to rename classes of the tree, or null if no
     * renaming
     */
    private static void renameTree(AHTMLTree node,
                                   Map<CSSClass, CSSClass> renames) {
        if (renames == null)
            return;

        Collection<CSSClass> cs = renameClasses(node.getCSSClasses(),
                                                renames);

        node.setCSSClasses(cs);

        for (AHTMLTree child : node.getChildren())
            renameTree(child, renames);
    }

    /**
     * @param cs a set of classes or null
     * @param renames a map for renaming classes, if null just
     * return cs
     * @return null if cs=null, else cs but will all elements
     * renamed
     */
    private static
    Collection<CSSClass> renameClasses(Collection<CSSClass> cs,
                                       Map<CSSClass, CSSClass> renames) {
        if (cs == null || renames == null)
            return cs;

        Set<CSSClass> newcs = new HashSet<CSSClass>();
        for (CSSClass c : cs) {
            CSSClass newc = renames.get(c);
            if (newc == null)
                newcs.add(c);
            else
                newcs.add(newc);
        }

        return newcs;
    }


    private static
    void translateRules(final Map<RuleGuard, CSSClass> guardClasses,
                        final Map<CSSClass, CSSClass> hiddenClasses,
                        final Result result,
                        final AbstractHTML<RuleGuard> ahtml) {
        for (final Rule<RuleGuard> r : ahtml.getRules()) {
            SimpleRuleGuard g = translateGuard(r.getGuard(),
                                               guardClasses,
                                               result.ahtml);

            addRule(r, g, hiddenClasses, result);
        }

    }

    private static
    SimpleRuleGuard translateGuard(
                        RuleGuard g,
                        final Map<RuleGuard, CSSClass> guardClasses,
                        final AbstractHTML<SimpleRuleGuard> simpleAHTML
                    ) {
        // defined below
        RuleGuardTranslator t = new RuleGuardTranslator(guardClasses,
                                                        simpleAHTML);
        g.accept(t);
        return t.getResult();
    }

    /**
     * Adds a rule to simpleAHTML that is a copy of r but with the
     * guard g instead
     *
     * @param r the rule to copy
     * @param g the new guard
     * @param renames a map from CSS classes to CSS classes with
     * different names, new rule will use the renamed classes on the
     * right hand side, but will not rename the classes in the guard
     * @param result the result object containing the simple ahtml
     */
    private static
    void addRule(final Rule<RuleGuard> r,
                 final SimpleRuleGuard g,
                 final Map<CSSClass, CSSClass> renames,
                 final Result result) {

        // simple rule guards just go across
        r.accept(new RuleVisitor<RuleGuard>() {
            public void visit(RuleAddChild<RuleGuard> r) {
                Collection<CSSClass> rhs = renameClasses(r.getRhsClasses(),
                                                         renames);
                Rule<SimpleRuleGuard> newr
                    = new RuleAddChild<SimpleRuleGuard>(g, rhs);
                result.ahtml.addRule(newr);
                result.originalRuleMap.put(newr, r);
            }

            public void visit(RuleAddClass<RuleGuard> r) {
                Collection<CSSClass> rhs = renameClasses(r.getRhsClasses(),
                                                         renames);
                Rule<SimpleRuleGuard> newr
                    = new RuleAddClass<SimpleRuleGuard>(g, rhs);
                result.ahtml.addRule(newr);
                result.originalRuleMap.put(newr, r);
            }
        });
    }

    private static class RuleGuardTranslator implements RuleGuardVisitor {

        Map<RuleGuard, CSSClass> guardClasses;
        AbstractHTML<SimpleRuleGuard> simpleAHTML;

        // whether we delved deep into the formula or not, if outer then
        // construct a full result, if inner, just return a css class that
        // represents the inner formula for use in further construction
        SimpleRuleGuard result;
        boolean outer = true;
        CSSClass innerClass = null;


        public RuleGuardTranslator(Map<RuleGuard, CSSClass> guardClasses,
                                   AbstractHTML<SimpleRuleGuard> simpleAHTML) {
            this.guardClasses = guardClasses;
            this.simpleAHTML = simpleAHTML;
        }

        public SimpleRuleGuard getResult() {
            return result;
        }

        public CSSClass getInnerClass() {
            return innerClass;
        }

        public void visit(SimpleRuleGuard g) {
            CSSClass gc = null;
            if (!outer) {
                gc = guardClasses.get(g);
                RuleAddClass<SimpleRuleGuard> originate
                    = new RuleAddClass<SimpleRuleGuard>(g, gc);
                simpleAHTML.addRule(originate);
            }
            setResult(g, gc);
        }

        public void visit(RuleUpGuard g) {
            doSubFmla(g.getSubFormula());
            CSSClass subc = this.getInnerClass();

            if (!outer) {
                CSSClass gc = guardClasses.get(g);

                RuleAddClass<SimpleRuleGuard> originate
                    = new RuleAddClass<SimpleRuleGuard>(
                              new SimpleRuleGuard(subc, null, null),
                              gc
                          );
                simpleAHTML.addRule(originate);
                setResult(null, gc);
            } else {
                setResult(new SimpleRuleGuard(subc, null, null), null);
            }
        }

        public void visit(RuleDownGuard g) {
            doSubFmla(g.getSubFormula());
            CSSClass subc = this.getInnerClass();

            if (!outer) {
                CSSClass gc = guardClasses.get(g);

                RuleAddClass<SimpleRuleGuard> originate
                    = new RuleAddClass<SimpleRuleGuard>(
                              new SimpleRuleGuard(null, null, subc),
                              gc
                          );
                simpleAHTML.addRule(originate);
                setResult(null, gc);
            } else {
                setResult(new SimpleRuleGuard(null, null, subc), null);
            }
        }

        public void visit(RuleAndGuard g) {
            Set<CSSClass> subs = new HashSet<CSSClass>();
            for (RuleGuard subg : g.getSubFormulas()) {
                doSubFmla(subg);
                subs.add(this.getInnerClass());
            }

            if (!outer) {
                CSSClass gc = guardClasses.get(g);
                RuleAddClass<SimpleRuleGuard> originate
                    = new RuleAddClass<SimpleRuleGuard>(
                              new SimpleRuleGuard(null, subs, null),
                              gc
                          );
                simpleAHTML.addRule(originate);
                setResult(null, gc);
            } else {
                setResult(new SimpleRuleGuard(null, subs, null), null);
            }

        }

        public void visit(RuleOrGuard g) {
            CSSClass gc = guardClasses.get(g);

            for (RuleGuard subg : g.getSubFormulas()) {
                doSubFmla(subg);
                CSSClass subc = this.getInnerClass();

                RuleAddClass<SimpleRuleGuard> originate
                    = new RuleAddClass<SimpleRuleGuard>(
                              new SimpleRuleGuard(null, subc, null),
                              gc
                          );
                simpleAHTML.addRule(originate);
            }

            setResult(new SimpleRuleGuard(null, gc, null), gc);
        }

        public void visit(RuleUpPlusGuard g) {
            CSSClass gc = guardClasses.get(g);

            RuleAddClass<SimpleRuleGuard> propagate
                = new RuleAddClass<SimpleRuleGuard>(
                          new SimpleRuleGuard(gc, null, null),
                          gc
                      );
            simpleAHTML.addRule(propagate);

            doSubFmla(g.getSubFormula());
            CSSClass subc = this.getInnerClass();

            RuleAddClass<SimpleRuleGuard> originate
                = new RuleAddClass<SimpleRuleGuard>(
                          new SimpleRuleGuard(subc, null, null),
                          gc
                      );
            simpleAHTML.addRule(originate);

            setResult(new SimpleRuleGuard(null, gc, null),
                      gc);
        }

        public void visit(RuleDownPlusGuard g) {
            CSSClass gc = guardClasses.get(g);

            RuleAddClass<SimpleRuleGuard> propagate
                = new RuleAddClass<SimpleRuleGuard>(
                          new SimpleRuleGuard(null, null, gc),
                          gc
                      );
            simpleAHTML.addRule(propagate);

            doSubFmla(g.getSubFormula());
            CSSClass subc = this.getInnerClass();

            RuleAddClass<SimpleRuleGuard> originate
                = new RuleAddClass<SimpleRuleGuard>(
                          new SimpleRuleGuard(null, null, subc),
                          gc
                      );
            simpleAHTML.addRule(originate);

            setResult(new SimpleRuleGuard(null, gc, null),
                      gc);
        }

        public void visit(RuleUpStarGuard g) {
            CSSClass gc = guardClasses.get(g);

            RuleAddClass<SimpleRuleGuard> propagate
                = new RuleAddClass<SimpleRuleGuard>(
                          new SimpleRuleGuard(gc, null, null),
                          gc
                      );
            simpleAHTML.addRule(propagate);

            doSubFmla(g.getSubFormula());
            CSSClass subc = this.getInnerClass();

            RuleAddClass<SimpleRuleGuard> originate
                = new RuleAddClass<SimpleRuleGuard>(
                          new SimpleRuleGuard(null, subc, null),
                          gc
                      );
            simpleAHTML.addRule(originate);

            setResult(new SimpleRuleGuard(null, gc, null),
                      gc);
        }

        public void visit(RuleDownStarGuard g) {
            CSSClass gc = guardClasses.get(g);

            RuleAddClass<SimpleRuleGuard> propagate
                = new RuleAddClass<SimpleRuleGuard>(
                          new SimpleRuleGuard(null, null, gc),
                          gc
                      );
            simpleAHTML.addRule(propagate);

            doSubFmla(g.getSubFormula());
            CSSClass subc = this.getInnerClass();

            RuleAddClass<SimpleRuleGuard> originate
                = new RuleAddClass<SimpleRuleGuard>(
                          new SimpleRuleGuard(null, subc, null),
                          gc
                      );
            simpleAHTML.addRule(originate);

            setResult(new SimpleRuleGuard(null, gc, null),
                      gc);
        }


        /**
         * @param g the guard to set as result if outer formula
         * @param c the class to set as representing the guard in case of !outer
         */
        private void setResult(SimpleRuleGuard g, CSSClass c) {
            if (outer)
                result = g;
            else
                innerClass = c;
        }

        /**
         * Recurse into translating a subformula
         *
         * @param subg the sub formula
         */
        private void doSubFmla(RuleGuard subg) {
            boolean oldouter = outer;
            outer = false;
            subg.accept(this);
            outer = oldouter;
        }
    }
}
