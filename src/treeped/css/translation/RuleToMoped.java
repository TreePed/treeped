/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RuleToMoped.java
// Author: Matthew Hague
//
// Translates an HTML Rule to Remopla Code in two parts:
//
//     1) The guard condition
//     2) The code to execute if the guard is satisfied

package treeped.css.translation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.apache.log4j.Logger;

import treeped.representation.*;
import static treeped.representation.RemoplaStaticFactory.*;
import treeped.css.Namer;
import treeped.css.representation.*;
import treeped.exceptions.VariableNotFoundException;

public class RuleToMoped implements RuleVisitor<SimpleRuleGuard> {

    static Logger logger = Logger.getLogger(RuleToMoped.class);

    private RemoplaExpr guard;
    private List<RemoplaStmt> stmts;

    private TranVars vars;

    private VariableNotFoundException varException = null;

    /**
     * Use translateRule to translate rules (can do multiple rules with same
     * object)
     */
    public RuleToMoped() {
        // do nothing
    }

    /**
     * Call this to translate a rule, use getGuard() and getStmts() to get the
     * result.  If called twice the previous translation is overwritten.
     *
     * If the rule is not interesting (i.e. the rhs classes don't have yvars in
     * TranVars, then we don't need any statements for this and getGuard() will
     * be null and getStmts empty;
     *
     * @param r the rule to translate
     * @param vars the tranvars object for the translation
     */
    public void translateRule(Rule<SimpleRuleGuard> r,
                              TranVars vars) throws VariableNotFoundException {
        this.vars = vars;
        this.guard = null;
        this.stmts = new ArrayList<RemoplaStmt>();

        r.accept(this);
        if (varException != null)
            throw varException;
    }

    /**
     * @return the guard on the translation -- a remopla expression, that when
     * true, means that the result of getStmt can be performed
     */
    public RemoplaExpr getGuard() {
        return guard;
    }

    /**
     * @return the remopla statements that can be executed if the getGuard guard
     * is satisfied
     */
    public List<RemoplaStmt> getStmts() {
        return stmts;
    }


    //////////////////////////////////////////////////////
    // Tran functions
    //


    public void visit(RuleAddClass<SimpleRuleGuard> r) {
        try {
            if (isInterestingRule(r)) {
                makeGuard(r);
                makeAddClassStmts(r);
            }
        } catch (VariableNotFoundException e) {
            varException = e;
        }
    }


    private void makeGuard(Rule<SimpleRuleGuard> r) {
        try {
            guard = rbool(true);

            SimpleRuleGuard g = r.getGuard();

            for (CSSClass c : g.getUpClasses()) {
                guard = and(guard, vars.getZVar(c));
            }

            for (CSSClass c : g.getFlatClasses()) {
                guard = and(guard, vars.getYVar(c));
            }

            Collection<CSSClass> cs = g.getDownClasses();
            if (!cs.isEmpty()) {
                for (CSSClass c : cs) {
                    guard = and(guard, vars.getXVar(c));
                }
                guard = and(guard, vars.getPopVar());
            }
        } catch (VariableNotFoundException e) {
            varException = e;
        }

    }

    private void makeAddClassStmts(Rule<SimpleRuleGuard> r)
    throws VariableNotFoundException {
        RemoplaAssignStmt assign = new RemoplaAssignStmt();
        for (CSSClass c : r.getRhsClasses()) {
            if (vars.hasYVar(c))
                assign.addParallelAssignment(vars.getYVar(c), rbool(true));
        }
        stmts.add(assign);
    }

    public void visit(RuleAddChild<SimpleRuleGuard> r) {
        try {
            // add child is always interesting
            makeGuard(r);
            makeAddChildStmts(r);
        } catch (VariableNotFoundException e) {
            varException = e;
        }
    }

    private void makeAddChildStmts(RuleAddChild<SimpleRuleGuard> r)
    throws VariableNotFoundException {
        // set xs to false (new child has no children)
        RemoplaAssignStmt assign = new RemoplaAssignStmt();
        for (CSSClass c : vars.getXVarClasses()) {
            assign.addParallelAssignment(vars.getXVar(c), rbool(false));
        }
        // set pop to false
        assign.addParallelAssignment(vars.getPopVar(), rbool(false));

        // call sim method
        Set<CSSClass> bs = r.getRhsClasses();

        List<RemoplaExpr> args = new ArrayList<RemoplaExpr>();
        // root' = false
        args.add(rbool(false));
        // y's
        for (CSSClass c : vars.getYVarClasses()) {
            args.add(rbool(bs.contains(c)));
        }
        // z's
        for (CSSClass c : vars.getZVarClasses()) {
            // TODO: check this is an ok assumption...
            if (vars.hasYVar(c))
                args.add(vars.getYVar(c));
        }
        RemoplaInvokeStmt call = invoke(Namer.getNodeMethodName(),
                                        args);

        stmts.add(assign);
        stmts.add(call);
    }


    private boolean isInterestingRule(Rule<SimpleRuleGuard> r) {
        for (CSSClass c : r.getRhsClasses()) {
            if (vars.hasYVar(c))
                return true;
        }
        return false;
    }
}
