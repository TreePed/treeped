/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// CmdOptions.java
// Author: Matthew Hague
//
// A static class that reads command line arguments and makes them available to
// the rest of the program

package treeped.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Formatter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import treeped.css.Namer;

import org.apache.log4j.Logger;

public class CmdOptions {

    static Logger logger = Logger.getLogger(CmdOptions.class);

    // can't change these, they're hangovers from jimpletomoped

    private static int intBits = 4;
    private static int minInt = 0;

    // can change these:

    private static String bddPackage = "java";
    private static int nodeNum = 100000;
    private static int cacheSize = 100000;
    private static boolean outputRemopla = false;
    private static boolean outputPDS = false;
    private static boolean ahtml = false;
    private static boolean fulltree = true;
    private static boolean realHtml = false;
    private static boolean variableHtml = false;
    private static Set<String> jqueryFuns = new HashSet<String>();
    private static String witnessClass = null;
    private static boolean checkCSSRules = true;
    private static boolean outputAHtml = false;
    private static boolean outputSimpleAHtml = false;
    private static boolean outputAHtmlResult = false;
    private static boolean outputCssRuleInfo = false;
    private static boolean outputStats = false;
    private static boolean modelCheck = true;

    private static List<String> htmlFiles = null;
    private static String ahtmlFile = null;


    public static String usage() {
        String usage = "Usage: ./TreePed [options] <task>\n";
        usage += "\n";
        usage += "<task> may be a list of HTML files for analysis of CSS redundancy:\n";
        usage += "  Example: ./TreePed src/examples/html/comments/comments.html\n";
        usage += "<task> may be an Abstract HTML files for analysis of class redundancy:\n";
        usage += "  Example: ./TreePed src/examples/news.ahtml\n";
        usage += "\n";
        usage += "Options:\n";
        usage += "\n";
        usage += formatOpt("-out-remopla",
                           "Output the Remopla translation generated to stdout.");
        usage += formatOpt("-translateOnly",
                           "Only perform the translation to Remopla.  Do not model-check.");
        usage += formatOpt("-out-pds",
                           "Output the pushdown system constructed from the Remopla to stdout.");
        usage += formatOpt("-ahtml",
                           "Model check an abstract html file instead of Java code (option autodetected if .ahtml file extension used).");
        usage += formatOpt("-realhtml",
                           "Model check a 'real' html file instead of Java code (option autodetected if .html file extension used).");
        usage += formatOpt("-nofulltree",
                           "Html model checking but only look for class addition to root of tree, not full tree as default.");
        usage += formatOpt("-varhtml",
                           "Unknown variables may contain html code (by default they are assumed not to)");
        usage += formatOpt("-jqfs",
                           "A comma (no spaces) separated list (e.g. remove,fadeIn) of jquery function names to be marked in the html tree (to check they are called)");
        usage += formatOpt("-witness",
                           "A CSS class name for which to produce a witnessing trace if the class is addable.");
        usage += formatOpt("-nocheckcss",
                           "Do not check for redundant css rules (only CSS classes).");
        usage += formatOpt("-out-ahtml",
                           "When taking html as input, show the generated abstract html.");
        usage += formatOpt("-out-simple",
                           "When taking ahtml as input, show the generated simple abstract html.");
        usage += formatOpt("-out-ahtml-res",
                           "When taking html as input, show the result of each abstract html analysis.");
        usage += formatOpt("-out-css-info",
                           "When outputting CSS rules, include some location information (this may be difficult to interpret).");
        usage += formatOpt("-out-stats",
                           "Output some statistics about things collected.");
        usage += formatOpt("-translate-only",
                           "Only perform translation steps, don't model check.");
        usage += formatOpt("-bdd",
                           "Specify the name of the bdd package to used (anything that javabdd recognises, default: java)");

        return usage;
    }

    public static boolean getModelCheck() {
        return modelCheck;
    }

    public static int getMinInt() {
        return minInt;
    }

    public static int getIntBits() {
        return intBits;
    }

    public static boolean getOutputPDS() {
        return outputPDS;
    }

    public static String getBddPackage() {
        return bddPackage;
    }

    public static int getNodeNum() {
        return nodeNum;
    }

    public static int getCacheSize() {
        return cacheSize;
    }

    public static boolean getOutputRemopla() {
        return outputRemopla;
    }

    public static boolean getAHTML() {
        return ahtml;
    }

    public static boolean getRealHTML() {
        return realHtml;
    }

    public static String getAHTMLFile() {
        return ahtmlFile;
    }

    public static List<String> getHTMLFiles() {
        return htmlFiles;
    }

    public static boolean getFullTree() {
        return fulltree;
    }

    public static boolean getVariableHtml() {
        return variableHtml;
    }

    public static Set<String> getJQueryFunctions() {
        return Collections.unmodifiableSet(jqueryFuns);
    }

    /**
     * @return a class to produce a witness to (or null if none requested)
     */
    public static String getWitnessClass() {
        return witnessClass;
    }


    /**
     * @return true if the user wants us to check css rules to see if any are redundant
     */
    public static boolean getCheckCSSRules() {
        return checkCSSRules;
    }

    public static boolean getOutputAHtml() {
        return outputAHtml;
    }

    public static boolean getOutputSimpleAHtml() {
        return outputSimpleAHtml;
    }

    public static boolean getOutputAHtmlResult() {
        return outputAHtmlResult;
    }

    public static boolean getOutputCssRuleInfo() {
        return outputCssRuleInfo;
    }

    public static boolean getOutputStats() {
        return outputStats;
    }


    /** Takes the command line args and processes them.  Results available
     * through get methods.
     *   @params args the String[] passed to main
     *   @return true if success, false otherwise
     */
    public static boolean readCommandLine(String[] args) {
        boolean ok = true;

        if (args.length < 1)
            return false;

        for (int i = 0; i < args.length && ok; i++) {
            if (args[i].equals("-jqfs")) {
                if (i + 1 < args.length) {
                    for (String f : args[i+1].split(","))
                        jqueryFuns.add(f);
                    i++;
                }
            } else if (args[i].equals("-witness")) {
                if (i + 1 < args.length) {
                    witnessClass = args[i+1];
                    i++;
                }
            } else if (args[i].equals("-bdd")) {
                if (i + 1 < args.length) {
                    bddPackage = args[i+1];
                    i++;
                }
            } else {
                if (args[i].equals("-out-remopla")) {
                    outputRemopla = true;
                } else if (args[i].equals("-translateOnly")) {
                    modelCheck = false;
                } else if (args[i].equals("-out-pds")) {
                    outputPDS = true;
                } else if (args[i].equals("-ahtml")) {
                    ahtml = true;
                } else if (args[i].equals("-realHtml")) {
                    realHtml = true;
                } else if (args[i].equals("-varhtml")) {
                    variableHtml = true;
                } else if (args[i].equals("-nofulltree")) {
                    fulltree = false;
                } else if (args[i].equals("-nocheckcss")) {
                    checkCSSRules = false;
                } else if (args[i].equals("-out-ahtml")) {
                    outputAHtml = true;
                } else if (args[i].equals("-out-simple")) {
                    outputSimpleAHtml = true;
                } else if (args[i].equals("-out-ahtml-res")) {
                    outputAHtmlResult = true;
                } else if (args[i].equals("-out-css-info")) {
                    outputCssRuleInfo= true;
                } else if (args[i].equals("-out-stats")) {
                    outputStats = true;
                } else if (args[i].equals("-translate-only")) {
                    modelCheck = false;
                } else if (ahtmlFile == null) {
                    if (!realHtml &&
                               treeped.css.Namer.isHTMLFile(args[i])) {
                        ahtmlFile = args[i];
                        ahtml = true;
                    } else if (treeped.css.Namer.isRealHTMLFile(args[i])) {
                        realHtml = true;
                        if (htmlFiles == null)
                            htmlFiles = new LinkedList<String>();
                        htmlFiles.add(args[i]);
                    }
                } else {
                    logger.error("Unexpected argument: " + args[i]);
                    ok = false;
                }
	        }
        }

        return ok &&  ((ahtml && ahtmlFile != null))
                     || (realHtml && htmlFiles != null);
    }

    // formats the option
    private static String tab = "    ";

    private static String formatOpt(String option, String description) {
        String res = tab + option + "\n\n";
        return res + wordWrap(description, tab + tab, "") + "\n";
    }


    // Word wrapping code stolen from Keith at Joustlog, then modded
    private static final Pattern wrapRE = Pattern.compile(".{0,63}(?:\\S(?:-| |$)|$)");

    private static String wordWrap(String str, String prefix, String postfix) {
        String s = "";
        Matcher m = wrapRE.matcher(str);
        while (m.find())
            s += prefix + m.group() + postfix + "\n";
        return s;
    }



}
