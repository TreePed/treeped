

package treeped.test;

import java.util.Map;
import java.util.Set;

import de.tum.in.jmoped.translator.Translator;
import de.tum.in.jmoped.underbone.NullSemiring;
import de.tum.in.jmoped.underbone.Remopla.BDDDomainCoverage;
import de.tum.in.wpds.Fa;
import de.tum.in.wpds.Pds;
import de.tum.in.wpds.PdsSat;
import de.tum.in.wpds.Sat;

import treeped.jmopedinterface.CommandLineProgressMonitor;
import treeped.jmopedinterface.SimpleRemoplaListener;
import treeped.remoplatopds.BDDSemiring;
import treeped.remoplatopds.RemoplaToPDS;
import treeped.remoplatopds.StmtOptimisations;
import treeped.remoplatopds.VarManager;
import treeped.representation.Remopla;
import treeped.representation.RemoplaStmt;
import treeped.main.CmdOptions;

public class CheckPds {

    public static Pds getPds(Remopla remopla, Map<RemoplaStmt, StmtOptimisations> stmtOptimisations) {
        return RemoplaToPDS.translate(remopla, stmtOptimisations);
    }

    /** Run the model checker on Remopla **/
    public static Set<String> doCheckPDS(Pds pds, String init, Remopla remopla,
                                         Map<RemoplaStmt, StmtOptimisations> stmtOptimisations) {
        Set<String> reachedSet = null;

        // First post*
        Set<String> labels = firstpoststar(pds, init);

        // Creates variable manager
        VarManager manager = new VarManager(CmdOptions.getBddPackage(),
                                            CmdOptions.getNodeNum(),
                                            CmdOptions.getCacheSize(),
                                            CmdOptions.getIntBits(),
                                            remopla,
                                            stmtOptimisations);


        // Initializes listener
        CommandLineProgressMonitor monitor = new CommandLineProgressMonitor();
        SimpleRemoplaListener listener = new SimpleRemoplaListener();
        listener.setProgressMonitor(monitor);
        listener.beginTask(init, labels);

        // Initializes FA
        Fa fa = new Fa();
        fa.add(new BDDSemiring(manager, manager.initVars()), Fa.q_i, init, Fa.q_f);

        // Second post*
        Sat sat;


        sat = new PdsSat(pds);
        sat.setListener(listener);
        sat.poststar(fa, monitor);

        listener.done();

        reachedSet = listener.getReachedSet();
        // initial label doesn't seem to make it into the set, so add it
        // here
        reachedSet.add(init);

        manager.printTempUsage();

        return reachedSet;
    }

	public static Set<String> firstpoststar(Pds pds, String init) {
		Fa fa = new Fa();
		fa.add(new NullSemiring(), Fa.q_i, init, Fa.q_f);
		PdsSat sat = new PdsSat(pds);
		Fa post = (Fa) sat.poststar(fa);
		Set<String> labels = post.getLabels();
		return labels;
	}

}
