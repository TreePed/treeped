/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// SimpleRemoplaListener.java
// Author: Matthew Hague
//
// Implements a simple listener that passes everything to the progress monitor


package treeped.jmopedinterface;

import java.util.HashSet;
import java.util.Set;

import de.tum.in.jmoped.underbone.ProgressMonitor;
import de.tum.in.jmoped.underbone.RemoplaListener;

import de.tum.in.wpds.SatListener;

import treeped.main.CmdOptions;

import org.apache.log4j.Logger;

public class SimpleRemoplaListener implements RemoplaListener {

    static Logger logger = Logger.getLogger(SimpleRemoplaListener.class);

    private static final String REACHED_LABEL_PREFIX = "LABELREACHED: ";

    ProgressMonitor monitor = null;
    int totalDone = 0;
    Set<String> reachedLabels = new HashSet();

    public SimpleRemoplaListener() {

    }

    public Set<String> getReachedSet() {
        return reachedLabels;
    }

    public void clearReachedSet() {
        reachedLabels.clear();
    }

	/**
	 * Wraps the progress monitor.
	 *
	 * @param monitor the progress monitor.
	 */
	public void setProgressMonitor(ProgressMonitor monitor) {
        this.monitor = monitor;
    }

	/**
	 * Starts the listener.
	 *
	 * @param labels the labels to be tested.
	 * @see ProgressMonitor#beginTask(String, int).
	 */
	public void beginTask(String name, Set<String> labels) {
        if (monitor != null) {
            monitor.beginTask(name, labels.size());
        }
    }

	/**
	 * Ends the listener.
	 *
	 * @see ProgressMonitor#done().
	 */
	public void done() {
        if (monitor != null) {
            monitor.done();
        }
    }

	/**
	 * Notifies that <code>label</code> is reached.
	 *
	 * @param label the label.
	 */
	public void reach(String label) {
        logger.info(REACHED_LABEL_PREFIX + label);
        reachedLabels.add(label);
        totalDone++;
        if (monitor != null) {
            monitor.worked(totalDone);
        }
    }
}
