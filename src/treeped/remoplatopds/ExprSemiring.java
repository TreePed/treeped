/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// ExprSemiring.java
// Author: Matthew Hague
//
// A placeholder semiring that will be used to decorate the initial pushdown
// rules.  The BDDSemiring class will implement a semiring properly using BDDs
// and have the capability to deal with the semiring operations when passed a
// "degenerate" argument of this class.
//
// This class simply stores the RemoplaStmt associated with the transition.
// This mapping may not be direct.  For example, an if statement will not label
// its rules by if statements, rather, for each guard there will be a skip
// statement to the handler state with the associated guard.

// This class gets messier for dealing with Pre*
//
//
// When dealing with a push rule during Pre* analysis we have
//
//      p a --R--> p' b c
//
// leads to p --a, R'--> q' for each
//
//      p' --b, R1--> q --c, R2--> q'
//
// where we go in stages.  When we see
//
//      q' --b, R1--> q
//
// we add a mock rule
//
//      p a --call(R'')--> q c (*)
//
// where R'' is the constraint of R to provide the correct local values to
// R1 (i.e. do the method call).  That is for a method call
//
//      myfun(e1, ..., en)
//
// where myfun has arguments av1, ..., avn, we have
//
//      R'' = (E)tmpi ( (E)locals . R1[tmp1/av1, ..., tmpn/avn] & tmpi = ei )
//
// that is, save the locals in myfun to temps, abstract them all because
// they're not in scope at the call site, use the temps to constrain the
// values of the variables at the call site, abstract the temps because
// they're no longer needed.
//
// Thus we're left with R''(G0,L0,G1) where G0 and L0 are the value of the
// local and globals before the function call, and G1 are the global values
// at q.
//
// From R'' we construct an expr semiring call(R'') that will label the mock
// rule (*).  Then when we see
//
//      q --c, R2-->q'
//
// we add
//
//      p --a, R'--> q'
//
// where R'(G0, L0, G1) = (E)G . R''(G0, L0, G) & R2(G, L0, G1).
//
// Note we need L0 in G2 because the local values transfer from a to c.
//
// This method constructs call(R'').



package treeped.remoplatopds;

import org.apache.log4j.Logger;

import treeped.representation.RemoplaStmt;
import treeped.representation.RemoplaExpr;

public class ExprSemiring extends NullSemiring {

    static Logger logger = Logger.getLogger(ExprSemiring.class);

    private RemoplaStmt stmt;
    private RemoplaExpr returnAssign;
    private RemoplaExpr guard;

    public static enum RuleType { REWRITE, PUSH, POP; }

    private RuleType ruleType;

    /** @param stmt the statement belonging to this rule, non-null
     * @param ruleType whether this rule is push, pop or rewrite
     * @param guard a pre-condition of the transition
     */
    public ExprSemiring(RemoplaStmt stmt, RuleType ruleType, RemoplaExpr guard) {
        assert stmt != null;
        this.stmt = stmt;
        this.returnAssign = null;
        this.ruleType = ruleType;
        this.guard = guard;
    }

    /** @param stmt the statement belonging to this rule, non-null
     * @param ruleType whether this rule is push, pop or rewrite
     */
    public ExprSemiring(RemoplaStmt stmt, RuleType ruleType) {
        this(stmt, ruleType, null);
    }


    /** @param returnAssign an expression semiring for handling the return from
     * a method call.  Assigns the return value to the variable indicated by the
     * returnAssign.
     */
    public ExprSemiring(RemoplaExpr returnAssign, RemoplaExpr guard) {
        assert returnAssign != null;
        this.returnAssign = returnAssign;
        this.ruleType = RuleType.REWRITE;
        this.guard = guard;
    }

    public ExprSemiring(RemoplaExpr returnAssign) {
        this(returnAssign, null);
    }

    public RemoplaStmt getStmt() {
        return stmt;
    }

    public RemoplaExpr getCondition() {
        return guard;
    }

    /** Before extending a bdd by this semi ring, the bdd must satisfy the given
     * condition
     */
    public void setCondition(RemoplaExpr condition) {
        this.guard = condition;
    }

    public RemoplaExpr getReturnAssign() {
        return returnAssign;
    }

    public boolean isReturnAssign() {
        return (returnAssign != null);
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public String toString() {
        String s = null;
        if (isReturnAssign()) {
            s = returnAssign.toString();
        } else {
            s = stmt.toString();
        }

        if (guard != null) {
            s += " Guard: " + guard;
        }

        return s;
    }
}

