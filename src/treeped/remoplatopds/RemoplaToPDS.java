/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaToPDS.java
// Author: Matthew Hague
//
// A class for translating our Remopla representation to the PDS representation
// used by jwpds.  The strategy is similar to the strategy used by jMoped2, but
// not so tied to the particular translation (but tied to our "generic" Remopla
// representation).
//
//   RemoplaToPDS: Manages the translation.
//
//   StmtToRule: Translates a given statement to a PDS Rule.  The created rule
//   will have a weight that is an instance of ExprSemiring.
//
//   ExprSemiring: a simple class that is a "placeholder" semiring.  The
//   semiring operations do nothing.  It is more efficient to just add
//   placeholders at this stage.
//
//   BDDSemiring: the true semiring implementation.  When combining with another
//   semiring, it will check for the placeholders of type ExprSemiring and
//   update the BDD appropriately.  Like jMoped2, our BDDSemiring is tied to our
//   ExprSemiring class.


package treeped.remoplatopds;

import java.util.Iterator;
import java.util.Map;

import de.tum.in.wpds.Pds;
import de.tum.in.wpds.Rule;

import treeped.representation.Remopla;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaStmt;

public class RemoplaToPDS {

    /** Translates a treeped.representation.Remopla object into a
     * de.tum.in.wpds.Pds object with treeped.remoplatopds.ExprSemiring
     * weights.  The class jimpletoremople.BDDSemiring provides a concrete semiring
     * that can be combined with ExprSemiring weights to produce valid results.
     */
    static public Pds translate(Remopla remopla, Map<RemoplaStmt, StmtOptimisations> stmtOptimisations) {
        StmtToRules str = new StmtToRules();

        Pds pds = new Pds();
        addGlobalRules(pds, remopla, stmtOptimisations, str);
        addMethods(pds, remopla, stmtOptimisations, str);

        return pds;
    }


    //////////////////////////////////////////////////////////////////////////////
    // Private methods
    //

    static private void addGlobalRules(Pds pds, Remopla remopla,
                                       Map<RemoplaStmt, StmtOptimisations> stmtOptimisations,
                                       StmtToRules str) {
        for (RemoplaStmt floater : remopla.getFloatingStmts()) {
            for (Rule rule : str.translateStmt(floater, null, remopla, stmtOptimisations)) {
                pds.add(rule);
            }
        }
    }

    static private void addMethods(Pds pds, Remopla remopla,
                                   Map<RemoplaStmt, StmtOptimisations> stmtOptimisations,
                                   StmtToRules str) {
        for (RemoplaMethod method : remopla.getMethods()) {
            Iterator stmts = method.getStmts().iterator();
            RemoplaStmt nextStmt = stmts.hasNext() ? (RemoplaStmt)stmts.next() : null;
            while (nextStmt != null) {
                RemoplaStmt stmt = nextStmt;
                nextStmt = stmts.hasNext() ? (RemoplaStmt)stmts.next() : null;
                for (Rule rule : str.translateStmt(stmt, nextStmt, remopla, stmtOptimisations)) {
                    pds.add(rule);
                }
            }
        }
    }
}
