	function supportsInputPlaceholder() {
	  var el = document.createElement('input');
	  return 'placeholder' in el;
	}
	$(function() {
        var searchInput = $('#searchInput');
		if (!supportsInputPlaceholder()) {
		    var placeholder = searchInput.attr('placeholder');
			searchInput
				.val(placeholder)
				.focus(function() {
					$(this).addClass('touched');
					if ($(this).val() === placeholder) {
						$(this).val('');
					}
				})
				.blur(function() {
					$(this).removeClass('touched');
					if ($(this).val() === '') {
						$(this).val(placeholder);
					}
				});
		}
	});
