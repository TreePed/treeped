/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Set;

import org.apache.log4j.Logger;

import treeped.css.analysis.ClassAddingChecker;
import treeped.css.analysis.PerClassClassAddingChecker;
import treeped.css.analysis.OneBDDClassAddingChecker;
import treeped.css.analysis.MultiRemoplaClassAddingChecker;
import treeped.css.analysis.HTMLAnalyser;
import treeped.css.parser.AbstractHTMLReader;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.RuleGuard;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.translation.AHTMLToSimpleAHTML;

public class Main {

    static Logger logger = Logger.getLogger(Main.class);

    static abstract class Test {
        String title;

        public Test(String title) {
            this.title = title;
        }

        public String getTitle() { return title; }

        abstract public boolean run() throws Exception;

        /**
         * Uses reflection to call any method on an object
         *
         * @param o any object
         * @param method name of a method in the object (public, private, whatever)
         * @param params parameters
         * @return result of calling o.method(params), null if call failed
         */
        protected Object subversiveInvoke(Object o, String method, Object... params) {
            try {
                Class<?> c = o.getClass();
                while (c != null) {
                    final Method[] methods = c.getDeclaredMethods();
                    for (int i = 0; i < methods.length; ++i) {
                        if (methods[i].getName().equals(method)) {
                            methods[i].setAccessible(true);
                            return methods[i].invoke(o, params);
                        }
                    }
                    c = c.getSuperclass();
                }
            } catch (InvocationTargetException e) {
                logger.error("Error with subversiveInvoke.", e.getCause());
            } catch (IllegalAccessException e) {
                logger.error("Error with subversiveInvoke.", e);
            }
            return null;
        }

        /**
         * Uses reflection to get any field from an object
         *
         * @param o any object
         * @param field name of field in the object (public, private, whatever)
         * @return the object o.field, null if failed to find it
         */
        protected Object subversiveGetField(Object o, String field) {
            try {
                Class<?> c = o.getClass();
                while (c != null) {
                    final Field[] fields= c.getDeclaredFields();
                    for (int i = 0; i < fields.length; ++i) {
                        if (fields[i].getName().equals(field)) {
                            fields[i].setAccessible(true);
                            return fields[i].get(o);
                        }
                    }
                    c = c.getSuperclass();
                }
            } catch (IllegalAccessException e) {
                logger.error("Error with subversiveGetField.", e);
            }
            return null;
        }

    }


    static class TestClassAddingChecker extends Test {
        private String ahtml;
        private CSSClass addClass;
        private Set<CSSClass> initClasses;
        private Set<CSSClass> parentClasses;
        private Set<CSSClass> childClasses;
        private boolean isRoot;
        private boolean expected;

        public TestClassAddingChecker(String title,
                                      boolean expected,
                                      String ahtml,
                                      String addClass,
                                      Set<CSSClass> initClasses,
                                      Set<CSSClass> parentClasses,
                                      Set<CSSClass> childClasses,
                                      boolean isRoot) {
            super(title);
            this.ahtml = ahtml;
            this.addClass = new CSSClass(addClass);
            this.initClasses = initClasses;
            this.parentClasses = parentClasses;
            this.childClasses = childClasses;
            this.isRoot = isRoot;
            this.expected = expected;
        }

        public boolean run() throws Exception {
            AbstractHTML<RuleGuard> abshtml
                = AbstractHTMLReader.loadFromString(ahtml);

            AbstractHTML<SimpleRuleGuard> sabshtml
                = AHTMLToSimpleAHTML.translate(abshtml, false).ahtml;

            ClassAddingChecker classAdder
                = new PerClassClassAddingChecker(sabshtml);

            boolean res = classAdder.check(addClass,
                                           initClasses,
                                           parentClasses,
                                           childClasses,
                                           isRoot).getResult();

            boolean passed = (new Boolean(expected)).equals(res);

            if (!passed) {
                System.out.println("Failed class adding with per class checker.");
                System.out.println(subversiveGetField(classAdder, "remopla"));
                System.out.println(subversiveGetField(classAdder, "pds"));
            } else {
                classAdder
                    = new OneBDDClassAddingChecker(sabshtml);

                res = classAdder.check(addClass,
                                       initClasses,
                                       parentClasses,
                                       childClasses,
                                       isRoot).getResult();

                passed = (new Boolean(expected)).equals(res);

                if (!passed) {
                    System.out.println("Failed class adding with one BDD checker.");
                    System.out.println(subversiveGetField(classAdder, "pds"));
                } else {
                    classAdder
                        = new MultiRemoplaClassAddingChecker(sabshtml);

                    res = classAdder.check(addClass,
                                           initClasses,
                                           parentClasses,
                                           childClasses,
                                           isRoot).getResult();

                    passed = (new Boolean(expected)).equals(res);

                    if (!passed) {
                        System.out.println("Failed class adding with multi remopla checker.");
                        System.out.println(subversiveGetField(classAdder, "pds"));
                    }
                }
            }

            return passed;
        }
    }

    static class TestFullTreeClassAddingChecker extends Test {
        private String ahtml;
        private CSSClass addClass;
        private Set<CSSClass> initClasses;
        private Set<CSSClass> parentClasses;
        private Set<CSSClass> childClasses;
        private boolean isRoot;
        private boolean expected;

        public TestFullTreeClassAddingChecker(String title,
                                              boolean expected,
                                              String ahtml,
                                              String addClass,
                                              Set<CSSClass> initClasses,
                                              Set<CSSClass> parentClasses,
                                              Set<CSSClass> childClasses,
                                              boolean isRoot) {
            super(title);
            this.ahtml = ahtml;
            this.addClass = new CSSClass(addClass);
            this.initClasses = initClasses;
            this.parentClasses = parentClasses;
            this.childClasses = childClasses;
            this.isRoot = isRoot;
            this.expected = expected;
        }

        public boolean run() throws Exception {
            AbstractHTML<RuleGuard> abshtml
                = AbstractHTMLReader.loadFromString(ahtml);

            AbstractHTML<SimpleRuleGuard> sabshtml
                = AHTMLToSimpleAHTML.translate(abshtml, false).ahtml;

            ClassAddingChecker classAdder
                = new PerClassClassAddingChecker(sabshtml);

            boolean res = classAdder.checkFullTree(addClass,
                                                   initClasses,
                                                   parentClasses,
                                                   childClasses,
                                                   isRoot).getResult();

            boolean passed = (new Boolean(expected)).equals(res);

            if (!passed) {
                System.out.println("Failed full tree with per class checker.");
                System.out.println("Check class: " + addClass);
                System.out.println(ahtml);
                System.out.println(subversiveGetField(classAdder, "remopla"));
                System.out.println(subversiveGetField(classAdder, "pds"));
            } else {
                // NOT IMPLEMENTED FOR ONE BDD

                classAdder
                     = new MultiRemoplaClassAddingChecker(sabshtml);

                res = classAdder.checkFullTree(addClass,
                                               initClasses,
                                               parentClasses,
                                               childClasses,
                                               isRoot).getResult();

                passed = (new Boolean(expected)).equals(res);

                if (!passed) {
                    System.out.println("Failed full tree with Multi Remopla checker.");
                    System.out.println("Class: " + addClass);
                    System.out.println(ahtml);
                    System.out.println(subversiveGetField(classAdder, "remopla"));
                    System.out.println(subversiveGetField(classAdder, "pds"));
                }
            }

            return passed;
        }
    }



    static class TestRedundancyChecker extends Test {
        private String ahtml;
        private HTMLAnalyser.ResultType expectedResult;
        private Set<CSSClass> expectedRedundancies;
        private HTMLAnalyser.ResultType expectedFTResult;
        private Set<CSSClass> expectedFTRedundancies;

        public TestRedundancyChecker(String title,
                                     String ahtml,
                                     HTMLAnalyser.ResultType expectedResult,
                                     Set<CSSClass> expectedRedundancies) {
            super(title);
            this.ahtml = ahtml;
            this.expectedResult = expectedResult;
            this.expectedRedundancies = expectedRedundancies;
            this.expectedFTResult = null;
            this.expectedFTRedundancies = null;
        }

        public TestRedundancyChecker(String title,
                                     String ahtml,
                                     HTMLAnalyser.ResultType expectedResult,
                                     Set<CSSClass> expectedRedundancies,
                                     HTMLAnalyser.ResultType expectedFTResult,
                                     Set<CSSClass> expectedFTRedundancies) {
            super(title);
            this.ahtml = ahtml;
            this.expectedResult = expectedResult;
            this.expectedRedundancies = expectedRedundancies;
            this.expectedFTResult = expectedFTResult;
            this.expectedFTRedundancies = expectedFTRedundancies;
        }




        public boolean run() throws Exception {
            HTMLAnalyser analyser = new HTMLAnalyser();

            HTMLAnalyser.AnalysisResult res
                = analyser.analyseHTMLString(ahtml, false);

            boolean passed;
            if (res.getType() == HTMLAnalyser.ResultType.REDUNDANCIES)
                passed = (res.getType().equals(expectedResult)) &&
                         (res.getRedundancies().equals(expectedRedundancies));
            else
                passed = (res.getType().equals(expectedResult));

            if (!passed) {
                System.out.println("Expected: ");
                printRes(expectedResult, expectedRedundancies);
                System.out.println("Got: ");
                printRes(res.getType(), res.getRedundancies());
            } else if (expectedFTResult != null) {
                analyser = new HTMLAnalyser();
                res = analyser.analyseHTMLString(ahtml, true);

                if (res.getType() == HTMLAnalyser.ResultType.REDUNDANCIES) {
                    passed = (res.getType().equals(expectedFTResult)) &&
                             (res.getRedundancies().equals(expectedFTRedundancies));
                } else {
                    passed = (res.getType().equals(expectedFTResult));
                }

                if (!passed) {
                    System.out.println("Expected in full tree check: ");
                    printRes(expectedFTResult, expectedFTRedundancies);
                    System.out.println("Got: ");
                    printRes(res.getType(), res.getRedundancies());
                }
            }

            return passed;
        }

        private static void printRes(HTMLAnalyser.ResultType type,
                                     Collection<CSSClass> redundancies) {
            System.out.println(type);
            if (redundancies != null) {
                System.out.println(": " + redundancies);
            }
        }

    }


    static Test [] tests = {
        new TestClassAddingChecker("Simple, one-step, class addition, can add.",
                                   true,
                                   "{ a }, AddClass({ b })\n",
                                   "b",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("Simple, one-step, class addition, can't add.",
                                   false,
                                   "{ b }, AddClass({ a })\n",
                                   "b",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("Simple, two-step, class addition, can add.",
                                   true,
                                   "{ a }, AddClass({ b })\n" +
                                   "{ a b }, AddClass({ c })\n",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("Simple, two-step, class addition, can't add.",
                                   false,
                                   "{ a }, AddClass({ a })\n" +
                                   "{ a b }, AddClass({ c })\n",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("One-step, class addition with upguard, can add.",
                                   true,
                                   "Up { a }, AddClass({ b })\n",
                                   "b",
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   false),
        new TestClassAddingChecker("One-step, class addition with upguard, can't add.",
                                   false,
                                   "Up { a }, AddClass({ b })\n",
                                   "b",
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet("b"),
                                   CSSClass.makeSet(),
                                   false),
        new TestClassAddingChecker("Two-step, class addition with upguard, can add.",
                                   true,
                                   "Up { a }, AddClass({ b })\n" +
                                   "{ a b }, AddClass({ c })\n",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   false),
        new TestClassAddingChecker("Two-step, class addition with upguard, can't add.",
                                   false,
                                   "Up { a }, AddClass({ b })\n" +
                                   "{ a b }, AddClass({ c })\n",
                                   "c",
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   false),
        new TestClassAddingChecker("Two-step, child then class addition with downguard, can add.",
                                   true,
                                   "Down { b }, AddClass({ b })\n" +
                                   "{ a }, AddChild({ b })\n",
                                   "b",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   false),
        new TestClassAddingChecker("Two-step, child then class addition with downguard, can't add.",
                                   false,
                                   "Down { b }, AddClass({ b })\n" +
                                   "{ a }, AddChild({ a })\n",
                                   "b",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("Depth 2 tree to add class, can add.",
                                   true,
                                   "{ a }, AddChild({ b })\n" +
                                   "Up { a }, AddClass({ d })\n" +
                                   "{ d }, AddChild({ a })\n" +
                                   "Down { a }, AddClass({ c })\n" +
                                   "Down { c }, AddClass({ b })\n",
                                   "b",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   false),
      new TestClassAddingChecker("Depth 2 tree to add class, can't add.",
                                 false,
                                 "{ a }, AddChild({ b })\n" +
                                 "Up { a }, AddClass({ d })\n" +
                                 "{ d }, AddChild({ a })\n" +
                                 "Down { a }, AddClass({ a })\n" +
                                 "Down { c }, AddClass({ b })\n",
                                 "b",
                                 CSSClass.makeSet("a"),
                                 CSSClass.makeSet(),
                                 CSSClass.makeSet(),
                                 false),
        new TestClassAddingChecker("AddChild with Down Guard, can reach.",
                                   true,
                                   "{ a }, AddChild({ b })\n" +
                                   "Down { b }, AddChild({ c })\n" +
                                   "Down { c }, AddClass({ b })\n",
                                   "b",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("AddChild with Down Guard, can't reach.",
                                   false,
                                   "{ a }, AddChild({ b })\n" +
                                   "Down { b }, AddChild({ c })\n" +
                                   "Down { c }, AddClass({ a })\n",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("AddChild with Up Guard, can reach.",
                                   true,
                                   "{ a }, AddChild({ b })\n" +
                                   "Up { b }, AddChild({ c })\n" +
                                   "Down { c }, AddClass({ b })\n",
                                   "b",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet("b"),
                                   CSSClass.makeSet(),
                                   false),
        new TestClassAddingChecker("AddChild with Down Guard, can't reach.",
                                   false,
                                   "{ a }, AddChild({ b })\n" +
                                   "Up { b }, AddChild({ c })\n" +
                                   "Down { c }, AddClass({ a })\n",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet("b"),
                                   CSSClass.makeSet(),
                                   false),
        new TestClassAddingChecker("AddClass with flat and down guard, can reach.",
                                   true,
                                   "{ a }, AddChild({ b })\n" +
                                   "{ a } Down { b }, AddClass({ c })",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("AddClass with flat and down guard, can't reach flat.",
                                   false,
                                   "{ a }, AddChild({ b })\n" +
                                   "{ b } Down { b }, AddClass({ c })",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("AddClass with flat and down guard, can't reach down.",
                                   false,
                                   "{ a }, AddChild({ a })\n" +
                                   "{ a } Down { b }, AddClass({ c })",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("AddClass with flat and up guard, can reach.",
                                   true,
                                   "{ a }, AddChild({ b })\n" +
                                   "Up { a } { b }, AddClass({ c })\n" +
                                   "Down { c }, AddClass({ c })",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("AddClass with flat and up guard, can't reach up.",
                                   false,
                                   "{ a }, AddChild({ b })\n" +
                                   "Up { b } { b }, AddClass({ c })\n" +
                                   "Down { c }, AddClass({ c })",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("AddClass with flat and up guard, can't reach flat.",
                                   false,
                                   "{ a }, AddChild({ b })\n" +
                                   "Up { a } { a }, AddClass({ c })\n" +
                                   "Down { c }, AddClass({ c })",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("All three, can reach",
                                   true,
                                   "{ a }, AddChild({ b })\n" +
                                   "{ b }, AddChild({ c })\n" +
                                   "Up { a } { b } Down { c }, AddClass({ a })\n" +
                                   "Down { a b }, AddClass({ c })",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),
        new TestClassAddingChecker("All tree, can't reach",
                                   false,
                                   "{ a }, AddChild({ b })\n" +
                                   "{ b }, AddChild({ c })\n" +
                                   "Up { a } { b } Down { a }, AddClass({ a })\n" +
                                   "Down { a b }, AddClass({ c })",
                                   "c",
                                   CSSClass.makeSet("a"),
                                   CSSClass.makeSet(),
                                   CSSClass.makeSet(),
                                   true),



        new TestFullTreeClassAddingChecker("Simple, one-step, class addition, can add.",
                                           true,
                                           "{ a }, AddChild({ b })\n",
                                           "b",
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet(),
                                           true),
        new TestFullTreeClassAddingChecker("Simple, one-step, class addition, can't add.",
                                           false,
                                           "{ b }, AddChild({ b })\n",
                                           "b",
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet(),
                                           true),
        new TestFullTreeClassAddingChecker("Simple, two-step, class addition, can add.",
                                           true,
                                           "{ a }, AddChild({ b })\n" +
                                           "{ b }, AddChild({ c })\n",
                                           "c",
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet(),
                                           true),
        new TestFullTreeClassAddingChecker("Simple, two-step, class addition, can't add.",
                                           false,
                                           "{ a }, AddChild({ b })\n" +
                                           "{ a b }, AddChild({ c })\n",
                                           "c",
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet(),
                                           true),
        new TestFullTreeClassAddingChecker("One-step, class addition with upguard, can add.",
                                           true,
                                           "Up { a }, AddChild({ b })\n",
                                           "b",
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           false),
        new TestFullTreeClassAddingChecker("One-step, class addition with upguard, can't add.",
                                           false,
                                           "Up { a }, AddChild({ b })\n",
                                           "b",
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet("b"),
                                           CSSClass.makeSet(),
                                           false),
        new TestFullTreeClassAddingChecker("Two-step, class addition with upguard, can add.",
                                           true,
                                           "Up { a }, AddClass({ b })\n" +
                                           "{ a b }, AddChild({ c })\n",
                                           "c",
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           false),
        new TestFullTreeClassAddingChecker("Two-step, class addition with upguard, can't add.",
                                           false,
                                           "Up { a }, AddClass({ b })\n" +
                                           "{ a b }, AddChild({ c })\n",
                                           "c",
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           false),
        new TestFullTreeClassAddingChecker("Depth 2 tree to add class, can add.",
                                           true,
                                           "{ a }, AddChild({ b })\n" +
                                           "Up { a }, AddClass({ d })\n" +
                                           "{ d }, AddChild({ a })\n" +
                                           "Down { a }, AddClass({ c })\n" +
                                           "Down { c }, AddClass({ b })\n",
                                           "c",
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet(),
                                           false),
        new TestFullTreeClassAddingChecker("Depth 2 tree to add class, can't add.",
                                           false,
                                           "{ a }, AddChild({ b })\n" +
                                           "Up { a }, AddClass({ d })\n" +
                                           "{ d }, AddChild({ a })\n" +
                                           "Down { a }, AddClass({ a })\n" +
                                           "Down { c }, AddClass({ b })\n",
                                           "c",
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet(),
                                           false),
        new TestFullTreeClassAddingChecker("Depth 2 tree to add class, can add at root (full tree shouldn't say yes if can add at root.",
                                           false,
                                           "Up { a }, AddClass({ d })\n" +
                                           "{ d }, AddChild({ a })\n" +
                                           "Down { a }, AddClass({ c })\n" +
                                           "Down { c }, AddClass({ b })\n",
                                           "c",
                                           CSSClass.makeSet("a"),
                                           CSSClass.makeSet(),
                                           CSSClass.makeSet(),
                                           false),

        new TestRedundancyChecker("Simple one-step add class with redundant rule",
                                  "{ a }, AddClass({ b })\n" +
                                  "{ a c }, AddChild({ a })\n" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("Simple add class via one child",
                                  "{ a }, AddChild({ b })\n" +
                                  "Down { b }, AddClass({ b })\n" +
                                  "{ a c }, AddChild({ a })\n" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("Simple add class via one already existing",
                                  "Down { b }, AddClass({ b })\n" +
                                  "{ a c }, AddChild({ a })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("Simple add class via two children",
                                  "{ a }, AddChild({ b })\n" +
                                  "Up { a }, AddClass({ c })\n" +
                                  "{ b c }, AddChild({ a })\n" +
                                  "Up { b c }, AddClass({ b })\n" +
                                  "Down { a b }, AddClass({ a })\n" +
                                  "Down { a b c }, AddClass({ b })\n" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Add class via two children, no redundancies",
                                  "{ a }, AddChild({ b })\n" +
                                  "Up { a }, AddClass({ c })\n" +
                                  "{ b c }, AddChild({ a })\n" +
                                  "Up { b c }, AddClass({ b })\n" +
                                  "Down { a b }, AddClass({ a })\n" +
                                  "Down { a b c }, AddClass({ b c })\n" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet(),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Start with two child tree",
                                  "Down { a }, AddClass({ c })\n" +
                                  "Up { a c }, AddClass({ e })\n" +
                                  "{ e }, AddChild({ d })\n" +
                                  "{ d }, AddChild({ a b })\n" +
                                  "Down { a b }, AddClass({ a })\n" +
                                  "Down { a d }, AddClass({ a })\n" +
                                  "Down { a b c }, AddClass({ d })\n" +
                                  "<node class=\"a\">" +
                                  "  <node class=\"b c\"/>" +
                                  "  <node class=\"a\"/>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "e"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Should need two class adding checks on node",
                                  "{ b }, AddChild({ a })\n" +
                                  "Up { b }, AddClass({ b })\n" +
                                  "Down { a b }, AddClass({ c })\n" +
                                  "Up { b c }, AddClass({ d })\n" +
                                  "Down { d }, AddClass({ d })\n" +
                                  "<node class=\"a\">" +
                                  "  <node class=\"b\"/>" +
                                  "  <node class=\"c\"/>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Start with two child tree, uses up guard with add child",
                                  "Down { a }, AddClass({ c })\n" +
                                  "Up { a c }, AddChild({ d })\n" +
                                  "{ d }, AddChild({ a b })\n" +
                                  "Down { a b }, AddClass({ a })\n" +
                                  "Down { a d }, AddClass({ a })\n" +
                                  "Down { a b c }, AddClass({ d })\n" +
                                  "<node class=\"a\">" +
                                  "  <node class=\"b c\"/>" +
                                  "  <node class=\"a\"/>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Start with two child tree, uses down guard with add child",
                                  "Down { a }, AddClass({ c })\n" +
                                  "Up { a c }, AddClass({ e })\n" +
                                  "{ e }, AddChild({ d })\n" +
                                  "{ d }, AddChild({ f })\n" +
                                  "Down { f }, AddChild({ a b })\n" +
                                  "Down { a b }, AddClass({ a })\n" +
                                  "Down { a d }, AddClass({ a })\n" +
                                  "Down { a b c }, AddClass({ d })\n" +
                                  "<node class=\"a\">" +
                                  "  <node class=\"b c\"/>" +
                                  "  <node class=\"a\"/>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "e", "f"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("One step with down+ guard, can add",
                                  "Down+ { b }, AddClass({ c })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"a\">\n" +
                                  "    <node class=\"b\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Two step with down+ and up+ guard, can add",
                                  "Down+ { b c }, AddClass({ c })\n" +
                                  "Up+ { a }, AddClass({ c })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node>\n" +
                                  "    <node class=\"b\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("One step with down+ guard, can't add",
                                  "Down+ { b }, AddClass({ c })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"a\">\n" +
                                  "    <node class=\"c\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b")),
        new TestRedundancyChecker("Two step with down+ and up+ guard, can't add",
                                  "Down+ { a }, AddClass({ c })\n" +
                                  "Up+ { a }, AddClass({ c })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node>\n" +
                                  "    <node class=\"b\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Two down+ guards, can add",
                                  "Down+ { b }, AddClass({ c })\n" +
                                  "Down+ { c }, AddClass({ b })\n" +
                                  "{ a b c }, AddClass({ d })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node>\n" +
                                  "    <node class=\"b\"/>\n" +
                                  "  </node>\n" +
                                  "  <node>\n" +
                                  "    <node class=\"c\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet(),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Down+ guard split, can't add",
                                  "Down+ { b c }, AddClass({ c })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node>\n" +
                                  "    <node class=\"b\"/>\n" +
                                  "  </node>\n" +
                                  "  <node>\n" +
                                  "    <node class=\"c\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Two down+ guards, can't add",
                                  "Down+ { b c }, AddClass({ c })\n" +
                                  "Down+ { c }, AddClass({ b })\n" +
                                  "{ a b c }, AddClass({ d })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node>\n" +
                                  "    <node class=\"b\"/>\n" +
                                  "  </node>\n" +
                                  "  <node>\n" +
                                  "    <node class=\"c\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c", "d"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("d")),
        new TestRedundancyChecker("Down+ guards after add child, can add",
                                  "Down+ { c }, AddClass({ c })\n" +
                                  "{ a }, AddChild({ b })\n" +
                                  "{ b }, AddChild({ c })\n" +
                                  "<node>\n" +
                                  "  <node>\n" +
                                  "    <node class=\"a\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("a", "b"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Flat and Down Guard, can reach",
                                  "{ a } Down { b }, AddClass({ c })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Flat and Down Guard, can't reach down",
                                  "{ a } Down { c }, AddClass({ c })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("Flat and Down Guard, can't reach flat",
                                  "{ b } Down { b }, AddClass({ c })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("Flat and Up Guard, can reach",
                                  "Up { a } { b }, AddClass({ c })\n" +
                                  "Down { b c }, AddClass({ b })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Flat and Up Guard, can't reach up",
                                  "Up { b } { b }, AddClass({ c })\n" +
                                  "Down { b c }, AddClass({ b })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("Flat and Up Guard, can't reach flat",
                                  "Up { a } { a }, AddClass({ c })\n" +
                                  "Down { b c }, AddClass({ b })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("All tree, can reach",
                                  "Up { a } { b } Down { a }, AddClass({ c })\n" +
                                  "Down { b c }, AddClass({ b })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\">\n" +
                                  "    <node class=\"a\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("All tree, can't reach up",
                                  "Up { b } { b } Down { a }, AddClass({ c })\n" +
                                  "Down { b c }, AddClass({ b })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\">\n" +
                                  "    <node class=\"a\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("All tree, can't reach flat",
                                  "Up { a } { c } Down { a }, AddClass({ c })\n" +
                                  "Down { b c }, AddClass({ b })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\">\n" +
                                  "    <node class=\"a\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("All tree, can't reach down",
                                  "Up { a } { b } Down { b }, AddClass({ c })\n" +
                                  "Down { b c }, AddClass({ b })\n" +
                                  "<node class=\"a\">\n" +
                                  "  <node class=\"b\">\n" +
                                  "    <node class=\"a\"/>\n" +
                                  "  </node>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("AddChild with down guard and child in given tree",
                                  "Down { ul subnav }, AddChild({ span })" +
                                  "<node class=\"li\">" +
                                  "    <node class=\"ul subnav\"/>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("ul", "subnav", "span"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),


        // Some tests for complex guards
        new TestRedundancyChecker("One step And, can add",
                                  "And { {a} {b} }, AddClass({c})\n" +
                                  "<node class=\"a b\"/>",
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet(),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("One step And, can't add",
                                  "And { {a} {b} }, AddClass({c})\n" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c")),
        new TestRedundancyChecker("One step Or, can add",
                                  "Or { {a} {b} }, AddClass({c})\n" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b")),
        new TestRedundancyChecker("Two step And, Or, Down+, Up+, can add",
                                  "And { Down+ {a} Down+ {b} }, AddClass({c})" +
                                  "Or { Up+ {a} Up+ {b} }, AddClass({d})" +
                                  "Down+ {b d}, AddClass({c})" +
                                  "<node class=\"a\">" +
                                  "  <node>" +
                                  "    <node class=\"b\"/>" +
                                  "  </node>" +
                                  "  <node>" +
                                  "    <node class=\"c\"/>" +
                                  "  </node>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "d"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Two step And, Down+, Up+, can't add",
                                  "And { Down+ {a} Down+ {b} }, AddClass({c})" +
                                  "And { Up+ {a} Up+ {b} }, AddClass({d})" +
                                  "Down+ {b d}, AddClass({c})" +
                                  "<node class=\"a\">" +
                                  "  <node>" +
                                  "    <node class=\"b\"/>" +
                                  "  </node>" +
                                  "  <node>" +
                                  "    <node class=\"c\"/>" +
                                  "  </node>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c", "d"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("d")),
        new TestRedundancyChecker("Or with add child, can add",
                                  "Or { Up {a} Up {b} }, AddClass({c})\n" +
                                  "{ a }, AddChild({a})" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b")),
        new TestRedundancyChecker("And with add child, can add",
                                  "And { Up {a} Down {b} }, AddClass({c})\n" +
                                  "{a}, AddChild({a})" +
                                  "{a}, AddChild({b})" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("And with add child, can't add",
                                  "And { Up {a} Down {b} }, AddClass({c})\n" +
                                  "{a}, AddChild({a})" +
                                  "{a}, AddChild({a})" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c")),
        new TestRedundancyChecker("Two step And, Or, Stacked Down Up, can add",
                                  "And { Down Down {a} Down Down {b} }, AddClass({c})" +
                                  "Or { Up Up {a} Up Up {b} }, AddClass({d})" +
                                  "Down+ {b d}, AddClass({c})" +
                                  "<node class=\"a\">" +
                                  "  <node>" +
                                  "    <node class=\"b\"/>" +
                                  "  </node>" +
                                  "  <node>" +
                                  "    <node class=\"c\"/>" +
                                  "  </node>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "d"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Two step And, Stacked Down Up, can't add",
                                  "And { Down Down {a} Down Down {b} }, AddClass({c})" +
                                  "And { Up Up {a} Up Up {b} }, AddClass({d})" +
                                  "Down+ {b d}, AddClass({c})" +
                                  "<node class=\"a\">" +
                                  "  <node>" +
                                  "    <node class=\"b\"/>" +
                                  "  </node>" +
                                  "  <node>" +
                                  "    <node class=\"c\"/>" +
                                  "  </node>" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c", "d"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("d")),
        new TestRedundancyChecker("Stacked ups and downs, can add",
                                  "And { Up Up Down Up {a} Down Down Down {b} }, AddClass({c})\n" +
                                  "{b}, AddChild({a})" +
                                  "{a}, AddChild({b})" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("Stacked ups and downs, can't add",
                                  "And { Up Up Down Up {a} Down Down Down Down {b} }, AddClass({c})\n" +
                                  "{b}, AddChild({a})" +
                                  "{a}, AddChild({b})" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("And subformula, can add",
                                  "Down+ And { Up {b} Down {b} }, AddClass({c})\n" +
                                  "{b}, AddChild({a})" +
                                  "{a}, AddChild({b})" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b"),
                                  HTMLAnalyser.ResultType.NO_REDUNDANCIES,
                                  CSSClass.makeSet()),
        new TestRedundancyChecker("And subformula, can't add",
                                  "Down+ And { Up {b} Down Down {b} }, AddClass({c})\n" +
                                  "{b}, AddChild({a})" +
                                  "{a}, AddChild({b})" +
                                  "<node class=\"a\"/>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "c"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("c")),
        new TestRedundancyChecker("Up* tests",
                                  "And { Up* { a } { b } }, AddClass({ d })\n" +
                                  "And { Up* { a } { c } }, AddClass({ e })\n" +
                                  "{ a b }, AddClass({ f })\n" +
                                  "<node class=\"a c\">\n" +
                                  "  <node class=\"b\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("b", "d", "f"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("f")),
        new TestRedundancyChecker("Down* tests",
                                  "And { Down* { a } { b } }, AddClass({ d })\n" +
                                  "And { Down* { a } { c } }, AddClass({ e })\n" +
                                  "{ a b }, AddClass({ f })\n" +
                                  "<node class=\"b\">\n" +
                                  "  <node class=\"a c\"/>\n" +
                                  "</node>",
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("a", "c", "e", "f"),
                                  HTMLAnalyser.ResultType.REDUNDANCIES,
                                  CSSClass.makeSet("f")),
   };


    static public void main(String[] args) {
        int nfailed = 0;
        int npassed = 0;
        for (Test t : tests) {
            try {
                System.out.println("Running " + t.getTitle() + "...");
                if (t.run()) {
                    System.out.println("PASSED");
                    npassed++;
                } else {
                    System.out.println("FAILED");
                    nfailed++;
                }
            } catch (Exception e) {
                logger.error("Error with test " + t.getTitle(), e);
            }
        }
        System.out.println("Passes: " + npassed + " Fails: " + nfailed);
    }

}
